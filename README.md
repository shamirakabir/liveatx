# cs373-liveatx

Repository for the CS373 Software Engineering IDB Project (Fall 2021, 11 AM, Group 11).

## Info

- Git SHA Phase 4: 0e00afa092a37282d0662ddabc838b3033d2f570
- Project leader: Thomas Norman and Ethan Tan (split leadership)
- GitLab pipelines: https://gitlab.com/shamirakabir/liveatx/-/pipelines
- Website: https://liveatx.me
- API Documentation: https://documenter.getpostman.com/view/17689474/UUy37QtV

## Members

- Name: Archer Hasbany
- EID: amh7398
- GitLab ID: ArcherH
- Estimated completion time: Phase 4 - 10 hours
- Actual completion time: Phase 4 - 10 hours

---

- Name: Thomas Norman
- EID: tgn293
- GitLab ID: st0rmin
- Estimated completion time: Phase 4 - 15 hours
- Actual completion time: Phase 4 - 10 hours

---

- Name: Robyn Fajardo
- EID: rff359
- GitLab ID: robynfajardo
- Estimated complettion time: Phase 4 - 10 hours
- Actual completion time: Phase 4 - 10 hours

---

- Name: Shamira Kabir
- EID: sk46954
- GitLab ID: shamirakabir
- Estimated completion time: Phase 4 - 10 hours
- Actual completion time: Phase 4 - 20 hours

---

- Name: Ethan Tan
- EID: eyt249
- GitLab ID: phloofy
- Estimated completion time: Phase 4 - 24 hours
- Actual completion time: Phase 4 - 40 hours
