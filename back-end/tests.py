from unittest import main, TestCase
import requests

class Unit_Tests(TestCase):

    # Restaurants
    # Get all restaurants
    def test_restaurants_all(self):
        response = requests.get('https://api.liveatx.me/api/restaurants?page=1&itemsPerPage=10')
        assert response.status_code == 200
        json_data = response.json()
        # Check length
        assert len(json_data['restaurants']) == 10
        # Check indexed restaurant
        assert json_data['restaurants'][2] == {
            "restaurant_id": '8RPIjybMh5Eg90eVsv3fRQ',
            "name": "Via 313 Pizza - North Campus",
            "review_count": '640',
            "category": "Pizza",
            "rating": '4.5',
            "latitude": 30.297890252512275,
            "longitude": -97.74142265200399,
            "distance": 417.3013914328731,
            "fulfillment": ['delivery', 'pickup'],
            "price": "$$",
            "address": "3016 Guadalupe St",
            "city": "Austin",
            "zipcode": 78705,
            "state": "TX",
            "phone": "+15123586193",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/7OUrV9hEtlYBiG0ahkWdiA/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/via-313-pizza-north-campus-austin?adjust_creative=xrJ9j_A8VXeV3kiR8pMlnA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=xrJ9j_A8VXeV3kiR8pMlnA",
            "delivery": "Yes",
            "is_open": "Yes"
        }

    # Get restaurant by id
    def test_restaurant_id(self):
        response = requests.get('https://api.liveatx.me/api/restaurants/id=MGzro82Fi4LYvc86acoONQ')
        assert response.status_code == 200
        json_data = response.json()
        assert json_data == {
            "restaurant_id": "MGzro82Fi4LYvc86acoONQ",
            "name": "Franklin Barbecue",
            "review_count": '5129',
            "category": "Barbeque",
            "rating": '4.5',
            "latitude": 30.2701348,
            "longitude": -97.7313451068641,
            "distance": 3010.688842047021,
            "fulfillment": ["delivery"],
            "price": "$$",
            "address": "900 E 11th St",
            "city": "Austin",
            "zipcode": 78702,
            "state": "TX",
            "phone": "+15126531187",
            "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/xV3c8kwe-q10139Dh-Xc-g/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/franklin-barbecue-austin?adjust_creative=xrJ9j_A8VXeV3kiR8pMlnA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=xrJ9j_A8VXeV3kiR8pMlnA",
            "delivery": "Yes",
            "is_open": "Yes"
        }

    # Get restaurants by distance from 78705
    def test_restaurants_distance(self):
        response = requests.get('https://api.liveatx.me/api/restaurants/distance=500')
        assert response.status_code == 200
        json_data = response.json()
        # Check length
        assert len(json_data) == 12
        # Check indexed restaurant
        assert json_data[2] == {
            "restaurant_id": "k8KHoiDklnXDtXvLyxbd-w",
            "name": "The Original Black’s Barbecue",
            "review_count": '725',
            "category": "Barbeque",
            "rating": '4.0',
            "latitude": 30.298492113663,
            "longitude": -97.7412682360079,
            "distance": 429.5533992174368,
            "fulfillment": ["delivery", "pickup"],
            "price": "$$",
            "address": "3110 Guadalupe St",
            "city": "Austin",
            "zipcode": 78705,
            "state": "TX",
            "phone": "+15125240801",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/NlYSnSeFb6y6dJ3TVANZYQ/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/the-original-black-s-barbecue-austin?adjust_creative=xrJ9j_A8VXeV3kiR8pMlnA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=xrJ9j_A8VXeV3kiR8pMlnA",
            "delivery": "Yes",
            "is_open": "Yes"
        }

    # Entertainment
    # Get all entertainment
    def test_entertainment_all(self):
        response = requests.get('https://api.liveatx.me/api/entertainment?page=1&itemsPerPage=10')
        assert response.status_code == 200
        json_data = response.json()
        # Check length
        assert len(json_data['entertainment']) == 10
        # Check indexed entertainment
        assert json_data['entertainment'][1] == {
            "entertainment_id": "ZtIdXjQnceRzzJtUX7YRug",
            "name": "Arcade UFO",
            "review_count": '68',
            "category": "Arcades",
            "rating": '4.0',
            "latitude": 30.2953,
            "longitude": -97.73663,
            "distance": 169.07536440661477,
            "fulfillment": [],
            "price": "N/A",
            "address": "3101 Speedway",
            "city": "Austin",
            "zipcode": 78705,
            "state": "TX",
            "phone": "+15123801725",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/Pz-J0MRZEFN3r29vWqqXgQ/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/arcade-ufo-austin?adjust_creative=xrJ9j_A8VXeV3kiR8pMlnA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=xrJ9j_A8VXeV3kiR8pMlnA",
            "delivery": "No",
            "is_open": "Yes"
        }

    # Get entertainment by id
    def test_entertainment_id(self):
        response = requests.get('https://api.liveatx.me/api/entertainment/id=C0d5kzUx6C19mLcxQyhxCA')
        assert response.status_code == 200
        json_data = response.json()
        assert json_data == {
            "entertainment_id": "C0d5kzUx6C19mLcxQyhxCA",
            "name": "Alamo Drafthouse Cinema South Lamar",
            "review_count": '423',
            "category": "Cinema",
            "rating": '3.5',
            "latitude": 30.256068,
            "longitude": -97.763324,
            "distance": 5165.306415929757,
            "fulfillment": ["delivery", "pickup"],
            "price": "$$",
            "address": "1120 South Lamar Blvd",
            "city": "Austin",
            "zipcode": 78704,
            "state": "TX",
            "phone": "+15128617040",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/MeWjh08CvmzO5qNu4MYstg/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/alamo-drafthouse-cinema-south-lamar-austin?adjust_creative=xrJ9j_A8VXeV3kiR8pMlnA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=xrJ9j_A8VXeV3kiR8pMlnA",
            "delivery": "Yes",
            "is_open": "Yes"
        }

    # Get entertainment by distance from 78705
    def test_entertainment_distance(self):
        response = requests.get('https://api.liveatx.me/api/entertainment/distance=1000')
        assert response.status_code == 200
        json_data = response.json()
        # Check length
        assert len(json_data) == 4
        # Check indexed entertainment
        assert json_data[2] == {
            "entertainment_id": "j2cYa0F0IU40EE4nh784yQ",
            "name": "Hole in the Wall",
            "review_count": '183',
            "category": "Music Venues",
            "rating": '4.0',
            "latitude": 30.29005,
            "longitude": -97.74161,
            "distance": 855.9983545170526,
            "fulfillment": [],
            "price": "$",
            "address": "2538 Guadalupe St",
            "city": "Austin",
            "zipcode": 78705,
            "state": "TX",
            "phone": "",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/Pv_THvEK97KSYytcN0FxbA/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/hole-in-the-wall-austin?adjust_creative=xrJ9j_A8VXeV3kiR8pMlnA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=xrJ9j_A8VXeV3kiR8pMlnA",
            "delivery": "No",
            "is_open": "Yes"
        }

    # Events
    # Get all events
    def test_events_all(self):
        response = requests.get('https://api.liveatx.me/api/events?page=1&itemsPerPage=10')
        assert response.status_code == 200
        json_data = response.json()
        # Check length
        assert len(json_data['events']) == 10
        # Check indexed event
        assert json_data['events'][2] == {
            "event_id": "5382274",
            "name": "Supersuckers",
            "event_type": "concert",
            "datetime_utc": "2021-10-31T20:30:00",
            "url": "https://seatgeek.com/supersuckers-tickets/austin-texas-3ten-acl-live-1-2021-10-31-8-30-pm/concert/5382274",
            "average_ticket_price": 53,
            "median_ticket_price": 49,
            "lowest_ticket_price": 42,
            "highest_ticket_price": 74,
            "capacity": 0,
            "performers": ["Supersuckers"],
            "images": ["https://seatgeek.com/images/performers-landscape/supersuckers-10b8a8/1727/huge.jpg"],
            "venue": "3TEN ACL Live",
            "address": "310 Willie Nelson Blvd",
            "zipcode": 78701
        }

    # Get event by id
    def test_event_id(self):
        response = requests.get('https://api.liveatx.me/api/events/id=5442702')
        assert response.status_code == 200
        json_data = response.json()
        assert json_data == {
            "event_id": "5442702",
            "name": "100 Gecs",
            "event_type": "concert",
            "datetime_utc": "2021-11-11T20:00:00",
            "url": "https://seatgeek.com/100-gecs-tickets/austin-texas-emo-s-austin-2021-11-11-8-pm/concert/5442702",
            "average_ticket_price": 79,
            "median_ticket_price": 60,
            "lowest_ticket_price": 30,
            "highest_ticket_price": 256,
            "capacity": 2150,
            "performers": ["100 Gecs"],
            "images": ["https://seatgeek.com/images/performers-landscape/100-gecs-fe30cd/757434/31904/huge.jpg"],
            "venue": "Emo's Austin",
            "address": "2015 East Riverside Drive",
            "zipcode": 78741
        }

    # Get events by zipcode
    def test_events_zipcode(self):
        response = requests.get('https://api.liveatx.me/api/events/zipcode=78701')
        assert response.status_code == 200
        json_data = response.json()
        # Check length
        assert len(json_data) == 95
        # Check indexed event
        assert json_data[4] == {
            "event_id": "5468409",
            "name": "TV Girl",
            "event_type": "concert",
            "datetime_utc": "2021-11-02T20:00:00",
            "url": "https://seatgeek.com/tv-girl-tickets/austin-texas-mohawk-2021-11-02-8-pm/concert/5468409",
            "average_ticket_price": 138,
            "median_ticket_price": 138,
            "lowest_ticket_price": 138,
            "highest_ticket_price": 138,
            "capacity": 900,
            "performers": ["TV Girl"],
            "images": ["https://seatgeek.com/images/performers-landscape/tv-girl-54b9f1/17098/huge.jpg"],
            "venue": "Mohawk",
            "address": "912 Red River",
            "zipcode": 78701
        }

if __name__ == "__main__":  # pragma: no cover
    main()
