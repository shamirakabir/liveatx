import requests
import json
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://liveatx:cs373liveatx@liveatx-database.cc2vv7sryjnj.us-east-2.rds.amazonaws.com:5432/postgres"

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)


class Events(db.Model):
    event_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    event_type = db.Column(db.String)
    datetime_utc = db.Column(db.String)
    url = db.Column(db.String)
    average_ticket_price = db.Column(db.Float)
    median_ticket_price = db.Column(db.Float)
    lowest_ticket_price = db.Column(db.Float)
    highest_ticket_price = db.Column(db.Float)
    capacity = db.Column(db.Integer)
    performers = db.Column(db.JSON)
    images = db.Column(db.JSON)
    venue = db.Column(db.String)
    address = db.Column(db.String)
    zipcode = db.Column(db.Integer)

    def __init__(
        self,
        event_id,
        name,
        event_type,
        datetime_utc,
        url,
        average_ticket_price,
        median_ticket_price,
        lowest_ticket_price,
        highest_ticket_price,
        capacity,
        performers,
        images,
        venue,
        address,
        zipcode,
    ):
        self.event_id = event_id
        self.name = name
        self.event_type = event_type
        self.datetime_utc = datetime_utc
        self.url = url
        self.average_ticket_price = average_ticket_price
        self.median_ticket_price = median_ticket_price
        self.lowest_ticket_price = lowest_ticket_price
        self.highest_ticket_price = highest_ticket_price
        self.capacity = capacity
        self.performers = performers
        self.images = images
        self.venue = venue
        self.address = address
        self.zipcode = zipcode


def get_events_from_api():
    url = "https://api.seatgeek.com/2/events?"
    params = {
        "client_id": "MjM3NDIxOTd8MTYzMzM4MjM1Ny4xODI0ODY",
        "venue.city": "Austin",
        "venue.state": "TX",
        "taxonomies.name": "concert",
        "datetime_utc.gte": "2021-11-01",
        "datetime_utc.lte": "2021-11-30",
        "per_page": 200,
    }

    events_json = {
        "events": [],
        "total": 0,
        "taxonomies": ["concert", "sports", "comedy"],
    }

    for taxo in events_json["taxonomies"]:
        params["taxonomies.name"] = taxo
        result = requests.get(url=url, params=params).json()
        events_json["total"] += result["meta"]["total"]

        for event in result["events"]:
            performers_list = []
            for performer in event["performers"]:
                performers_list.append(performer["name"])

            image_list = []
            for image in event["performers"]:
                image_list.append(image["images"]["huge"])

            events_json["events"].append(
                {
                    "event_id": event["id"],
                    "name": event["short_title"],
                    "event_type": event["type"],
                    "datetime_utc": event["datetime_local"],
                    "url": event["url"],
                    "average_ticket_price": event["stats"]["average_price"],
                    "median_ticket_price": event["stats"]["median_price"],
                    "lowest_ticket_price": event["stats"]["lowest_price"],
                    "highest_ticket_price": event["stats"]["highest_price"],
                    "capacity": event["venue"]["capacity"],
                    "performers": performers_list,
                    "images": image_list,
                    "venue": event["venue"]["name"],
                    "address": event["venue"]["address"],
                    "zipcode": event["venue"]["postal_code"],
                }
            )

    with open("data/event_data.json", "w") as outfile:
        json.dump(events_json, outfile)


db.create_all()
db.session.commit()


def setup_event_db():
    # code to add events from json to db
    with open("data/event_data.json") as file:
        data = json.load(file)

    events = []

    for key in data["events"]:
        event_id = key["event_id"]
        name = key["name"]
        event_type = key["event_type"]
        datetime_utc = key["datetime_utc"]
        url = key["url"]
        average_ticket_price = key["average_ticket_price"]
        median_ticket_price = key["median_ticket_price"]
        lowest_ticket_price = key["lowest_ticket_price"]
        highest_ticket_price = key["highest_ticket_price"]
        capacity = key["capacity"]
        performers = key["performers"]
        images = key["images"]
        venue = key["venue"]
        address = key["address"]
        zipcode = key["zipcode"]

        event = Events(
            event_id,
            name,
            event_type,
            datetime_utc,
            url,
            average_ticket_price,
            median_ticket_price,
            lowest_ticket_price,
            highest_ticket_price,
            capacity,
            performers,
            images,
            venue,
            address,
            zipcode,
        )

        events.append(event)

    db.session.add_all(events)
    db.session.commit()


if __name__ == "__main__":
    # get_events_from_api()
    setup_event_db()
