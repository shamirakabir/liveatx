import requests
import json
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# import config

app = Flask(__name__)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://liveatx:cs373liveatx@liveatx-database.cc2vv7sryjnj.us-east-2.rds.amazonaws.com:5432/postgres"

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)


class Entertainment(db.Model):
    entertainment_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    review_count = db.Column(db.Integer)
    category = db.Column(db.String)
    rating = db.Column(db.Float)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    distance = db.Column(db.Float)
    fulfillment = db.Column(db.JSON)
    price = db.Column(db.String)
    address = db.Column(db.String)
    city = db.Column(db.String)
    zipcode = db.Column(db.Integer)
    state = db.Column(db.String)
    phone = db.Column(db.String)
    image_url = db.Column(db.String)
    yelp_url = db.Column(db.String)
    delivery = db.Column(db.String)
    is_open = db.Column(db.String)

    def __init__(
        self,
        entertainment_id,
        name,
        review_count,
        category,
        rating,
        latitude,
        longitude,
        distance,
        fulfillment,
        price,
        address,
        city,
        zipcode,
        state,
        phone,
        image_url,
        yelp_url,
        delivery,
        is_open,
    ):
        self.entertainment_id = entertainment_id
        self.name = name
        self.review_count = review_count
        self.category = category
        self.rating = rating
        self.latitude = latitude
        self.longitude = longitude
        self.distance = distance
        self.fulfillment = fulfillment
        self.price = price
        self.address = address
        self.city = city
        self.zipcode = zipcode
        self.state = state
        self.phone = phone
        self.image_url = image_url
        self.yelp_url = yelp_url
        self.delivery = delivery
        self.is_open = is_open


def get_entertainment_from_api():
    instances_per_page = 20
    url = "https://api.yelp.com/v3/businesses/search?"
    header = {"Authorization": "Bearer " + config.api_key}
    params = {"location": "78705", "radius": 5000, "offset": 0, "term": "arcade"}
    entertainment_json = {
        "businesses": [],
        "total": 0,
        "terms": ["arcade", "bowling", "escape room", "movie theatre", "museum"],
    }

    for category in entertainment_json["terms"]:
        params["offset"] = 0
        params["term"] = category
        this_page = requests.get(url=url, headers=header, params=params).json()
        total = this_page["total"]

        while params["offset"] <= total - instances_per_page:
            entertainment_json["businesses"] += this_page["businesses"]
            entertainment_json["total"] += this_page["total"]
            params["offset"] += instances_per_page
            this_page = requests.get(url=url, headers=header, params=params).json()

    with open("data/entertainment_data.json", "w") as outfile:
        json.dump(entertainment_json, outfile)


db.create_all()
db.session.commit()


def setup_entertainment_db():
    # code to add entertainment from json to db
    with open("data/entertainment_data.json") as file:
        data = json.load(file)

    entertainment_list = []

    for key in data["businesses"]:
        entertainment_id = key["id"]
        name = key["name"]
        review_count = key["review_count"]
        category = key["categories"][0]["title"]
        rating = key["rating"]
        latitude = key["coordinates"]["latitude"]
        longitude = key["coordinates"]["longitude"]
        distance = key["distance"]
        fulfillment = key["transactions"]

        if "price" in key:
            price = key["price"]
        else:
            price = "N/A"

        address = key["location"]["address1"]
        city = key["location"]["city"]
        zipcode = int(key["location"]["zip_code"])
        state = key["location"]["state"]
        phone = key["phone"]
        image_url = key["image_url"]
        yelp_url = key["url"]

        if "delivery" in key["transactions"]:
            delivery = "Yes"
        else:
            delivery = "No"

        if not key["is_closed"]:
            is_open = "Yes"
        else:
            is_open = "No"

        entertainment = Entertainment(
            entertainment_id,
            name,
            review_count,
            category,
            rating,
            latitude,
            longitude,
            distance,
            fulfillment,
            price,
            address,
            city,
            zipcode,
            state,
            phone,
            image_url,
            yelp_url,
            delivery,
            is_open,
        )

        entertainment_list.append(entertainment)

    db.session.add_all(entertainment_list)
    db.session.commit()


if __name__ == "__main__":
    # get_entertainment_from_api()
    setup_entertainment_db()
