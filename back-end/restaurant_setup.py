import requests
import json
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# import config

app = Flask(__name__)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://liveatx:cs373liveatx@liveatx-database.cc2vv7sryjnj.us-east-2.rds.amazonaws.com:5432/postgres"

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

class Restaurants(db.Model):
    restaurant_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    review_count = db.Column(db.Integer)
    category = db.Column(db.String)
    rating = db.Column(db.Float)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    distance = db.Column(db.Float)
    fulfillment = db.Column(db.JSON)
    price = db.Column(db.String)
    address = db.Column(db.String)
    city = db.Column(db.String)
    zipcode = db.Column(db.Integer)
    state = db.Column(db.String)
    phone = db.Column(db.String)
    image_url = db.Column(db.String)
    yelp_url = db.Column(db.String)
    delivery = db.Column(db.String)
    is_open = db.Column(db.String)

    def __init__(
        self,
        restaurant_id,
        name,
        review_count,
        category,
        rating,
        latitude,
        longitude,
        distance,
        fulfillment,
        price,
        address,
        city,
        zipcode,
        state,
        phone,
        image_url,
        yelp_url,
        delivery,
        is_open,
    ):
        self.restaurant_id = restaurant_id
        self.name = name
        self.review_count = review_count
        self.category = category
        self.rating = rating
        self.latitude = latitude
        self.longitude = longitude
        self.distance = distance
        self.fulfillment = fulfillment
        self.price = price
        self.address = address
        self.city = city
        self.zipcode = zipcode
        self.state = state
        self.phone = phone
        self.image_url = image_url
        self.yelp_url = yelp_url
        self.delivery = delivery
        self.is_open = is_open


def get_restaurants_from_api():
    instances_per_page = 20
    url = "https://api.yelp.com/v3/businesses/search?"
    header = {"Authorization": "Bearer " + config.api_key}
    params = {
        "location": "78705",
        "radius": 5000,
        "price": "1, 2, 3",
        "offset": 0,
    }
    restaurants_json = {
        "businesses": [],
        "total": 0,
    }

    this_page = requests.get(url=url, headers=header, params=params).json()
    total = this_page["total"]

    while params["offset"] <= total - instances_per_page:
        restaurants_json["businesses"] += this_page["businesses"]
        restaurants_json["total"] += this_page["total"]
        params["offset"] += instances_per_page
        this_page = requests.get(url=url, headers=header, params=params).json()

    with open("data/restaurant_data.json", "w") as outfile:
        json.dump(restaurants_json, outfile)


db.create_all()
db.session.commit()


def setup_restaurant_db():
    # code to add restaurants from json to db
    with open("data/restaurant_data.json") as file:
        data = json.load(file)

    restaurants = []

    for key in data["businesses"]:
        restaurant_id = key["id"]
        name = key["name"]
        review_count = key["review_count"]
        category = key["categories"][0]["title"]
        rating = key["rating"]
        latitude = key["coordinates"]["latitude"]
        longitude = key["coordinates"]["longitude"]
        distance = key["distance"]
        fulfillment = key["transactions"]
        price = key["price"]
        address = key["location"]["address1"]
        city = key["location"]["city"]
        zipcode = int(key["location"]["zip_code"])
        state = key["location"]["state"]
        phone = key["phone"]
        image_url = key["image_url"]
        yelp_url = key["url"]

        if "delivery" in key["transactions"]:
            delivery = "Yes"
        else:
            delivery = "No"

        if not key["is_closed"]:
            is_open = "Yes"
        else:
            is_open = "No"

        restaurant = Restaurants(
            restaurant_id,
            name,
            review_count,
            category,
            rating,
            latitude,
            longitude,
            distance,
            fulfillment,
            price,
            address,
            city,
            zipcode,
            state,
            phone,
            image_url,
            yelp_url,
            delivery,
            is_open,
        )

        restaurants.append(restaurant)

    db.session.add_all(restaurants)
    db.session.commit()


if __name__ == "__main__":
    # get_restaurants_from_api()
    setup_restaurant_db()
