from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import and_, or_, func
from restaurant_setup import Restaurants
from event_setup import Events
from entertainment_setup import Entertainment

def wrap_dict_query(name, q):
    try:
        return q[name]
    except KeyError:
        return None

# ---------------- Filter -------------
def filter_restaurants(q, restaurant_query):
    name = wrap_dict_query('name', q)
    price = wrap_dict_query('price', q)
    rating = wrap_dict_query('rating', q)
    category = wrap_dict_query('category', q)
    review_count = wrap_dict_query('review_count', q)
    is_open = wrap_dict_query('is_open', q)

    if name:
        restaurant_query = filter_restaurants_by(restaurant_query, "name", name)

    if price:
        restaurant_query = filter_restaurants_by(restaurant_query, "price", price)

    if rating:
        restaurant_query = filter_restaurants_by(restaurant_query, "rating", rating)

    if category:
        restaurant_query = filter_restaurants_by(restaurant_query, "category", category)

    if review_count:
        restaurant_query = filter_restaurants_by(restaurant_query, "review_count", review_count)

    if is_open:
        restaurant_query = filter_restaurants_by(restaurant_query, "is_open", is_open)

    return restaurant_query

def filter_restaurants_by(restaurant_query, filter, val):
    if filter == "name":
        restaurant_query = restaurant_query.filter(Restaurants.name.op('~')(val[0]))

    elif filter == "price":
        restaurant_query = restaurant_query.filter(Restaurants.price.in_(val))

    elif filter == "rating":
        restaurant_query = restaurant_query.filter(Restaurants.rating.in_(val))

    elif filter == "category":
        restaurant_query = restaurant_query.filter(Restaurants.category.in_(val))

    elif filter == "review_count":
        restaurant_query = restaurant_query.filter(Restaurants.review_count.review_count >= val[0])

    elif filter == "is_open":
        restaurant_query = restaurant_query.filter(Restaurants.is_open.in_(val))

    return restaurant_query

def filter_entertainment(q, entertainment_query):
    name = wrap_dict_query('name', q)
    price = wrap_dict_query('price', q)
    rating = wrap_dict_query('rating', q)
    category = wrap_dict_query('category', q)
    review_count = wrap_dict_query('review_count', q)
    is_open = wrap_dict_query('is_open', q)

    if name:
        entertainment_query = filter_entertainment_by(entertainment_query, "name", name)

    if price:
        entertainment_query = filter_entertainment_by(entertainment_query, "price", price)

    if rating:
        entertainment_query = filter_entertainment_by(entertainment_query, "rating", rating)

    if category:
        entertainment_query = filter_entertainment_by(entertainment_query, "category", category)

    if review_count:
        entertainment_query = filter_entertainment_by(entertainment_query, "review_count", review_count)

    if is_open:
        entertainment_query = filter_entertainment_by(entertainment_query, "is_open", is_open)

    return entertainment_query

def filter_entertainment_by(entertainment_query, filter, val):
    if filter == "name":
        entertainment_query = entertainment_query.filter(Entertainment.name.op('~')(val[0]))

    elif filter == "price":
        entertainment_query = entertainment_query.filter(Entertainment.price.in_(val))

    elif filter == "rating":
        entertainment_query = entertainment_query.filter(Entertainment.rating.in_(val))

    elif filter == "category":
        entertainment_query = entertainment_query.filter(Entertainment.category.in_(val))

    elif filter == "review_count":
        entertainment_query = entertainment_query.filter(Entertainment.review_count >= val[0])

    elif filter == "is_open":
        entertainment_query = entertainment_query.filter(Entertainment.is_open.in_(val))

    return entertainment_query

def filter_events(q, event_query):
    name = wrap_dict_query('name', q)
    average_ticket_price = wrap_dict_query('average_ticket_price', q)
    lowest_ticket_price = wrap_dict_query('lowest_ticket_price', q)
    highest_ticket_price = wrap_dict_query('highest_ticket_price', q)
    median_ticket_price = wrap_dict_query('median_ticket_price', q)
    datetime_utc = wrap_dict_query('datetime_utc', q)
    capacity = wrap_dict_query('capacity', q)

    if name:
        event_query = filter_events_by(event_query, "name", name)

    if average_ticket_price:
        event_query = filter_events_by(event_query, "average_ticket_price", average_ticket_price)

    if lowest_ticket_price:
        event_query = filter_events_by(event_query, "lowest_ticket_price", lowest_ticket_price)

    if highest_ticket_price:
        event_query = filter_events_by(event_query, "highest_ticket_price", highest_ticket_price)

    if median_ticket_price:
        event_query = filter_events_by(event_query, "median_ticket_price", median_ticket_price)

    if datetime_utc:
        event_query = filter_events_by(event_query, "datetime_utc", datetime_utc)

    if capacity:
        event_query = filter_events_by(event_query, "capacity", capacity)

    return event_query

def filter_events_by(event_query, filter, val):
    if filter == "name":
        event_query = event_query.filter(Events.name.op('~')(val[0]))

    elif filter == "average_ticket_price":
        event_query = event_query.filter(Events.average_ticket_price <= val[0])

    elif filter == "lowest_ticket_price":
        event_query = event_query.filter(Events.lowest_ticket_price <= val[0])

    elif filter == "highest_ticket_price":
        event_query = event_query.filter(Events.highest_ticket_price <= val[0])

    elif filter == "median_ticket_price":
        event_query = event_query.filter(Events.median_ticket_price <= val[0])

    elif filter == "datetime_utc":
        event_query = event_query.filter(Events.datetime_utc.in_(val))

    elif filter == "capacity":
        event_query = event_query.filter(Events.capacity.in_(val))

    return event_query

# ---------------- Sorting -------------
def sort_restaurants(sorting, restaurant_query):
    if not sorting:
        return restaurant_query
    else:
        sorting = sorting[0].split("-")

    # In descending order
    if len(sorting) > 1:
        return sort_restaurant_by(sorting[1], restaurant_query, True)
    else:
        return sort_restaurant_by(sorting[0], restaurant_query, False)

# Sorts restaurants by name, price, rating, type, review count, open now
def sort_restaurant_by(sorting, restaurant_query, desc):
    col = None

    if sorting == "name":
        col = Restaurants.name
    elif sorting == "price":
        col = Restaurants.price
    elif sorting == "rating":
        col = Restaurants.rating
    elif sorting == "category":
        col = Restaurants.category
    elif sorting == "review_count":
        col = Restaurants.review_count
    elif sorting == "is_open":
        col = Restaurants.is_open
    else:
        return restaurant_query

    if desc:
        return restaurant_query.order_by(col.desc())
    else:
        return restaurant_query.order_by(col)

def sort_entertainment(sorting, entertainment_query):
    if not sorting:
        return entertainment_query
    else:
        sorting = sorting[0].split("-")

    # In descending order
    if len(sorting) > 1:
        return sort_entertainment_by(sorting[1], entertainment_query, True)
    else:
        return sort_entertainment_by(sorting[0], entertainment_query, False)


# Sorts entertainment by name, price, rating, type, review count, open now
def sort_entertainment_by(sorting, entertainment_query, desc):
    col = None

    if sorting == "name":
        col = Entertainment.name
    elif sorting == "price":
        col = Entertainment.price
    elif sorting == "rating":
        col = Entertainment.rating
    elif sorting == "category":
        col = Entertainment.category
    elif sorting == "review_count":
        col = Entertainment.review_count
    elif sorting == "is_open":
        col = Entertainment.is_open
    else:
        return entertainment_query

    if desc:
        return entertainment_query.order_by(col.desc())
    else:
        return entertainment_query.order_by(col)


def sort_events(sorting, events_query):
    if not sorting:
        return events_query
    else:
        sorting = sorting[0].split("-")

    # In descending order
    if len(sorting) > 1:
        return sort_events_by(sorting[1], events_query, True)
    else:
        return sort_events_by(sorting[0], events_query, False)


# Sorts entertainment by name, price, rating, type, review count, open now
def sort_events_by(sorting, events_query, desc):
    col = None

    if sorting == "name":
        col = Events.name
    elif sorting == "average_ticket_price":
        col = Events.average_ticket_price
    elif sorting == "lowest_ticket_price":
        col = Events.lowest_ticket_price
    elif sorting == "highest_ticket_price":
        col = Events.highest_ticket_price
    elif sorting == "median_ticket_price":
        col = Events.median_ticket_price
    elif sorting == "datetime_utc":
        col = Events.datetime_utc
    elif sorting == "capacity":
        col = Events.capacity

    else:
        return events_query

    if desc:
        return events_query.order_by(col.desc())
    else:
        return events_query.order_by(col)
    
    
# ---------------- Searching -------------

# name, cuisine type, zipcode
def search_restaurants(q, restaurant_query):
    if not q:
        return restaurant_query
    else:
        q = q[0].strip()

    terms = q.split()
    terms = [w.lower() for w in terms]

    searches = []
    for term in terms:
        try:
            searches.append(Restaurants.zipcode.in_([int(term)]))
        except ValueError:
            pass
            
        searches.append(Restaurants.category.ilike("%{}%".format(term)))
        searches.append(Restaurants.name.ilike("%{}%".format(term)))

    restaurant_query = restaurant_query.filter(or_(*tuple(searches)))

    return restaurant_query

# name, type, zipcode
def search_entertainment(q, entertainment_query):
    if not q:
        return entertainment_query
    else:
        q = q[0].strip()

    terms = q.split()
    terms = [w.lower() for w in terms]

    searches = []
    for term in terms:
        try:
            searches.append(Entertainment.zipcode.in_([int(term)]))
        except ValueError:
            pass

        searches.append(Entertainment.category.ilike("%{}%".format(term)))
        searches.append(Entertainment.name.ilike("%{}%".format(term)))

    entertainment_query = entertainment_query.filter(or_(*tuple(searches)))

    return entertainment_query

# name, type, zipcode
def search_events(q, events_query):
    if not q:
        return events_query
    else:
        q = q[0].strip()

    terms = q.split()
    terms = [w.lower() for w in terms]

    searches = []
    for term in terms:
        try:
            searches.append(Events.zipcode.in_([int(term)]))
        except ValueError:
            pass

        searches.append(Events.event_type.ilike("%{}%".format(term)))
        searches.append(Events.name.ilike("%{}%".format(term)))

    events_query = events_query.filter(or_(*tuple(searches)))

    return events_query
