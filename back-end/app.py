# From Flask-Restful quickstart
from flask import Flask, jsonify, render_template, request
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS
from marshmallow import fields
from Filter_Search_Sort import *
from restaurant_setup import Restaurants
from entertainment_setup import Entertainment
from event_setup import Events
import json
import flask
import math

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
api = Api(app)

# connection to AWS db
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://liveatx:cs373liveatx@liveatx-database.cc2vv7sryjnj.us-east-2.rds.amazonaws.com:5432/postgres"

db = SQLAlchemy(app)
marsh = Marshmallow(app)

@app.route("/")
def cash_money():
    return '<img src="https://c.tenor.com/Im93yqaHJiIAAAAC/dinosaur-shades.gif" alt="swagzilla" />'

# Schemas
class RestaurantsSchema(marsh.Schema):
    class Meta:
        ordered = True

    restaurant_id = fields.String(required=True)
    name = fields.String(required=False)
    review_count = fields.String(required=False)
    category = fields.String(required=False)
    rating = fields.String(required=False)
    latitude = fields.Float(required=False)
    longitude = fields.Float(required=False)
    distance = fields.Float(required=False)
    fulfillment = fields.List(fields.String, required=False)
    price = fields.String(required=False)
    address = fields.String(required=False)
    city = fields.String(required=False)
    zipcode = fields.Integer(required=False)
    state = fields.String(required=False)
    phone = fields.String(required=False)
    image_url = fields.String(required=False)
    yelp_url = fields.String(required=False)
    delivery = fields.String(required=False)
    is_open = fields.String(required=False)


class EntertainmentSchema(marsh.Schema):
    class Meta:
        ordered = True

    entertainment_id = fields.String(required=True)
    name = fields.String(required=False)
    review_count = fields.String(required=False)
    category = fields.String(required=False)
    rating = fields.String(required=False)
    latitude = fields.Float(required=False)
    longitude = fields.Float(required=False)
    distance = fields.Float(required=False)
    fulfillment = fields.List(fields.String, required=False)
    price = fields.String(required=False)
    address = fields.String(required=False)
    city = fields.String(required=False)
    zipcode = fields.Integer(required=False)
    state = fields.String(required=False)
    phone = fields.String(required=False)
    image_url = fields.String(required=False)
    yelp_url = fields.String(required=False)
    delivery = fields.String(required=False)
    is_open = fields.String(required=False)


class EventsSchema(marsh.Schema):
    class Meta:
        ordered = True

    event_id = fields.String(required=True)
    name = fields.String(required=False)
    event_type = fields.String(required=False)
    datetime_utc = fields.String(required=False)
    url = fields.String(required=False)
    average_ticket_price = fields.Float(required=False)
    median_ticket_price = fields.Float(required=False)
    lowest_ticket_price = fields.Float(required=False)
    highest_ticket_price = fields.Float(required=False)
    capacity = fields.Integer(required=False)
    performers = fields.List(fields.String, required=False)
    images = fields.List(fields.String, required=False)
    venue = fields.String(required=False)
    address = fields.String(required=False)
    zipcode = fields.Integer(required=False)


# Initialize schemas
restaurant_schema = RestaurantsSchema()
restaurants_schema = RestaurantsSchema(many=True)

entertainment_schema = EntertainmentSchema()
entertainments_schema = EntertainmentSchema(many=True)

event_schema = EventsSchema()
events_schema = EventsSchema(many=True)

def pagination(response, pageNum, itemsPerPage):
    pageNum = int(pageNum)
    itemsPerPage = int(itemsPerPage)
    responseLen = len(response)
    calcNumPages = math.ceil(responseLen / itemsPerPage)
    lowerBound = (pageNum - 1) * itemsPerPage
    upperBound = (pageNum - 1) * itemsPerPage + itemsPerPage
    if upperBound > responseLen:
        upperBound = responseLen
    if pageNum < 0 or pageNum > calcNumPages:
        response = []
    else:
        response = response[lowerBound : upperBound]
    return (calcNumPages, response)


def wrap_dict_query(name, q):
    try:
        return q[name]
    except KeyError:
        return None

# Get all restaurants
@app.route("/api/restaurants", methods=["GET"])
def get_all_restaurants():
    queries = request.args.to_dict(flat=False)
    restaurants = db.session.query(Restaurants)

    # Searching
    q = wrap_dict_query("q", queries)
    if q:
        restaurants = search_restaurants(q, restaurants)

    # Sorting
    sorting = wrap_dict_query("sort", queries)
    restaurants = sort_restaurants(sorting, restaurants)
    
    # Filtering
    restaurants = filter_restaurants(queries, restaurants)

    response = restaurants_schema.dump(restaurants)
    page = wrap_dict_query("page", queries)
    itemsPerPage = wrap_dict_query("itemsPerPage", queries)
    if page and itemsPerPage:
        paginationResp = pagination(response, queries["page"][0], queries["itemsPerPage"][0])
        response = paginationResp[1]
        return jsonify({"numberOfPages": paginationResp[0], "restaurants": response})
    else:
        return jsonify({"restaurants": response})

# Get restaurant by id
@app.route("/api/restaurants/id=<id>", methods=["GET"])
def get_restaurants_by_id(id):
    restaurant = Restaurants.query.get(id)
    if restaurant is None:
        response = flask.Response(
            json.dumps({"error:": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return restaurant_schema.jsonify(restaurant)


# Get restaurant by distance from 78705 (campus)
@app.route("/api/restaurants/distance=<distance>", methods=["GET"])
def get_restaurants_by_distance(distance):
    restaurants = Restaurants.query.filter(Restaurants.distance <= distance)
    if restaurants is None:
        response = flask.Response(
            json.dumps({"error: no restaurants within that distance found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return restaurants_schema.jsonify(restaurants)


# Get all entertainment
@app.route("/api/entertainment", methods=["GET"])
def get_all_entertainment():
    queries = request.args.to_dict(flat=False)
    entertainments = db.session.query(Entertainment)

    # Searching
    q = wrap_dict_query("q", queries)
    if q:
        entertainments = search_entertainment(q, entertainments)

    # Sorting
    sorting = wrap_dict_query("sort", queries)
    entertainments = sort_entertainment(sorting, entertainments)

    # Filtering
    entertainments = filter_entertainment(queries, entertainments)

    response = entertainments_schema.dump(entertainments)
    page = wrap_dict_query("page", queries)
    itemsPerPage = wrap_dict_query("itemsPerPage", queries)
    if page and itemsPerPage:
        paginationResp = pagination(response, queries["page"][0], queries["itemsPerPage"][0])
        response = paginationResp[1]
        return jsonify({"numberOfPages": paginationResp[0], "entertainment": response})
    else:
        return jsonify({"entertainment": response})


# Get entertainment by id
@app.route("/api/entertainment/id=<id>", methods=["GET"])
def get_entertainment_by_id(id):
    entertainment = Entertainment.query.get(id)
    if entertainment is None:
        response = flask.Response(
            json.dumps({"error:": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return entertainment_schema.jsonify(entertainment)


# Get entertainment by distance from 78705 (campus)
@app.route("/api/entertainment/distance=<distance>", methods=["GET"])
def get_entertainment_by_distance(distance):
    entertainment = Entertainment.query.filter(Entertainment.distance <= distance)
    if entertainment is None:
        response = flask.Response(
            json.dumps({"error: no entertainment within that distance found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return entertainments_schema.jsonify(entertainment)


# Get all events
@app.route("/api/events", methods=["GET"])
def get_all_events():
    queries = request.args.to_dict(flat=False)
    events = db.session.query(Events)

    # Searching
    q = wrap_dict_query("q", queries)
    if q:
        events = search_events(q, events)

    # Sorting
    sorting = wrap_dict_query("sort", queries)
    events = sort_events(sorting, events)

    # Filtering
    events = filter_events(queries, events)

    response = events_schema.dump(events)
    page = wrap_dict_query("page", queries)
    itemsPerPage = wrap_dict_query("itemsPerPage", queries)
    if page and itemsPerPage:
        paginationResp = pagination(response, queries["page"][0], queries["itemsPerPage"][0])
        response = paginationResp[1]
        return jsonify({"numberOfPages": paginationResp[0], "events": response})
    else:
        return jsonify({"events": response})


# Get event by id
@app.route("/api/events/id=<id>", methods=["GET"])
def get_event_by_id(id):
    event = Events.query.get(id)
    if event is None:
        response = flask.Response(
            json.dumps({"error:": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return event_schema.jsonify(event)


# Get event by zipcode
@app.route("/api/events/zipcode=<zipcode>", methods=["GET"])
def get_events_by_zipcode(zipcode):
    events = Events.query.filter(Events.zipcode == zipcode)
    if events is None:
        response = flask.Response(
            json.dumps({"error: no events in zipcode ": zipcode}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return events_schema.jsonify(events)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)

# if __name__ == '__main__':
#     app.run(debug=True)
