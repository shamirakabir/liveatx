#! /bin/python3

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.firefox.options import Options

import sys

options = Options()
options.headless = True
driver = webdriver.Firefox(options=options)
url = 'https://liveatx.me'

class Selenium(unittest.TestCase):
    
    # basic test
    def test1(self):
        driver.get(url)
        result = driver.find_element_by_css_selector('#root')
        self.assertNotEqual(result, None)

    # test home page cards
    def test2(self):
        driver.get(url)
        WebDriverWait(driver, 5).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'MuiCard-root')))
        results = driver.find_elements_by_class_name('MuiCard-root')
        self.assertEqual(len(results), 3)
        for elem in results:
            self.assertEqual(elem.is_enabled(), True)

    # test home page navbar
    def test3(self):
        driver.get(url)
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.CLASS_NAME, 'MuiAppBar-root')))
        appbar = driver.find_element_by_class_name('MuiAppBar-root')
        WebDriverWait(driver, 5).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'MuiButtonBase-root')))
        
        results = appbar.find_elements(By.CLASS_NAME, 'MuiButtonBase-root')
        self.assertEqual(len(results), 8)
        for elem in results:
            self.assertEqual(elem.is_enabled(), True)

    # test about page cards
    @unittest.SkipTest
    def test4(self):
        driver.get(url + '/about')
        WebDriverWait(driver, 5).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'MuiCard-root')))
        results = driver.find_elements(By.CLASS_NAME, 'MuiCard-root')
        self.assertEqual(len(results), 5)
        for elem in results:
            self.assertEqual(elem.is_enabled(), True)

    # test about page navbar
    def test5(self):
        driver.get(url + '/about')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.CLASS_NAME, 'MuiAppBar-root')))
        appbar = driver.find_element_by_class_name('MuiAppBar-root')
        WebDriverWait(driver, 5).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'MuiButtonBase-root')))
        results = appbar.find_elements(By.CLASS_NAME, 'MuiButtonBase-root')
        self.assertEqual(len(results), 8)
        for elem in results:
            self.assertEqual(elem.is_enabled(), True)

    # test restaurant page pagination
    @unittest.SkipTest
    def test6(self):
        driver.get(url + '/restaurant')
        pagination = driver.find_element_by_class_name('MuiTablePagination-root')
        self.assertEqual(pagination.is_enabled(), True)

    @unittest.SkipTest
    def test7(self):
        driver.get(url + '/restaurant')
        actions = driver.find_element_by_class_name('MuiTablePagination-actions')
        self.assertEqual(actions.is_enabled(), True)
        results = actions.find_elements(By.TAG_NAME, 'button')
        self.assertEqual(len(results), 2)
        self.assertEqual(results[0].is_enabled(), False)
 
    # test restaurant page navbar
    def test8(self):
        driver.get(url + '/restaurant')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.CLASS_NAME, 'MuiAppBar-root')))
        appbar = driver.find_element_by_class_name('MuiAppBar-root')
        WebDriverWait(driver, 5).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'MuiButtonBase-root')))
        results = appbar.find_elements(By.CLASS_NAME, 'MuiButtonBase-root')
        self.assertEqual(len(results), 8)
        for elem in results:
            self.assertEqual(elem.is_enabled(), True)

    # test event page pagination
    @unittest.SkipTest
    def test9(self):
        driver.get(url + '/event')
        pagination = driver.find_element_by_class_name('MuiTablePagination-root')
        self.assertEqual(pagination.is_enabled(), True)

    @unittest.SkipTest    
    def test10(self):
        driver.get(url + '/event')
        actions = driver.find_element_by_class_name('MuiTablePagination-actions')
        self.assertEqual(actions.is_enabled(), True)
        results = actions.find_elements(By.TAG_NAME, 'button')
        self.assertEqual(len(results), 2)
        self.assertEqual(results[0].is_enabled(), False)

    # test event page navbar
    def test11(self):
        driver.get(url + '/event')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.CLASS_NAME, 'MuiAppBar-root')))
        appbar = driver.find_element_by_class_name('MuiAppBar-root')
        WebDriverWait(driver, 5).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'MuiButtonBase-root')))
        results = appbar.find_elements(By.CLASS_NAME, 'MuiButtonBase-root')
        self.assertEqual(len(results), 8)
        for elem in results:
            self.assertEqual(elem.is_enabled(), True)

    # test entertainment page pagination
    @unittest.SkipTest
    def test12(self):
        driver.get(url + '/entertainment')
        pagination = driver.find_element_by_class_name('MuiTablePagination-root')
        self.assertEqual(pagination.is_enabled(), True)

    @unittest.SkipTest
    def test13(self):
        driver.get(url + '/entertainment')
        actions = driver.find_element_by_class_name('MuiTablePagination-actions')
        self.assertEqual(actions.is_enabled(), True)
        results = actions.find_elements(By.TAG_NAME, 'button')
        self.assertEqual(len(results), 2)
        self.assertEqual(results[0].is_enabled(), False)

    # test entertainment page navbar
    def test14(self):
        driver.get(url + '/entertainment')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.CLASS_NAME, 'MuiAppBar-root')))
        appbar = driver.find_element_by_class_name('MuiAppBar-root')
        WebDriverWait(driver, 5).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'MuiButtonBase-root')))
        results = appbar.find_elements(By.CLASS_NAME, 'MuiButtonBase-root')
        self.assertEqual(len(results), 8)
        for elem in results:
            self.assertEqual(elem.is_enabled(), True)            

    # test sitewide search
    def test15(self):
        driver.get(url)
        search_bar = driver.find_element_by_xpath('//input[@placeholder="Enter sitewide search here."]')
        self.assertEqual(search_bar.is_enabled(), True)
        search_bar.send_keys('austin')
        search_bar.send_keys(Keys.ENTER)
        self.assertIn('/search?q=austin', driver.current_url)

    def test16(self):
        driver.get(url)
        search_bar = driver.find_element_by_xpath('//input[@placeholder="Enter sitewide search here."]')
        self.assertEqual(search_bar.is_enabled(), True)
        search_bar.send_keys('austin')
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[1]/div/div/span/span/span/button').click()
        self.assertIn('/search?q=austin', driver.current_url)

    # test restaurant sort
    @unittest.SkipTest
    def test17(self):
        driver.get(url + '/restaurant')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[1]/span/button')))
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[1]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]/div/span/span')))
        x = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]/div/span/span').text.lower()
        y = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[2]/td[1]/div[2]/div/span/span').text.lower()
        self.assertLess(x, y)
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[1]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]/div/span/span')))
        x = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]/div/span/span').text.lower()
        y = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[2]/td[1]/div[2]/div/span/span').text.lower()
        self.assertGreater(x, y)

    @unittest.SkipTest
    def test18(self):
        driver.get(url + '/restaurant')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[2]/span/button')))
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[2]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]/div[2]/div/span/span')))
        x = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]/div[2]/div/span/span').text)
        y = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[2]/div[2]/div/span/span').text)
        self.assertLessEqual(x, y)
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[2]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]/div[2]/div/span/span')))
        x = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]/div[2]/div/span/span').text)
        y = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[2]/div[2]/div/span/span').text)
        self.assertGreaterEqual(x, y)

    @unittest.SkipTest
    def test19(self):
        driver.get(url + '/restaurant')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[3]/span/button')))
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[3]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span')))
        x = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span').text)
        y = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[3]/div[2]/div/span/span').text)
        self.assertLessEqual(x, y)
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[3]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span')))
        x = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span').text)
        y = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[3]/div[2]/div/span/span').text)
        self.assertGreaterEqual(x, y)

    # test event sort
    @unittest.SkipTest
    def test20(self):
        driver.get(url + '/event')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[1]/span/button')))
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[1]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]/div/span/span')))
        x = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]/div/span/span').text.lower()
        y = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[2]/td[1]/div[2]/div/span/span').text.lower()
        self.assertLess(x, y)
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[1]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]/div/span/span')))
        x = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]/div/span/span').text.lower()
        y = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[2]/td[1]/div[2]/div/span/span').text.lower()
        self.assertGreater(x, y)

    @unittest.SkipTest
    def test21(self):
        driver.get(url + '/event')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[3]/span/button')))
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[3]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span')))
        x = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span').text.lower()
        y = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[3]/div[2]/div/span/span').text.lower()
        self.assertLessEqual(x, y)
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[3]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span')))
        x = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span').text.lower()
        y = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[3]/div[2]/div/span/span').text.lower()
        self.assertGreaterEqual(x, y)

    @unittest.SkipTest
    def test22(self):
        driver.get(url + '/event')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[4]/span/button')))
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[4]/span/button').click()
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[4]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[4]/div[2]')))
        x = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[4]/div[2]').text)
        y = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[4]/div[2]').text)
        self.assertGreaterEqual(x, y)

    # test entertainment sort
    @unittest.SkipTest
    def test23(self):
        driver.get(url + '/entertainment')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[1]/span/button')))
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[1]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]')))
        x = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]').text.lower()
        y = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[2]/td[1]/div[2]').text.lower()
        self.assertLess(x, y)
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[1]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]')))
        x = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]/div[2]').text.lower()
        y = driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[2]/td[1]/div[2]').text.lower()
        self.assertGreater(x, y)

    @unittest.SkipTest
    def test24(self):
        driver.get(url + '/entertainment')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[2]/span/button')))
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[2]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]/div[2]')))
        x = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]/div[2]').text)
        y = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[2]/div[2]').text)
        self.assertLessEqual(x, y)
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[2]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]/div[2]')))
        x = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]/div[2]').text)
        y = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[2]/div[2]').text)
        self.assertGreaterEqual(x, y)

    @unittest.SkipTest
    def test25(self):
        driver.get(url + '/entertainment')
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[3]/span/button')))
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[3]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span')))
        x = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span').text)
        y = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[3]/div[2]/div/span/span').text)
        self.assertLessEqual(x, y)
        driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/thead/tr/th[3]/span/button').click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span')))
        x = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[3]/div[2]/div/span/span').text)
        y = float(driver.find_element_by_xpath('/html/body/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[5]/td[3]/div[2]/div/span/span').text)
        self.assertGreaterEqual(x, y)

    # test search
    def test26(self):
        driver.get(url + '/restaurant')
        search_bar = driver.find_element_by_xpath('//input[@placeholder="Search restaurants"]')
        self.assertEqual(search_bar.is_enabled(), True)
        search_bar.send_keys('austin')
        search_bar.send_keys(Keys.ENTER)

    def test27(self):
        driver.get(url + '/event')
        search_bar = driver.find_element_by_xpath('//input[@placeholder="Search events"]')
        self.assertEqual(search_bar.is_enabled(), True)
        search_bar.send_keys('austin')
        search_bar.send_keys(Keys.ENTER)

    def test28(self):
        driver.get(url + '/entertainment')
        search_bar = driver.find_element_by_xpath('//input[@placeholder="Search entertainment"]')
        self.assertEqual(search_bar.is_enabled(), True)
        search_bar.send_keys('austin')
        search_bar.send_keys(Keys.ENTER)
        
if __name__ == "__main__":
    if len(sys.argv) > 1:
        url = sys.argv[1]    
    unittest.main(argv=['first-arg-is-ignored'], warnings='ignore')
    
