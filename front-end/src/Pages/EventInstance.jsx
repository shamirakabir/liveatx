import React, { useEffect, useState }	from 'react';
import {
    Button,
    Card,
    CardContent,
    CardMedia,
    Grid,
    Typography,
}					from '@material-ui/core';
import MapComponent			from '../Components/MapComponent.jsx';
import Nearby				from '../Components/Nearby.jsx';
import Placeholder			from '../Components/Placeholder.jsx';
import { getAPI }			from '../Library/Query.jsx';
import styles				from '../Styles/Instance.module.css';
import 'antd/dist/antd.css';

require('datejs');

const api_url = process.env.REACT_APP_NOT_SECRECT_CODE;
const geocoding_url = 'https://maps.googleapis.com/maps/api/geocode';
const key = 'AIzaSyBvA8E2EteyZApEyKJ_dp8p0EVcsCLyJAU';

const EventInstance = () => {
    const [data, setData]	= useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError]	= useState(null);
    const [pos, setPos]		= useState(null);
    const [date, setDate]       = useState(null);

    useEffect(() => {
        const fetch = async () => {
            setLoading(true);
            const url = window.location.href;
            const id  = url.substring(url.lastIndexOf('/')+1);
            getAPI(`${api_url}/events/id=${id}`)
                .then(response => {
                    setData(response.data);
                    return response.data;
                })
                .then(data => {
                    setDate(new Date(data.datetime_utc));
                    return data;
                })
                .then(data => getAPI(`${geocoding_url}/json?address=${data.address}&key=${key}`))
                .then(response => setPos(response.data.results[0].geometry.location))
                .catch(setError)
                .finally(() => setLoading(false));
        };
        fetch();
    }, []);

    if (error) return <Placeholder.Error err={error} />;
    if (loading) return <div style={{height: '80vh'}}><Placeholder.Loading /></div>;
    return (
        <React.Fragment>
          <div className={styles.content}>
            <br></br>
            <br></br>
            <div className='row'>
              <div className='col-sm'>
                <Grid
                  container
                  spacing={24}
                  justify='center'
                  style={{minHeight: '100vh', maxWidth: '100%'}}
                >
                  <Grid item xs={4}>
                    <div className='col-sm'>
                      <Card
                        style={{
                            width: '631px',
                            top: '158px',
                            left: '34px',
                            height: '712px',
                            position: 'absolute',
                        }}
                      >
                        <CardContent>
                          <Typography><h3> Location</h3></Typography>
                          Venue: {data.venue}
                          <br></br>
                          Location: {data.address}, Austin TX {data.zipcode}
                          <br></br>
                          <br></br>
                          <br></br>
                          <br></br>
                          <div className={styles.map}>
                            <MapComponent
                              position={pos}
                              markers={[{...pos, noWindow: true}]}
                            />
                          </div>
                        </CardContent>
                      </Card>
                    </div>
                  </Grid>
                  <Grid item xs={4}>
                    <div className='col-sm'>
                      <Card
                        style={{
                            width: '746px',
                            top: '158px',
                            left: '683px',
                            height: '402px',
                            position: 'absolute',
                        }}
                      >
                        <CardContent>
                          <Typography><h3>About {data.name}</h3></Typography>
                          <CardMedia
                            component='img'
                            height='200'
                            width='200'
                            src={data.images[0]}
                            alt='data.name'
                          />
                          <br></br>
                          <div
                            className='grid'
                            style={{alignContent: 'center'}}
                          >
                            <div className='row align-items-center'>
                              <div className='col-xl-6'>
                                Average Ticket Price {data.average_ticket_price}
                                <br></br>
                                Event Date: {date.toString('MMMM d yyyy hh:mm tt')}
                                <br></br>
                                Event Type: {data.event_type}
                                <br></br>
                                <Button href={data.url}>
                                  Event Page
                                </Button>
                                <br></br>
                                <br></br>
                                <br></br>
                                <br></br>
                              </div>
                            </div>
                          </div>
                        </CardContent>                        
                      </Card>
                    </div>
                  </Grid>
                  <Grid item xs={4}>
                    <div className='col-sm'>
                      <Card
                        style={{
                            width: '359px',
                            top: '578px',
                            left: '683px',
                            height: '292px',
                            position: 'absolute',
                        }}
                      >
                        <CardContent>
                          <Typography><h3> Entertainment Nearby </h3></Typography>
                          <div style={{listStyle: 'none'}}>
                            <Nearby.Entertainment distance={data.zipcode} />
                          </div>
                          <br></br>
                          <br></br>
                          <br></br>                          
                        </CardContent>
                      </Card>
                    </div>
                  </Grid>
                  <Grid item xs={4}>
                    <div className='col-sm'>
                      <Card
                        style={{
                            width: '359px',
                            top: '578px',
                            left: '1061px',
                            height: '292px',
                            position: 'absolute',
                        }}
                      >
                        <CardContent>
                          <Typography><h3> Restaurants Nearby </h3></Typography>
                          <div style={{listStyle: 'none'}}>
                            <Nearby.Restaurants distance={data.zipcode} />
                          </div>
                          <br></br>
                          <br></br>
                          <br></br>                          
                        </CardContent>
                      </Card>
                    </div>
                  </Grid>
                </Grid>
              </div>
            </div>
          </div>
        </React.Fragment>
    );    
};

export default EventInstance;
