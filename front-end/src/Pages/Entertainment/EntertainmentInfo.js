export const attributes = [
    {
      name: "Name",
      multiple: false,
      // fields: ["A-Z", "Z-A"],
      fields: ["Coming Soon"],
      disabled: true,
    },
    {
      name: "Rating",
      multiple: true,
      // fields: ["Basketball", "Soccer", "American Football"],
      fields: ["Coming Soon"],
      disabled: true,
    },
    {
      name: "Number of Reviews",
      multiple: true,
      fields: ["Coming Soon"],
      disabled: true,
    },
    {
      name: "Price",
      multiple: true,
      fields: ["Coming Soon"],
      disabled: true,
    },
    {
      name: "Open Now",
      multiple: true,
      fields: ["Coming Soon"],
      disabled: true,
    },
  ];
  