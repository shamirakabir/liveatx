import React, { Component } from 'react';
import { Card, CardActions, CardContent, CardMedia, Button, Typography, Grid } from '@material-ui/core';
import axios from 'axios';
import MapComponent from '../../Components/MapComponent.jsx'
import "./Entertainment.css";

// Credit to Around ATX Group from Last Semester

class EntertainmentInstance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            entertainmentData: null,
            restaurantData: null,
            eventData: null
        }
    }

    async componentDidMount() {
        const url = window.location.href
        const idStartIndex = url.lastIndexOf('/') + 1
        const id = url.substring(idStartIndex)
        const url_string = `${process.env.REACT_APP_NOT_SECRECT_CODE}/entertainment/id=${id}`;
        const entertainmentRes = await axios.get(url_string);
        const url_rest = `${process.env.REACT_APP_NOT_SECRECT_CODE}/restaurants/distance=${entertainmentRes.data['distance']}`
        const restaurantRes = await axios.get(url_rest);
        const url_event = `${process.env.REACT_APP_NOT_SECRECT_CODE}/events/zipcode=${entertainmentRes.data['zipcode']}`
        // const url_event = "http://localhost/api/events/zipcode=" + restaurantRes.data['zipcode']
        const eventsRes = await axios.get(url_event);

        this.setState({
            restaurantData: restaurantRes.data,
            entertainmentData: entertainmentRes.data,
            eventData: eventsRes.data
        })
    }

    displayRestaurant() {
        if (this.state.restaurantData == null) {
            return null;
        } else if (this.state.restaurantData.length === 0) {
            return (<p>No restaurants found.</p>)
        }
        var item = []
        for (var i = 0; i < this.state.restaurantData.length; i++) {
            let rest = this.state.restaurantData[i];
            item.push (
                <React.Fragment>
                    <li>
                        <Button href={"/restaurants/" + rest["restaurant_id"]}>{rest["name"]}</Button>
                    </li>
                </React.Fragment>
            );
        }
        return item.slice(0, 5);
    }

    displayEvent() {
        if (this.state.eventData == null) {
            return null;
        } else if (this.state.eventData.length === 0) {
            return (<p>No events found.</p>)
        }
        var item = []
        for (var i = 0; i < this.state.eventData.length; i++) {
            let event = this.state.eventData[i];
            item.push (
                <React.Fragment>
                    <li>
                        <Button href={"/events/" + event["event_id"]}>{event["name"]}</Button>
                    </li>
                </React.Fragment>
            );
        }
        return item.slice(0, 5);
    }

    render() {
        let restaurantData = this.state.restaurantData;
        let entertainmentData = this.state.entertainmentData;
        let eventData = this.state.eventData;
        if (restaurantData == null || entertainmentData == null || eventData == null) {
            <div>
                <h2><br></br><br></br>Loading....</h2>
            </div>
            return null
        }

    return (
        <React.Fragment>
          <div style={{margin: 'auto', marginTop: '10vh', width: '90%'}}>
	    <br></br><br></br>
	    <div className="row">
	      <div className="col-sm">
		<Grid
		  container spacing={24}
		  justify="center"
		  style={{minHeight: '100vh', maxWidth: '100%'}}>
		  <Grid item xs={4}>
		    <div className="col-sm">
		      <Card style={{width: '631px', top: '158px', left: '34px', height: '712px', position: 'absolute'}}>
			<CardContent>
			  <Typography><h3>Location</h3></Typography>
			  {'Address: ' + entertainmentData.address + ', ' + entertainmentData.city + ' ' + entertainmentData.state + ' ' + entertainmentData.zipcode}<br></br>
			  {'Latitude: ' + entertainmentData.latitude}<br></br>
			  {'Longitude: ' + entertainmentData.longitude}<br></br>
			  <br></br><br></br><br></br>
			  <MapComponent
			    lat={entertainmentData.latitude}
			    lng={entertainmentData.longitude}
			    containerStyle={{position: 'relative', height: '400px', width: '600px'}}
			    />
			</CardContent>
		      </Card>
		    </div>
		  </Grid>
		</Grid>
		<Grid item xs={4}>
		  <div className="col-sm">
		    <Card style={{width: '746px', top: '158px', left: '683px', height: '402px', position: 'absolute'}}>
		      <CardContent>
			<Typography><h3>About {entertainmentData.name}</h3></Typography>
			<CardMedia
			  component='img'
			  height='200'
			  width='200'
			  src={entertainmentData.image_url}
			  alt={entertainmentData.name}
			  />
			<br></br>
			<div class='grid' style={{alignContent: 'center'}}>
			  <div class='row align-items-center'>
			    <div class='col-xl-6'>
			      {"Price Level: " + entertainmentData.price}<br></br>
                              {"Phone: " + entertainmentData.phone}<br></br>
                              <Button href={entertainmentData.url}>Yelp Page</Button><br></br>
			      <br></br><br></br><br></br>
			    </div>
			  </div>
			</div>
		      </CardContent>
		    </Card>
		  </div>
		</Grid>
		<Grid item xs={4}>
		  <div className='col-sm'>
		    <Card style={{width: '359px', top: '578px', left: '683px', height: '292px', position: 'absolute'}}>
		      <CardContent>
			<Typography><h3> Events Nearby </h3></Typography>
			<div style={{listStyle: 'none'}}>{this.displayEvent()}</div>
			<br></br><br></br><br></br>
		      </CardContent>
		    </Card>
		  </div>
		</Grid>
		<Grid item xs={4}>
		  <div className='col-sm'>
		    <Card style={{width: '359px', top: '578px', left: '1061px', height: '292px', position: 'absolute'}}>
		      <CardContent>
			<Typography><h3> Restaurants Nearby </h3></Typography>
			<div style={{listStyle: 'none'}}>{this.displayRestaurant()}</div>
			<br></br><br></br><br></br>
		      </CardContent>
		    </Card>
		  </div>
		</Grid>
	      </div>
	    </div>
	  </div>
	</React.Fragment>
    )

    }

}

export default EntertainmentInstance;
