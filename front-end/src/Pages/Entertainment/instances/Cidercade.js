import React from 'react';
import { Card, CardActions, CardContent, CardMedia, CardHeader, Button, Typography, Grid } from '@material-ui/core';
import "./style.css";
import CidercadeMap from "./map-cidercade.png"
import MapComponent from "../../../Components/MapComponent.js"

function Cidercade() {
    return (
        <div>
            <h1>Cidercade Austin</h1>
            <Grid
                container
                justify="center"
            >
                <Card className="card">
                    {/* <CardMedia>

                    </CardMedia> */}
                    <CardContent>
                        <Typography>
                            Location: 30.2525, -97.74104
                        </Typography>
                        <Typography>
                            Address: 600 E Riverside Dr
                        </Typography>
                        <Typography>
                            Hours: Su-Th: 10AM-12AM, Fr-Sa: 10AM-1AM
                        </Typography>
                        <Typography>
                            Prices: $$
                        </Typography>
                        <Typography>
                            Nearby Event: 
                        </Typography>
                        <Button color="primary" size="small" href="/event/worldcupqualifier">
                            World Cup Qualifier
                        </Button>
                        <Typography>
                            Nearby Restaurant: 
                        </Typography>
                        <Button color="primary" size="small" href="/restaurant/roppolos">
                            Roppolo's Pizza
                        </Button>
                    </CardContent>
                </Card>
                <Card className="card">
                    <CardMedia
                        component="img"
                        height="200"
                        image={'https://s3-media4.fl.yelpcdn.com/bphoto/7q_a2PHab9HyQVo9Dxuo6g/o.jpg'}
                        alt="Cidercade"
                    />
                </Card>
            <Card className="card">
	    <CardContent>
	    <MapComponent
	lat="30.2525"
	lng="-97.74104"
	containerStyle={{position: 'relative', height: '200px', width: '300px'}}/>
	    </CardContent>
                </Card>
            </Grid>
        </div>
    );
}

export default Cidercade;
