import React from 'react';
import { Card, CardActions, CardContent, CardMedia, Button, Typography, Grid } from '@material-ui/core';
import UnionMap from "./map-union.png"


function Union() {
    return (
        <div>
            <h1>Union Underground</h1>
            <Grid
                container
                justify="center"
            >
                <Card className="card">
                    <CardContent>
                        <Typography>
                            Location: 30.2855534, -97.7415957
                        </Typography>
                        <Typography>
                            Address: 2247 Guadalupe St
                        </Typography>
                        <Typography>
                            Hours: M-S: 10AM-12AM, Su: 12PM-12AM
                        </Typography>
                        <Typography>
                            Prices: $
                        </Typography>
                        <Typography>
                            Nearby Event: 
                        </Typography>
                        <Button color="primary" size="small" href="/event/tallestman">
                            Tallest Man On Earth
                        </Button>
                        <Typography>
                            Nearby Restaurant: 
                        </Typography>
                        <Button color="primary" size="small" href="/restaurant/don">
                            Don Japanese
                        </Button>
                    </CardContent>
                </Card>
                <Card className="card">
                    <CardMedia
                        component="img"
                        height="200"
                        image={'https://s3-media2.fl.yelpcdn.com/bphoto/Tla5Mn9adKHVPIu0a6uwCA/o.jpg'}
                        alt="Union"
                    />
                </Card>
                <Card className="card">
                    <CardMedia
                        component="img"
                        height="200"
                        image={UnionMap}
                        alt="Union"
                    />
                </Card>
            </Grid>
        </div>
    )  
}

export default Union;