import React from 'react';
import { Card, CardActions, CardContent, CardMedia, Button, Typography, Grid } from '@material-ui/core';
import ZiplineMap from "./map-ziplining.png"

function Zipline() {
    
    return (
        <div>
            <h1>Lake Travis Zipline Adventures</h1>
            <Grid
                container
                justify="center"
            >   
                <Card className="card">
                    <CardContent>
                        <Typography>
                            Location: 30.42915, -97.89014
                        </Typography>
                        <Typography>
                            Address: 14529 Pocohontas Trl
                        </Typography>
                        <Typography>
                            Hours: Wed-Mon: 8AM-6PM
                        </Typography>
                        <Typography>
                            Prices: $
                        </Typography>
                        <Typography>
                            Nearby Event: 
                        </Typography>
                        <Button color="primary" size="small" href="/event/poppy">
                            Poppy
                        </Button>
                        <Typography>
                            Nearby Restaurant: 
                        </Typography>
                        <Button color="primary" size="small" href="/restaurant/arturos">
                            Arturos
                        </Button>
                        
                    </CardContent>
                </Card>
                <Card className="card">
                    <CardMedia
                        component="img"
                        height="200"
                        image={'https://s3-media2.fl.yelpcdn.com/bphoto/fYV8q9-yduLe-uV9Ncgktw/o.jpg'}
                        alt="Zipline"
                    />
                </Card>
                <Card className="card">
                    <CardMedia
                        component="img"
                        height="200"
                        image={ZiplineMap}
                        alt="Zipline"
                    />
                </Card>
            </Grid>
        </div>
    )  
}

export default Zipline;