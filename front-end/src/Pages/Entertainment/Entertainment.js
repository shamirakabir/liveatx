import React, {Component, useEffect, useState}	from 'react';
import axios					from 'axios';
import MUIDataTable				from "mui-datatables"; 
import Highlighter				from 'react-highlight-words';
import { getAPI }				from '../library/Data.js';
import { SearchBar }				from '../Search/GeneralSearchBar.js';
import { useHistory }				from 'react-router-dom';
import {
    NumberParam,
    useQueryParams,
    withDefault,
} from 'use-query-params';

// Credit to Around ATX Group from Last Semester

function Entertainment() {
    const history			= useHistory();
    const [data, setData]		= useState([]);
    const [searchText, setSearchText]	= useState('');
    const [load, setLoad]		= useState(false);
    const [tempSearch, setTempSearch]	= useState('');
    const [params, setParams]		= useQueryParams({pg: withDefault(NumberParam, 1)});
    
    useEffect(() => {
	const fetchData = async () => {
	    await getAPI(process.env.REACT_APP_NOT_SECRECT_CODE+'/entertainment').then(response => {
		setData(response.data.entertainment);
		setLoad(true);
	    }).catch((error) => {
		setData([]);
		setLoad(true);
	    });
	}
	fetchData();
    }, []);

    const handlePageChange = (num) => {
	setParams(params => ({...params, pg: num}));
	history.push('/entertainment?pg='+num);
    }
    
    const EntertainmentCustomBodyRender = (value, tableMeta, updateValue) => (
      <div>
        <Highlighter
          highlightClassName="highlight-class"
          searchWords={[searchText]}
          textToHighlight={value + ""}
          ></Highlighter>
      </div>
    );

    const columns = [
	{
            name: 'entertainment_id',
            label: 'Entertainment id',
            options: {
		filter: false,
		sort: false,
		display: "excluded",
            },
	},
	{
            name: 'name',
            label: 'Name',
            options: {
		filter: true,
		sort: true,
		filterType: 'checkbox',
		filterOptions: { 
                    names: ['A-I', 'J-R', 'S-Z'],
                    logic(name, filters) {
			const show = 
                              (filters.indexOf('A-I') >= 0 && 
                               name.charCodeAt(0) >= "A".charCodeAt(0) && 
                               name.charCodeAt(0) <= "I".charCodeAt(0)) ||
                              (filters.indexOf('J-R') >= 0 && 
                               name.charCodeAt(0) >= "J".charCodeAt(0) && 
                               name.charCodeAt(0) <= "R".charCodeAt(0)) ||              
                              (filters.indexOf('S-Z') >= 0 && 
                               name.charCodeAt(0) >= "S".charCodeAt(0) && 
                               name.charCodeAt(0) <= "Z".charCodeAt(0));
			return !show;
                    }
		}
            }
	},
	{
            name: 'rating',
            label: 'Rating (out of 5)',
            options: {
		filter: true,
		sort: true,
		filterType: 'checkbox',
		filterOptions: {
                    names: ['3', '3.5', '4', '4.5', '5'],
                    logic(rating, filters) {
			const show =
			      (filters.indexOf('3') >= 0 && rating === 3) ||
			      (filters.indexOf('3.5') >= 0 && rating === 3.5) ||
			      (filters.indexOf('4') >= 0 && rating === 4) ||
			      (filters.indexOf('4.5') >= 0 && rating === 4.5) ||
			      (filters.indexOf('5') >= 0 && rating === 5)
			return !show;
                    }
		},
            }
	},
	{
            name: 'review_count',
            label: 'Review Count',
            options: {
		filter: true,
		sort: true,
		filterType: 'checkbox',
		filterOptions: {
                    names: ["Low", "Medium", "High"],
                    logic(review_count, filters) {
			const show =
			      (filters.indexOf("Low") >= 0 && review_count < 1700) ||
			      (filters.indexOf("Medium") >= 0 && review_count >= 1700 && review_count < 3400) ||
			      (filters.indexOf("High") >= 0 && review_count >= 3400);
			return !show;
                    }
		},
		customBodyRender: (value, tableMeta, updateValue) =>
                    EntertainmentCustomBodyRender(value, tableMeta, updateValue),
            }
	},
	{
            name: 'price',
            label: 'Price Level',
            options: {
		filter: true,
		sort: true,
		filterType: 'checkbox',
		filterOptions: {
                    names: ["$", "$$", "$$$"],
                    logic(price, filters) {
			const show =
			      (filters.indexOf("$") >= 0 && price === '$') ||
			      (filters.indexOf("$$") >= 0 && price === '$$') ||
			      (filters.indexOf("$$$") >= 0 && price === '$$$');
			return !show;
                    }
		},
		customBodyRender: (value, tableMeta, updateValue) =>
                    EntertainmentCustomBodyRender(value, tableMeta, updateValue),
            }
	},
	{
            name: 'is_open',
            label: 'Open Now',
            options: {
		filter: true,
		sort: true,
		filterType: 'checkbox',
            },
            customBodyRender: (value, tableMeta, updateValue) =>
                EntertainmentCustomBodyRender(value, tableMeta, updateValue),
	},
    ];

    const options = {
	filter: true,
	filterType: 'multiselect',
	onRowClick: (rowData) => {
	    window.location.assign("/entertainment/" + rowData[0]);
	},
	// search: false,
  customSearch: (query, currentRow) => {
      let found = false;
      if (query.length < 2) {
          query = "";
      }
      setSearchText(query);
      currentRow.forEach((col) => {
        if (col != null && col.toString().toLowerCase().includes(query.toLowerCase())) {
          found = true;
        }
      });
      return found;
  },
  onSearchClose: () => {
      setSearchText("");
  },
	download: false,
	print: false,
	selectableRowsHideCheckboxes: true,
	selectableRowsHeader: false,
	viewColumns: false,
	onChangePage: handlePageChange,
	page: params.pg,
    };

    return (
      <div className="body">
        <div className="table-container">
          <div className="grid" style={{alignContent:'center'}}>
            <div className="row align-items-center">
              <div className="col-xl-4">
                <h2 className="page-title">Entertainment</h2>
                <h3 style={{textAlign:"center"}}>Check out all the Entertainment in Austin.</h3>
                <h6 style={{textAlign:"center"}}>Filter, sort, or search to find an entertainment</h6>
		{/* <SearchBar
		  onChange={text => setTempSearch(text)}
		  onSearch={text => history.push('/entertainment/search?q='+text)}
		  value={tempSearch}
		  placeholder='Search entertainment'
		  /> */}
              </div>
            </div>
          </div>
        </div>
        <div style={{display:'table', tableLayout:'fixed', width:'100%', height:'100%', cursor:'pointer'}}>
          { load ? (
          <>
            <MUIDataTable columns={columns} options={options} data={data}/>
          </>
          ) : (
          <h1 style={{ textAlign: "center", color: "red" }}> Loading </h1>
          )}
        </div>
      </div>
    )

}

export default Entertainment; 
