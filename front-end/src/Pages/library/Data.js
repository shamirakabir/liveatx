import axios from "axios";
import cache from "lru-cache";

const apiCache = new cache();

export const getAPI = async (url) => {
  return checkCache(url) || getData(url);
};

const getData = async (url) => {
  const data = await axios.get(url);
  apiCache.set(url, data);
  console.log(data)
  return data;
};

const checkCache = (hash) => {
  if (!apiCache.has(hash)) {
    return null;
  }
  return apiCache.get(hash);
};
