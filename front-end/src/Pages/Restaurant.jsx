/* external */
import React, { useEffect, useState }	from 'react';
import {
    Link,
    useHistory,
}					from 'react-router-dom';
import {
    useQueryParams,
    ArrayParam,
    NumberParam,
    StringParam,
    withDefault,
}					from 'use-query-params';
import { stringify }			from 'query-string';
import {
    Button,
    Card,
    Col,
    Drawer,
    Input,
    Pagination,
    Row,
    Select,
    Slider,
    Space,
    Spin,
    Switch,
    Table,
    Typography,
}					from 'antd';

/* components */
import Highlight			from '../Components/Highlight.jsx';
import MapComponent			from '../Components/MapComponent.jsx';
import Placeholder			from '../Components/Placeholder.jsx';
import Rating				from '../Components/Rating.jsx';
import RestaurantCard			from '../Components/RestaurantCard.jsx';
import { restaurantTableColumns }	from '../Components/Columns.jsx';

import { query }			from '../Library/Query.jsx';
import styles				from '../Styles/Search.module.css';
import 'antd/dist/antd.css';

const { Meta } = Card;
const { Title } = Typography;

const Restaurant = () => {
    const history			= useHistory();
    const [loading, setLoading]		= useState(true);
    const [data, setData]		= useState([]);
    const [search, setSearch]		= useState(undefined);
    const [isQuery, setIsQuery]		= useState(false);
    const [error, setError]		= useState(null);
    const [displayType, setDisplayType] = useState('grid');
    const [showFilter, setShowFilter]	= useState(false);
    const [letters, setLetters]		= useState([]);
    const [categories, setCategories]	= useState([]);
    const [maxRC, setMaxRC]		= useState(0);
    
    const [params, setParams] = useQueryParams({
	q: withDefault(StringParam, ''),
	page: withDefault(NumberParam, 1),
	itemsPerPage: withDefault(NumberParam, 10),
	sort: withDefault(StringParam, ''),
        name: withDefault(StringParam, ''),
	category: withDefault(ArrayParam, []),
        rating: withDefault(ArrayParam, []),
        minr: withDefault(NumberParam, 1),
        maxr: withDefault(NumberParam, 5),
        is_open: withDefault(StringParam, 'Yes'),
    });
    
    const [sortDescending, setSortDescending]		= useState(params.sort && params.sort.length > 0 && params.sort[0] === '-');
    const [filterName, setFilterName]			= useState(params.name);
    const [filterCategories, setFilterCategories]	= useState(params.category);
    const [filterRating, setFilterRating]		= useState([params.minr, params.maxr]);
    const [filterOpen, setFilterOpen]			= useState(params.is_open);
    
    const handleSearchChange = obj => setSearch(obj.target.value);
    const handleSearch = text => {
	setParams(params => ({...params, q: text}));
	history.push(`/restaurant?${stringify({...params, q: text})}`);
	setIsQuery(true);
    };

    const handlePageChange = num => {
	setParams(params => ({...params, page: num}));
	history.push(`/restaurant?${stringify({...params, page: num})}`);
    };

    const handlePagination = (page, pageSize) => {
	setParams(params => ({...params, page: page, itemsPerPage: pageSize}));
	history.push(`/restaurant?${stringify({...params, page: page, itemsPerPage: pageSize})}`);
    };

    const handleSort = (value, option) => {        
	setParams(params => ({...params, q: sortDescending ? '-' + value : value}));
	history.push(`/restaurant?${stringify({...params, sort: sortDescending ? '-' + value : value})}`);
    };

    const handleSortDirection = () => {
        const value = params.sort.substring(sortDescending ? 1 : 0); 
        setSortDescending(!sortDescending);
        setParams(params => ({...params, q: sortDescending ? value : '-' + value}));
	history.push(`/restaurant?${stringify({...params, sort: sortDescending ? value : '-' + value})}`);
    };

    const handleFilterName = (value, options) => {
        const values = options.map(x => x.value);
        setFilterName(`^[${values.join('|')}]`);
        if (values.length === 0)
            setFilterName('');
    };
    
    const handleFilterCategory = (value, options) => {
	const values = options.map(x => x.value);
        setFilterCategories(values);
    };

    const handleApplyFilter = () => {
        var ratings = [];
        for (var i = filterRating[0]; i <= filterRating[1]; i += 0.5)
            ratings.push(i);        
        const newParams = params => ({
            ...params,
            name: filterName,
            category: filterCategories,
            rating: ratings,
            minr: filterRating[0],
            maxr: filterRating[1],
            is_open: filterOpen,
        });
        setParams(newParams);
        history.push(`/restaurant?${stringify(newParams(params))}`);
    };   

    useEffect(() => {
	setLoading(true);
	setIsQuery(params.q ? true : false);
	const fetch = async () => {
	    query('restaurants', params)
                .then(response => setData(response.data))
                .then(() => query('restaurants'))
                .then(response => {
		    const data = response.data.restaurants;
                    setLetters([...new Set(data.map(x => x.name[0]))].sort());
		    setCategories([...new Set(data.map(x => x.category))]);
		})
	        .catch(error => {
		    console.error(error);
		    setError(error);
	        })
                .finally(() => setLoading(false));
	};
	fetch();
    }, [params, history]);

    const Hi = (props) => (
	<Highlight
	  words={[params.q]}
	  text={props.text} />
    );

    const DisplayGrid = () => {
	if (data.restaurants.length === 0)
	    return <Typography>No results found.</Typography>;
	return (
	    <div className={styles.grid}>
	      {
		  data.restaurants.map(result => (
		      <RestaurantCard
			query={params.q}
			{...result}
		      />
		  ))
	      }
	    </div>
	);
    };

    const DisplayTable = () => {
	if (data.restaurants.length === 0)
	    return <Typography>No results found.</Typography>;
	return (
	    <Space className={styles.table} direction='vertical'>
	      <Table
		dataSource={data.restaurants}
		columns={restaurantTableColumns(params.q)}
		pagination={false}
	      />
	    </Space>
	);
    };

    const DisplayMap = () => {
	var center = { lat: 0, lng: 0, };
	if (data.restaurants.length === 0)
	    return <Typography>No results found.</Typography>;
	const markerData = data.restaurants.map(result => {
	    center.lat += result.latitude;
	    center.lng += result.longitude;
	    return {
		name: result.name,
		title: result.name,
		position: { lat: result.latitude, lng: result.longitude, },
		info: (
		    <RestaurantCard.Mini
		      query={query}
		      {...result}
		    />
		),
	    };
	});
	center.lat /= data.restaurants.length;
	center.lng /= data.restaurants.length;
	return(
	    <div className={styles.center}>
	      <div
		style={{
		    position: 'relative',
		    height: '50vh',
		    width: '100%',
		}}>
		<MapComponent
		  position={center}
		  zoom={12}
		  markers={markerData}
		/>
	      </div>
	    </div>
	);
    };

    if (error)
	return <Placeholder.Error err={error}/>;

    return (
	<React.Fragment>
	  <section className={styles.content}>
	    <div className={styles.contentHeader}>
	      <Typography.Title level={1}>
		{ isQuery ? (
		    'Results for "' + params.q + '"'
		) : (
		    'Restaurants'
		) }
	      </Typography.Title>
	      <div className='searchBar'>
		<Input.Search
		  size='large'
		  placeholder='Search restaurants'
		  enterButton='Search'
		  onChange={handleSearchChange}
		  onSearch={handleSearch}
		  value={search}
		  loading={loading}
		/>
	      </div>
	    </div>
	    {
		loading ? (
		    <div className={styles.center}>
		      <Spin size='large' />
		    </div>
		) : (
		    <React.Fragment>
		      <div className={styles.results}>
			<Drawer
			  title='Filters'
			  placement='bottom'
			  onClose={() => setShowFilter(false)}
			  visible={showFilter}
			  extra={
			      <Space>
				<Button type='primary' onClick={handleApplyFilter}>Apply</Button>
				<Button onClick={() => setShowFilter(false)}>Cancel</Button>
			      </Space>
			  }
			>
			  <Row style={{width: '100%'}}>
                            <Col span={12}>
                              <Space
                                direction='vertical'
                                style={{width: '90%'}}
                                size='large'
                              >
			        <Select
				  showSearch
				  allowClear
				  style={{width: '100%'}}
				  mode='multiple'
				  placeholder='Name'
				  defaultValue={params.name.split('').filter(c => c >= 'A' && c <= 'Z')}
				  onChange={handleFilterName}
				  optionFilterProp='label'
				  options={letters.map(c => ({label: c, value: c}))}
			        />
                                <Select
				  showSearch
				  allowClear
				  style={{width: '100%'}}
				  mode='multiple'
				  placeholder='Category'
				  defaultValue={params.category}
				  onChange={handleFilterCategory}
				  optionFilterProp='label'
				  options={categories.map(cat => ({label: cat, value: cat}))}
			        />
                                <div style={{width: '100%'}}>
                                  {'Open Now: '}
                                  <Switch
                                    checked={filterOpen === 'Yes'}
                                    onChange={checked => setFilterOpen(checked ? 'Yes' : 'No')}
                                  />
                                </div>
                                </Space>
                              </Col>
                            <Col span={12}>
                              <Space
                                direction='vertical'
                                style={{width: '90%'}}
                                size='large'
                              >
                                <Row style={{width: '100%'}}>
                                  <Col flex='none'>
                                    Rating:
                                  </Col>
                                  <Col span={1} />
                                  <Col flex={1}>
                                    <Slider
                                      range
                                      min={1}
                                      max={5}
                                      value={filterRating}
                                      step={0.5}
                                      onChange={setFilterRating}
                                    />
                                  </Col>                    
                                </Row>
                              </Space>
			    </Col>
			    <Col span={12}>
			    </Col>
			  </Row>
			</Drawer>
			<Row justify='end'>
			  <Col flex={1}>
			    <Select
			      showSearch
			      style={{width: '100%'}}
			      placeholder='Sort by'
			      onChange={handleSort}
                              defaultValue={params.sort.substring(sortDescending ? 1 : 0) === '' ?
                                            null : params.sort.substring(sortDescending ? 1 : 0)}
			      optionFilterProp='label'
			      options={[
				  {label: 'None', value: null},
				  {label: 'Name', value: 'name'},
				  {label: 'Category', value: 'category'},
				  {label: 'Rating', value: 'rating'},
				  {label: 'Review Count', value: 'review_count'},
				  {label: 'Open Now', value: 'is_open'},
			      ]}
			    />
			  </Col>
                          <Col flex='none'>
                            <Button onClick={handleSortDirection}>
                              {sortDescending ? 'Descending' : 'Ascending'}
                            </Button>
                          </Col>
			  <Col flex='none'>
			    <Button onClick={() => setShowFilter(true)}>
			      Filters
			    </Button>
			  </Col>
			  <Col span={1} />
			  <Col flex='none'>
			    <Pagination
			      size='small'
			      current={params.page}
			      pageSize={params.itemsPerPage}
			      total={data.numberOfPages * params.itemsPerPage}
			      onChange={handlePagination}
			      showSizeChanger
			    />
			  </Col>
			  <Col span={1} />
			  <Col flex='none'>
			    <Space>
			      <div>Display as </div>
			      <Button
				onClick={event => setDisplayType('grid')}
				disabled={displayType === 'grid'}
			      >
				Grid
			      </Button>
			      <Button
				onClick={event => setDisplayType('table')}
				disabled={displayType === 'table'}
			      >
				Table
			      </Button>
			      <Button
				onClick={event => setDisplayType('map')}
				disabled={displayType === 'map'}
			      >
				Map
			      </Button>
			    </Space>
			  </Col>
			</Row>
			<br></br>
			{ (displayType === 'grid')  ? <DisplayGrid />  : null }
			{ (displayType === 'table') ? <DisplayTable /> : null }
			{ (displayType === 'map')   ? <DisplayMap />   : null }
		      </div>
		    </React.Fragment>
		)
	    }
	  </section>
	</React.Fragment>
    );
};

export default Restaurant;
