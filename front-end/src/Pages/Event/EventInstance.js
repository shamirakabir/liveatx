import React, { Component } from 'react';
import { Card, CardActions, CardContent, CardMedia, Button, Typography, Grid } from '@material-ui/core';
import axios from 'axios';
import MapComponent from '../../Components/MapComponent.jsx';
import "./Event.css";

require('datejs');

// Credit to Around ATX Group from Last Semester

class EventInstance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            entertainmentData: null,
            restaurantData: null,
            eventData: null,
	    location: null
        }
    }

    async componentDidMount() {
        const url = window.location.href
        const idStartIndex = url.lastIndexOf('/') + 1
        const id = url.substring(idStartIndex)
        const url_string = `${process.env.REACT_APP_NOT_SECRECT_CODE}/events/id=${id}`
        const eventRes = await axios.get(url_string);
        const url_rest = `${process.env.REACT_APP_NOT_SECRECT_CODE}/restaurants/distance=${eventRes.data['zipcode']}`
        const restaurantRes = await axios.get(url_rest);
        const url_ent = `${process.env.REACT_APP_NOT_SECRECT_CODE}/entertainment/distance=${eventRes.data['zipcode']}`
        const entertainmmentRes = await axios.get(url_ent);
        const address = eventRes.data.address;
        const key = 'AIzaSyBvA8E2EteyZApEyKJ_dp8p0EVcsCLyJAU';
        const geocoding_url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + key;
	    const locationRes = await axios.get(geocoding_url);


	console.log(locationRes.status);
        this.setState({
            eventData: eventRes.data,
	        location: locationRes.data.results[0].geometry.location,
            entertainmentData: entertainmmentRes.data,
            restaurantData: restaurantRes.data
        })
    }

    displayEntertainment() {
        if (this.state.entertainmentData == null) {
            return null;
        } else if (this.state.entertainmentData.length === 0) {
            return (<p>No entertainment found.</p>)
        }
        var item = []
        for (var i = 0; i < this.state.entertainmentData.length; i++) {
            let ent = this.state.entertainmentData[i];
            item.push (
                <React.Fragment>
                    <li>
                        <Button href={"/entertainment/" + ent["entertainment_id"]}>{ent["name"]}</Button>
                    </li>
                </React.Fragment>
            );
        }
        return item.slice(0, 5);
    }

    displayRestaurant() {
        if (this.state.restaurantData == null) {
            return null;
        } else if (this.state.restaurantData.length === 0) {
            return (<p>No restaurants found.</p>)
        }
        var item = []
        for (var i = 0; i < this.state.restaurantData.length; i++) {
            let rest = this.state.restaurantData[i];
            item.push (
                <React.Fragment>
                    <li>
                        <Button href={"/restaurants/" + rest["restaurant_id"]}>{rest["name"]}</Button>
                    </li>
                </React.Fragment>
            );
        }
        return item.slice(0, 5);
    }
    
    render() {
        let entertainmentData = this.state.entertainmentData;
        let eventData = this.state.eventData;
        let restaurantData = this.state.restaurantData;
        if (restaurantData == null || entertainmentData == null || eventData == null) {
            <div>
                <h2><br></br><br></br>Loading....</h2>
            </div>
            return null
        }
	let location = this.state.location;
	var date = new Date(eventData.datetime_utc);	
	return (
          <React.Fragment>
            <div style={{margin: 'auto', marginTop: '10vh', width: '90%'}}>
              <br></br><br></br>
              <div className="row">
                <div className="col-sm">
                  <Grid
                    container
                    spacing={24}
                    justify="center"
                    style={{ minHeight: '100vh', maxWidth: '100%'}}
                    >
                    <Grid item xs={4}>
                      <div className="col-sm" >
                        <Card style={{ width: "631px", top: "158px", left: "34px", height: "712px", position: "absolute"}}>
                          <CardContent>
                            <Typography ><h3>Location</h3></Typography>
                            {"Venue: " + eventData.venue}<br></br>
                            {"Location: " + eventData.address + ", Austin TX " + eventData.zipcode}<br></br><br></br><br></br><br></br>
                            <MapComponent
                              lat={location.lat}
                              lng={location.lng}
                              containerStyle={{position: 'relative', height: '400px', width: '600px'}}/>
                          </CardContent>
                        </Card>
                      </div>
                    </Grid>
                    <Grid item xs={4}>
                      <div className="col-sm" >
                        <Card style={{ width: "746px", top: "158px", left: "683px", height: "402px", position: "absolute"}}>
                          <CardContent>
                            <Typography><h3>About {eventData.name}</h3></Typography>
                            <CardMedia
                              component="img"
                              height="200"
                              image={eventData.images[0]}
                              alt={eventData.name}
                              />
                            <br></br>
                            <div class="grid" style={{alignContent:'center'}}>
                              <div class="row align-items-center">
                                <div class="col-xl-6">
                                  {"Average Ticket Price: " + eventData.average_ticket_price}<br></br>
				  {"Event Date: " + date.toString('MMMM d yyyy hh:mm tt')}<br></br>
                                  {"Event Type: " + eventData.event_type}<br></br>
                                  <Button href={eventData.url}>Event Page</Button><br></br><br></br><br></br><br></br>
                                </div>
                              </div>
                            </div>
                          </CardContent>
                        </Card>
                      </div>
                    </Grid>
                    <Grid item xs={4}>
                      <div className="col-sm" >
                        <Card style={{ width: "359px", top: "578px", left: "683px", height: "292px", position: "absolute"}}>
                          <CardContent>
                            <Typography><h3> Entertainment Nearby </h3></Typography>
                            <div style={{listStyle:"none"}}>{this.displayEntertainment()}</div><br></br><br></br><br></br>
                          </CardContent>
                        </Card>
                      </div>
                    </Grid>
                    <Grid item xs={4}>
                      <div className="col-sm" >
                        <Card style={{ width: "359px", top: "578px", left: "1061px", height: "292px", position: "absolute"}}>
                          <CardContent>
                            <Typography><h3> Restaurants Nearby </h3></Typography>
                            <div style={{listStyle:"none"}}>{this.displayRestaurant()}</div><br></br><br></br><br></br>
                          </CardContent>
                        </Card>
                      </div>
                    </Grid>
                  </Grid>
		</div> 
              </div>
            </div>
	  </React.Fragment> 
	);	
    }
}

export default EventInstance;
