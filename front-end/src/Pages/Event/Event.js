import React, {Component, useEffect, useState}	from 'react';
import axios					from 'axios';
import MUIDataTable				from "mui-datatables"; 
import Highlighter				from 'react-highlight-words';
import { getAPI }				from '../library/Data.js';
import { SearchBar }				from '../Search/GeneralSearchBar';
import { useHistory }				from 'react-router-dom';
import {
    NumberParam,
    useQueryParams,
    withDefault,
} from 'use-query-params';

// Credit to Around ATX Group from Last Semester

function Event() {
    const history			= useHistory();
    const [data, setData]		= useState([]);
    const [searchText, setSearchText]	= useState("");
    const [load, setLoad]		= useState(false);
    const [tempSearch, setTempSearch]	= useState("");
    const [params, setParams]		= useQueryParams({pg: withDefault(NumberParam, 1)});
    
    useEffect(() => {
	const fetchData = async () => {
	    await getAPI(process.env.REACT_APP_NOT_SECRECT_CODE+'/events').then(response => {
		setData(response.data.events)
		setLoad(true)
	    }).catch((error) => {
		setData([])
		setLoad(true)
	    });
	}
	fetchData();
    }, []);
    
    const handlePageChange = (num) => {
	setParams(params => ({...params, pg: num}));
	history.push('/event?pg='+num);
    } 
    
    const eventCustomBodyRender = (value, tableMeta, updateValue) => (
	    <div>
            <Highlighter
        highlightClassName="highlight-class"
        searchWords={[searchText]}
        textToHighlight={value + ""}
            ></Highlighter>
	    </div>
    );

    const columns = [
	{
            name: 'event_id',
            label: 'Event id',
            options: {
		filter: false,
		sort: false,
		display: "excluded",
            },
	},
	{
            name: 'name',
            label: 'Name',
            options: {
		filter: true,
		sort: true,
		filterType: 'checkbox',
		filterOptions: { 
                    names: ['A-I', 'J-R', 'S-Z'],
                    logic(name, filters) {
			const show = 
                              (filters.indexOf('A-I') >= 0 && 
                               name.charCodeAt(0) >= "A".charCodeAt(0) && 
                               name.charCodeAt(0) <= "I".charCodeAt(0)) ||
                              (filters.indexOf('J-R') >= 0 && 
                               name.charCodeAt(0) >= "J".charCodeAt(0) && 
                               name.charCodeAt(0) <= "R".charCodeAt(0)) ||              
                              (filters.indexOf('S-Z') >= 0 && 
                               name.charCodeAt(0) >= "S".charCodeAt(0) && 
                               name.charCodeAt(0) <= "Z".charCodeAt(0));
			return !show;
                    }
		}, 
		customBodyRender: (value, tableMeta, updateValue) =>
		    eventCustomBodyRender(value, tableMeta, updateValue),
            }
	},
	{
            name: 'event_type',
            label: 'Event Type',
            options: {
		filter: true,
		sort: true,
		filterType: 'checkbox',
		filterOptions: {
		    names: ['A-I', 'J-R', 'S-Z'],
		    logic(name, filters) {
			const show = 
			      (filters.indexOf('A-I') >= 0 && 
                               name.charCodeAt(0) >= "A".charCodeAt(0) && 
                               name.charCodeAt(0) <= "I".charCodeAt(0)) ||
			      (filters.indexOf('J-R') >= 0 && 
                               name.charCodeAt(0) >= "J".charCodeAt(0) && 
                               name.charCodeAt(0) <= "R".charCodeAt(0)) ||              
			      (filters.indexOf('S-Z') >= 0 && 
                               name.charCodeAt(0) >= "S".charCodeAt(0) && 
                               name.charCodeAt(0) <= "Z".charCodeAt(0));
			return !show;
                    }
		}
            }
	},
	{
            name: 'venue',
            label: 'Venue',
            options: {
		filter: true,
		sort: true,
		filterType: 'checkbox',
		filterOptions: { 
                    names: ['A-I', 'J-R', 'S-Z'],
                    logic(name, filters) {
			const show = 
                              (filters.indexOf('A-I') >= 0 && 
                               name.charCodeAt(0) >= "A".charCodeAt(0) && 
                               name.charCodeAt(0) <= "I".charCodeAt(0)) ||
                              (filters.indexOf('J-R') >= 0 && 
                               name.charCodeAt(0) >= "J".charCodeAt(0) && 
                               name.charCodeAt(0) <= "R".charCodeAt(0)) ||              
                              (filters.indexOf('S-Z') >= 0 && 
                               name.charCodeAt(0) >= "S".charCodeAt(0) && 
                               name.charCodeAt(0) <= "Z".charCodeAt(0));
			return !show;
                    }
		}, 
		customBodyRender: (value, tableMeta, updateValue) =>
		    eventCustomBodyRender(value, tableMeta, updateValue),
            }
	},
	{
            name: 'average_ticket_price',
            label: 'Average Ticket Price',
            options: {
		filter: true,
		sort: true,
		filterType: 'checkbox',
            }
	},
	{
            name: 'zipcode',
            label: 'Zipcode for Event',
            options: {
		filter: true,
		sort: true,
		filterType: 'checkbox',
            }
	},
    ];

    const options = {
	filter: true,
	filterType: 'multiselect',
	onRowClick: (rowData) => {
	    window.location.assign("/events/" + rowData[0]);
	},
    customSearch: (query, currentRow) => {
        let found = false;
        if (query.length < 2) {
            query = "";
        }
        setSearchText(query);
        currentRow.forEach((col) => {
          if (col != null && col.toString().toLowerCase().includes(query.toLowerCase())) {
            found = true;
          }
        });
        return found;
    },
    onSearchClose: () => {
        setSearchText("");
    },
	download: false,
	print: false,
	selectableRowsHideCheckboxes: true,
	selectableRowsHeader: false,
	viewColumns: false,
	onChangePage: handlePageChange,
	page: params.pg,
    };

  return (
    <div className="body">
        <div className="table-container">
            <div class="grid" style={{alignContent:'center'}}>
                <div class="row align-items-center">
                    <div class="col-xl-4">
                        <h2 className="page-title">Events</h2>
                        <h3 style={{textAlign:"center"}}>Check out all the places in Austin.</h3>
                        <h6 style={{textAlign:"center"}}>Filter, sort, or search to find an event.</h6>
			{/* <SearchBar
			  onChange={text => setTempSearch(text)}
			  onSearch={text => history.push('/events/search?q='+text)}
			  value={tempSearch}
			  placeholder='Search events'
			  /> */}
                    </div>
                </div>
            </div>
        </div>
        <div style={{display:'table', tableLayout:'fixed', width:'100%', height:'100%', cursor:'pointer'}}>
            { load ? (
                <>
                <MUIDataTable columns={columns} options={options} data={data}/>
                </>
            ) : (
                <h1 style={{ textAlign: "center", color: "red" }}> Loading </h1>
            )}
        </div>
    </div>
  )

}

export default Event; 
