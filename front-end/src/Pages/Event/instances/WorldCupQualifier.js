import React from 'react';
import { Card, CardActions, CardContent, CardMedia, Button, Typography, Grid } from '@material-ui/core';

function WorldCupQualifier() {
    return (
        <div>
            <h1>World Cup Qualifier</h1>
            <Grid
                container
                justify="center"
            >
                <Card className="card">
                    <CardContent>
                    <Typography>
                            Location: 30.3877, -97.7194
                        </Typography>
                        <Typography>
                            Address: 10414 McKalla Place
                        </Typography>
                        <Typography>
                            Venue: Q2 Stadium
                        </Typography>
                        <Typography>
                            Date and Time: 2021-10-06 7 p.m.
                        </Typography>
                        <Typography>
                            Performer: U.S. Men's National Soccer Team
                        </Typography>
                        <Typography>
                            Capacity: 1000
                        </Typography>
                        <Button color="primary" size="small" href="/restaurant/roppolos">
                            Roppolos Pizza
                        </Button>
                        <Typography>
                            Nearby Entertainment: 
                        </Typography>
                        <Button color="primary" size="small" href="/entertainment/zipline">
                            Ziplining
                        </Button>
                    </CardContent>
                </Card>
                <Card className="card">
                    <CardMedia
                        component="img"
                        height="200"
                        image={'https://pbs.twimg.com/media/Eor0SpLVEAEUAas.jpg'}
                        alt="Zipline"
                    />
                </Card>
                <Card className="card">
                    <CardMedia
                        component="iframe"
                        src= {"https://www.youtube.com/embed/DiLvJP1NPWQ"}
                        height="200"
                    />
                </Card>
            </Grid>
        </div>
    )  
}

export default WorldCupQualifier;