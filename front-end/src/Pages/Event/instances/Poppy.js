import React from 'react';
import { Card, CardActions, CardContent, CardMedia, CardHeader, Button, Typography, Grid } from '@material-ui/core';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import "./style.css";

function Poppy() {
    return (
        <div>
            <h1>Poppy</h1>
            <Grid
                container
                justify="center"
            >
                <Card className="card">
                    {/* <CardMedia>

                    </CardMedia> */}
                    <CardContent>
                        <Typography>
                            Location: 30.2405,-97.7283
                        </Typography>
                        <Typography>
                            Address: 2015 East Riverside Drive
                        </Typography>
                        <Typography>
                            Venue: Emo's Austin
                        </Typography>
                        <Typography>
                            Date and Time: 2021-10-06 8 p.m.
                        </Typography>
                        <Typography>
                            Performer: Poppy
                        </Typography>
                        <Typography>
                            Capacity: 2150
                        </Typography>
                        <Typography>
                            Nearby Restaurant: 
                        </Typography>
                        <Button color="primary" size="small" href="/restaurant/arturos">
                            Arturos
                        </Button>
                        <Typography>
                            Nearby Entertainment: 
                        </Typography>
                        <Button color="primary" size="small" href="/entertainment/union">
                            Union Underground
                        </Button>
                    </CardContent>
                </Card>
                <Card className="card">
                    <CardMedia
                        component="img"
                        height="200"
                        image={'https://www.nme.com/wp-content/uploads/2018/04/GettyImages-917912260_POPPY_1000.jpg'}
                        alt="Poppy"
                    />
                </Card>
                <Card className="card">
                    <CardMedia
                        component="iframe"
                        src={"https://www.youtube.com/embed/YTvIG6gHMh0"}
                        height="200"
                    />
                </Card>
            </Grid>
        </div>
    );
}

export default Poppy;