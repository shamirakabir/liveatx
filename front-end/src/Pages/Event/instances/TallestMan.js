import React from 'react';
import { Card, CardActions, CardContent, CardMedia, Button, Typography, Grid } from '@material-ui/core';

function TallestMan() {
    return (
        <div>
            <h1>The Tallest Man on Earth</h1>
            <Grid
                container
                justify="center"
            >
                <Card className="card">
                <CardContent>
                        <Typography>
                            Location: 30.2621, -97.7294
                        </Typography>
                        <Typography>
                            Address: 1308 East 4th Street
                        </Typography>
                        <Typography>
                            Venue: Scoot Inn
                        </Typography>
                        <Typography>
                            Date and Time: 2021-10-07 7 p.m.
                        </Typography>
                        <Typography>
                            Performer: The Tallest Man On Earth
                        </Typography>
                        <Typography>
                            Capacity: 1000
                        </Typography>
                        <Typography>
                            Nearby Restaurant: 
                        </Typography>
                        <Button color="primary" size="small" href="/restaurant/don">
                            Don Japanese
                        </Button>
                        <Typography>
                            Nearby Entertainment: 
                        </Typography>
                        <Button color="primary" size="small" href="/entertainment/cidercade">
                            Cidercade
                        </Button>
                    </CardContent>
                </Card>
                <Card className="card">
                    <CardMedia
                        component="img"
                        height="200"
                        image={'https://upload.wikimedia.org/wikipedia/commons/0/0d/Tallest_Man_On_Earth_Newport_2015.jpg'}
                        alt="Union"
                    />
                </Card>
                <Card className="card">
                    <CardMedia
                        component="iframe"
                        src= {"https://www.youtube.com/embed/nnxPKY7NSoM"}
                        height="200"
                    />
                </Card>
            </Grid>
        </div>
    )  
}

export default TallestMan;