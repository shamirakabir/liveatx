import React, { useEffect, useState }	from 'react';
import {
    Button,
    Card,
    CardContent,
    CardMedia,
    Grid,
    Typography,
}					from '@material-ui/core';
import { Spin }				from 'antd';

import MapComponent			from '../Components/MapComponent.jsx';
import Nearby from '../Components/Nearby.jsx';
import Placeholder			from '../Components/Placeholder.jsx';
import { getAPI }			from '../Library/Query.jsx';
import styles				from '../Styles/Instance.module.css';
import 'antd/dist/antd.css';

const api_url = process.env.REACT_APP_NOT_SECRECT_CODE;

const RestaurantInstance = () => {
    const [data, setData]				= useState(null);
    const [loading, setLoading]				= useState(true);
    const [error, setError]				= useState(null);
    const [pos, setPos]					= useState(null);

    useEffect(() => {
        const fetch = async () => {
            setLoading(true);
            const url = window.location.href;
            const id  = url.substring(url.lastIndexOf('/')+1);
            getAPI(`${api_url}/restaurants/id=${id}`)
                .then(response => {
                    setData(response.data);
                    setPos({
                        lat: parseFloat(response.data.latitude),
                        lng: parseFloat(response.data.longitude),
                    });
                })
                .catch(setError)
                .finally(() => setLoading(false));
        };
        fetch();
    }, []);    

    if (error) return <Placeholder.Error />;
    
    if (loading)
        return (
            <div
              className={styles.center}
              style={{height: '80vh'}}
            >
              <Spin size='large' />
            </div>
        );

    return (        
        <React.Fragment>
          <div className={styles.content}>
            <br></br>
            <br></br>
            <div className='row'>
              <div className='col-sm'>
                <Grid
                  container
                  spacing={24}
                  justify='center'
                  style={{minHeight: '100vh', maxWidth: '100%'}}
                >
                  <Grid item xs={4}>
                    <div className='col-sm'>
                      <Card
                        style={{
                            width: '631px',
                            top: '158px',
                            left: '34px',
                            height: '712px',
                            position: 'absolute',
                        }}
                      >
                        <CardContent>
                          <Typography><h3> Location</h3></Typography>
                          Address: {data.address}, {data.city} {data.state} {data.zipcode}
                          <br></br>
                          Latitude: {data.latitude}
                          <br></br>
                          Longitude: {data.longitude}
                          <br></br>
                          <br></br>
                          <br></br>
                          <div className={styles.map}>
                            <MapComponent
                              position={pos}
                              markers={[{...pos, noWindow: true}]}
                            />
                          </div>
                        </CardContent>
                      </Card>
                    </div>
                  </Grid>
                  <Grid item xs={4}>
                    <div className='col-sm'>
                      <Card
                        style={{
                            width: '746px',
                            top: '158px',
                            left: '683px',
                            height: '402px',
                            position: 'absolute',
                        }}
                      >
                        <CardContent>
                          <Typography><h3>About {data.name}</h3></Typography>
                          <CardMedia
                            component='img'
                            height='200'
                            width='200'
                            src={data.image_url}
                            alt='data.name'
                          />
                          <br></br>
                          <div
                            className='grid'
                            style={{alignContent: 'center'}}
                          >
                            <div className='row align-items-center'>
                              <div className='col-xl-6'>
                                Price Level: {data.price}
                                <br></br>
                                Phone: {data.phone.slice(0,2)}({data.phone.slice(2,5)}) {data.phone.slice(5,8)}-{data.phone.substring(8)}
                                <br></br>
                                Delivery: {data.delivery}
                                <br></br>
                                <Button href={data.yelp_url}>
                                  Yelp Page
                                </Button>
                                <br></br>
                                <br></br>
                                <br></br>
                                <br></br>
                              </div>
                            </div>
                          </div>
                        </CardContent>                        
                      </Card>
                    </div>
                  </Grid>
                  <Grid item xs={4}>
                    <div className='col-sm'>
                      <Card
                        style={{
                            width: '359px',
                            top: '578px',
                            left: '683px',
                            height: '292px',
                            position: 'absolute',
                        }}
                      >
                        <CardContent>
                          <Typography><h3> Entertainment Nearby </h3></Typography>
                          <div style={{listStyle: 'none'}}>
                            <Nearby.Entertainment distance={data.distance} />
                          </div>
                          <br></br>
                          <br></br>
                          <br></br>                          
                        </CardContent>
                      </Card>
                    </div>
                  </Grid>
                  <Grid item xs={4}>
                    <div className='col-sm'>
                      <Card
                        style={{
                            width: '359px',
                            top: '578px',
                            left: '1061px',
                            height: '292px',
                            position: 'absolute',
                        }}
                      >
                        <CardContent>
                          <Typography><h3> Events Nearby </h3></Typography>
                          <div style={{listStyle: 'none'}}>
                            <Nearby.Events zipcode={data.zipcode} />
                          </div>
                          <br></br>
                          <br></br>
                          <br></br>                          
                        </CardContent>
                      </Card>
                    </div>
                  </Grid>
                </Grid>
              </div>
            </div>
          </div>
        </React.Fragment>        
    );    
};

export default RestaurantInstance;
