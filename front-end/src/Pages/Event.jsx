
/* external */
import React, { useEffect, useState }	from 'react';
import {
    Link,
    useHistory,
}					from 'react-router-dom';
import {
    useQueryParams,
    ArrayParam,
    NumberParam,
    StringParam,
    withDefault,
}					from 'use-query-params';
import { stringify }			from 'query-string';
import {
    Button,
    Card,
    Col,
    Drawer,
    Input,
    Pagination,
    Row,
    Select,
    Slider,
    Space,
    Spin,
    Switch,
    Table,
    Typography,
}					from 'antd';

/* components */
import Highlight			from '../Components/Highlight.jsx';
import MapComponent			from '../Components/MapComponent.jsx';
import Placeholder			from '../Components/Placeholder.jsx';
import Rating				from '../Components/Rating.jsx';
import EventCard			from '../Components/EventCard.jsx';
import { eventTableColumns }		from '../Components/Columns.jsx';

import { query }			from '../Library/Query.jsx';
import styles				from '../Styles/Search.module.css';
import 'antd/dist/antd.css';

const { Meta } = Card;
const { Title } = Typography;

const Event = () => {
    const history			= useHistory();
    const [loading, setLoading]		= useState(true);
    const [data, setData]		= useState([]);
    const [search, setSearch]		= useState(undefined);
    const [isQuery, setIsQuery]		= useState(false);
    const [error, setError]		= useState(null);
    const [displayType, setDisplayType] = useState('grid');
    const [showFilter, setShowFilter]	= useState(false);
    const [letters, setLetters]		= useState([]);
    const [maxAverage, setMaxAverage]	= useState(0);
    const [maxHighest, setMaxHighest]	= useState(0);
    const [maxLowest, setMaxLowest]	= useState(0);
    const [maxMedian, setMaxMedian]	= useState(0);
    
    const [params, setParams] = useQueryParams({
	q: withDefault(StringParam, ''),
	page: withDefault(NumberParam, 1),
	itemsPerPage: withDefault(NumberParam, 10),
	sort: withDefault(StringParam, ''),
        name: withDefault(StringParam, ''),
        average_ticket_price: withDefault(NumberParam, 999999),
        highest_ticket_price: withDefault(NumberParam, 999999),
        lowest_ticket_price: withDefault(NumberParam, 999999),
        median_ticket_price: withDefault(NumberParam, 999999),
    });
    
    const [sortDescending, setSortDescending]	= useState(params.sort && params.sort.length > 0 && params.sort[0] === '-');
    const [filterName, setFilterName]		= useState(params.name);
    const [filterAverage, setFilterAverage]	= useState(params.average_ticket_price);
    const [filterHighest, setFilterHighest]	= useState(params.highest_ticket_price);
    const [filterLowest, setFilterLowest]	= useState(params.lowest_ticket_price);
    const [filterMedian, setFilterMedian]	= useState(params.median_ticket_price);
    
    const handleSearchChange = obj => setSearch(obj.target.value);
    const handleSearch = text => {
	setParams(params => ({...params, q: text}));
	history.push(`/event?${stringify({...params, q: text})}`);
	setIsQuery(true);
    };

    const handlePageChange = num => {
	setParams(params => ({...params, page: num}));
	history.push(`/event?${stringify({...params, page: num})}`);
    };

    const handlePagination = (page, pageSize) => {
	setParams(params => ({...params, page: page, itemsPerPage: pageSize}));
	history.push(`/event?${stringify({...params, page: page, itemsPerPage: pageSize})}`);
    };

    const handleSort = (value, option) => {        
	setParams(params => ({...params, q: sortDescending ? '-' + value : value}));
	history.push(`/event?${stringify({...params, sort: sortDescending ? '-' + value : value})}`);
    };

    const handleSortDirection = () => {
        const value = params.sort.substring(sortDescending ? 1 : 0); 
        setSortDescending(!sortDescending);
        setParams(params => ({...params, q: sortDescending ? value : '-' + value}));
	history.push(`/event?${stringify({...params, sort: sortDescending ? value : '-' + value})}`);
    };

    const handleFilterName = (value, options) => {
        const values = options.map(x => x.value);        
        setFilterName(`^[${values.join('|')}]`);
        if (values.length === 0)
            setFilterName('');
    };
        
    const handleApplyFilter = () => {
        var ratings = [];
        const newParams = params => ({
            ...params,
            name: filterName,
            average_ticket_price: filterAverage,
            highest_ticket_price: filterHighest,
            lowest_ticket_price: filterLowest,
            median_ticket_price: filterMedian,
        });
        setParams(newParams);
        history.push(`/event?${stringify(newParams(params))}`);
    };

    useEffect(() => {
	setLoading(true);
	setIsQuery(params.q ? true : false);
	const fetch = async () => {
	    query('events', params)
                .then(response => setData(response.data))
                .then(() => query('events'))
                .then(response => {
		    const data = response.data.events;
                    setLetters([...new Set(data.map(x => x.name[0]))].sort());
                    setMaxAverage(Math.max(0, ...data.map(x => x.average_ticket_price)));
                    setMaxHighest(Math.max(0, ...data.map(x => x.highest_ticket_price)));
                    setMaxLowest(Math.max(0, ...data.map(x => x.lowest_ticket_price)));
                    setMaxMedian(Math.max(0, ...data.map(x => x.median_ticket_price)));
		})
	        .catch(error => {
		    console.error(error);
		    setError(error);
	        })
                .finally(() => setLoading(false));
	};
	fetch();
    }, [params, history]);

    const Hi = (props) => (
	<Highlight
	  words={[params.q]}
	  text={props.text} />
    );

    const DisplayGrid = () => {
	if (data.events.length === 0)
	    return <Typography>No results found.</Typography>;
	return (
	    <div className={styles.grid}>
	      {
		  data.events.map(result => (
		      <EventCard
			query={params.q}
			{...result}
		      />
		  ))
	      }
	    </div>
	);
    };

    const DisplayTable = () => {
	if (data.events.length === 0)
	    return <Typography>No results found.</Typography>;
	return (
	    <Space className={styles.table} direction='vertical'>
	      <Table
		dataSource={data.events}
		columns={eventTableColumns(params.q)}
		pagination={false}
	      />
	    </Space>
	);
    };

    if (error)
	return <Placeholder.Error err={error}/>;

    return (
	<React.Fragment>
	  <section className={styles.content}>
	    <div className={styles.contentHeader}>
	      <Typography.Title level={1}>
		{ isQuery ? (
		    'Results for "' + params.q + '"'
		) : (
		    'Events'
		) }
	      </Typography.Title>
	      <div className='searchBar'>
		<Input.Search
		  size='large'
		  placeholder='Search events'
		  enterButton='Search'
		  onChange={handleSearchChange}
		  onSearch={handleSearch}
		  value={search}
		  loading={loading}
		/>
	      </div>
	    </div>
	    {
		loading ? (
		    <div className={styles.center}>
		      <Spin size='large' />
		    </div>
		) : (
		    <React.Fragment>
		      <div className={styles.results}>
			<Drawer
			  title='Filters'
			  placement='bottom'
			  onClose={() => setShowFilter(false)}
			  visible={showFilter}
			  extra={
			      <Space>
				<Button type='primary' onClick={handleApplyFilter}>Apply</Button>
				<Button onClick={() => setShowFilter(false)}>Cancel</Button>
			      </Space>
			  }
			>
			  <Row style={{width: '100%'}}>
			    <Col span={12}>
                              <Space
                                direction='vertical'
                                style={{width: '90%'}}
                                size='large'
                              >
			        <Select
				  showSearch
				  allowClear
				  style={{width: '100%'}}
				  mode='multiple'
				  placeholder='Name'
				  defaultValue={params.name.split('').filter(c => c >= 'A' && c <= 'Z')}
				  onChange={handleFilterName}
				  optionFilterProp='label'
				  options={letters.map(c => ({label: c, value: c}))}
			        />
                                </Space>
                              </Col>
                            <Col span={12}>
                              <Space
                                direction='vertical'
                                style={{width: '90%'}}
                                size='large'
                              >
                                <Row style={{width: '100%'}}>
                                  <Col flex='none'>
                                    Average Ticket Price:
                                  </Col>
                                  <Col span={1} />
                                  <Col flex={1}>
                                    <Slider
                                      min={0}
                                      max={maxAverage}
                                      value={filterAverage}
                                      step={1}
                                      onChange={setFilterAverage}
                                    />
                                  </Col>                    
                                </Row>
                                <Row style={{width: '100%'}}>
                                  <Col flex='none'>
                                    Highest Ticket Price:
                                  </Col>
                                  <Col span={1} />
                                  <Col flex={1}>
                                    <Slider
                                      min={0}
                                      max={maxHighest}
                                      value={filterHighest}
                                      step={1}
                                      onChange={setFilterHighest}
                                    />
                                  </Col>                    
                                </Row>
                                <Row style={{width: '100%'}}>
                                  <Col flex='none'>
                                    Lowest Ticket Price:
                                  </Col>
                                  <Col span={1} />
                                  <Col flex={1}>
                                    <Slider
                                      min={0}
                                      max={maxLowest}
                                      value={filterLowest}
                                      step={1}
                                      onChange={setFilterLowest}
                                    />
                                  </Col>                    
                                </Row>
                                <Row style={{width: '100%'}}>
                                  <Col flex='none'>
                                    Median Ticket Price:
                                  </Col>
                                  <Col span={1} />
                                  <Col flex={1}>
                                    <Slider
                                      min={0}
                                      max={maxMedian}
                                      value={filterMedian}
                                      step={1}
                                      onChange={setFilterMedian}
                                    />
                                  </Col>                    
                                </Row>

                              </Space>
			    </Col>
			    <Col span={12}>
			    </Col>
			  </Row>
			</Drawer>
			<Row justify='end'>
			  <Col flex={1}>
			    <Select
			      showSearch
			      style={{width: '100%'}}
			      placeholder='Sort by'
			      onChange={handleSort}
                              defaultValue={params.sort.substring(sortDescending ? 1 : 0) === '' ?
                                            null : params.sort.substring(sortDescending ? 1 : 0)}
			      optionFilterProp='label'
			      options={[
				  {label: 'None', value: null},
				  {label: 'Name', value: 'name'},
                                  {label: 'Average Ticket Price', value: 'average_ticket_price'},
                                  {label: 'Highest Ticket Price', value: 'highest_ticket_price'},
                                  {label: 'Lowest Ticket Price', value: 'lowest_ticket_price'},
                                  {label: 'Median Ticket Price', value: 'median_ticket_price'},
                                  {label: 'Time', value: 'datetime_utc'},
                                  {label: 'Capacity', value: 'capacity'}
			      ]}
			    />
			  </Col>
                          <Col flex='none'>
                            <Button onClick={handleSortDirection}>
                              {sortDescending ? 'Descending' : 'Ascending'}
                            </Button>
                          </Col>
			  <Col flex='none'>
			    <Button onClick={() => setShowFilter(true)}>
			      Filters
			    </Button>
			  </Col>
			  <Col span={1} />
			  <Col flex='none'>
			    <Pagination
			      size='small'
			      current={params.page}
			      pageSize={params.itemsPerPage}
			      total={data.numberOfPages * params.itemsPerPage}
			      onChange={handlePagination}
			      showSizeChanger
			    />
			  </Col>
			  <Col span={1} />
			  <Col flex='none'>
			    <Space>
			      <div>Display as </div>
			      <Button
				onClick={event => setDisplayType('grid')}
				disabled={displayType === 'grid'}
			      >
				Grid
			      </Button>
			      <Button
				onClick={event => setDisplayType('table')}
				disabled={displayType === 'table'}
			      >
				Table
			      </Button>
			    </Space>
			  </Col>
			</Row>
			<br></br>
			{ (displayType === 'grid')  ? <DisplayGrid />  : null }
			{ (displayType === 'table') ? <DisplayTable /> : null }
		      </div>
		    </React.Fragment>
		)
	    }
	  </section>
	</React.Fragment>
    );
};

export default Event;
