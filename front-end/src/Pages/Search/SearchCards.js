import React, { Fragment }	from 'react';
import { Card }			from 'antd';
import { Link }			from 'react-router-dom';
import Highlighter		from 'react-highlight-words';
import styles			from './Search.module.css';
import Highlight from '../../Components/Highlight.jsx';
import Rating from '../../Components/Rating.jsx';
import 'antd/dist/antd.css';

// Credit to Jefferson Ye's Texas Votes group

const { Meta } = Card;
require('datejs');

export function RestaurantsCard(props) {
    const {
        address,
        category,
        city,
        delivery,
        distance,
        fulfillment,
        image_url,
        is_open,
        latitude,
        longitude,
        name,
        phone,
        price,
        rating,
        restaurant_id,
        review_count,
        state,
        yelp_url,
        zipcode,
        searchQuery
    } = props;

    const Hi = (props) => (
        <Highlight
          words={[searchQuery]}
          text={props.text} />
    );
    
    return (
        <Link to={`/restaurants/${restaurant_id}`} target='_blank'>
          <Card
	    className={styles.generalCard}
	    hoverable
	    cover={
	        <img src={image_url}
                     alt={name}
                     className={styles.cardCover} />
	    }
          >
            <Meta
              title={
	          <Hi text={name} /> }
              description={
	          <React.Fragment>
		    <Hi text={category} />
	            <br></br>
		    <Hi text={address} />
		    <br></br>
		    <Rating rating={parseFloat(rating)} />
	            <br></br>
                    from {review_count} reviews
		    <br></br>
		    { is_open ? (
                        <Hi text='Open Now' />
                    ) : (
                        <Hi text='Closed'/>
                    ) }
	          </React.Fragment>
              }
            />
          </Card> </Link>
    );
}

export function EventsCard(props) {
    const {
        name,
        type,
        event_id,
        datetime_utc,
        url,
        images,
        capacity,
        performers,
        venue,
        latitude,
        longitude,
        address,
        city,
        zip_code,
        state,
        searchQuery
    } = props;

    const date = new Date(datetime_utc);    
		
    return (
      <Link to={`/events/${event_id}`} target='_blank'>
      <Card
	className={styles.generalCard}
	hoverable
	cover={<img src={images[0]} alt={name} className={styles.cardCover} />}>
        <Meta
          title={
          <Highlighter
            highlightClassName={styles.highlight}
            searchWords={[searchQuery]}
            textToHighlight={`${name}`} />
          }
          description={
	  <>
            <Highlighter
              highlightClassName={styles.highlight}
              searchWords={[searchQuery]}
              textToHighlight={venue}/>
	    <br></br>
	    <Highlighter
	      highlightClassName={styles.highlight}
              searchWords={[searchQuery]}
              textToHighlight={address} />
	    <br></br>
	    <Highlighter
	      highlightClassName={styles.highlight}
              searchWords={[searchQuery]}
              textToHighlight={date.toString('MMMM d yyyy hh:mm tt')} />
	  </>
          }
          />
      </Card> </Link>
    );
}

export function EntertainmentCard(props) {
    const {
        address,
        category,
        city,
        delivery,
        distance,
        entertainment_id,
        fulfillment,
        image_url,
        is_open,
        latitude,
        longitude,
        name,
        phone,
        price,
        rating,
        review_count,
        state,
        yelp_url,
        zipcode,
	searchQuery
    } = props;
    
    return (
	<Link to={`/entertainment/${entertainment_id}`} target='_blank'>
	<Card
	  className={styles.generalCard}
	  hoverable
	  cover={
	  <img src={image_url} alt={name} className={styles.cardCover} />
	  }
	  >
          <Meta
            title={
            <Highlighter
              highlightClassName={styles.highlight}
              searchWords={[searchQuery]}
              textToHighlight={`${name}`}
              />
            }
            description={
	    <>
            <Highlighter
              highlightClassName={styles.highlight}
              searchWords={[searchQuery]}
              textToHighlight={category} />
	    <br></br>
	    <Highlighter
	      highlightClassName={styles.highlight}
              searchWords={[searchQuery]}
              textToHighlight={address} />
	  </>
            }
            />
	</Card> </Link>
    );
}

// Card that links the rest of the results on the general search page.
export function MoreResultsCard({ amount, model, searchQuery }) {
    return (
	<Link to={`/${model}/search?q=${searchQuery}`} target='_blank'>
	<Card className={styles.generalCard}>
	  <Meta
	    title={`View more ${model} results here.`}
	    description={`There are ${amount} results remaining.`}
	    />
	</Card> </Link>
    )
}
