import React, { Fragment, useState, useEffect }		from "react"
import { Typography, Tabs }				from "antd"
import { useHistory, useLocation }			from "react-router-dom"
import { getAPI }					from "../library/Data"
import styles						from "./Search.module.css"
import { useQueryParams, StringParam, withDefault, }	from "use-query-params"
import {
    RestaurantsCard,
    EventsCard,
    EntertainmentCard,
    MoreResultsCard,    
} from "./SearchCards"
import GeneralSearchBar from "./GeneralSearchBar"
import "antd/dist/antd.css";
import RestaurantCard from '../../Components/RestaurantCard.jsx'
import EventCard from '../../Components/EventCard.jsx'

// Credit to Jefferson Ye's Texas Votes group

const { Title, Paragraph } = Typography

const SEARCH_PATHS = ["restaurants", "events", "entertainment"]
const SEARCH_LIMIT = 10

// General search page
export default function Search() {
    const history = useHistory();
    const [loaded, setLoaded] = useState(false);
    const [tempSearch, setTempSearch] = useState("");
    const [params, setParams] = useState({q: new URLSearchParams(useLocation().search).get("q")});
    // TODO: Not critical, but below code is ideal way to get url query param
    /*const [params, setParams] = useQueryParams({
        q: withDefault(StringParam, "")
	})*/
    const [results, setResults] = useState({
	restaurants: [],
	events: [],
	entertainment: [],
    });
    
    // Handler for textChange on GeneralSearchBar
    const handleSearchChange = (text) => {
	setTempSearch(text);
    };

    const handleSearch = (text) => {
	setParams(params => ({...params, q: text}));
	history.push('/search?q='+text);
    };

    // Called on componentMount
    useEffect(() => {
	
	// Asynchronous function that searches all three models
	const fetchData = async () => {
	    try {
                setLoaded(false);
                
		// creates an array of promises that will return the results for each model search
		const promises = SEARCH_PATHS.map((model) => {
		    // return getAPI("http://localhost/api/" + model + "?q=" + params.q)
			console.log(process.env.REACT_APP_NOT_SECRECT_CODE + "/" + model + "?q=" + params.q)
		    return getAPI(process.env.REACT_APP_NOT_SECRECT_CODE + "/" + model + "?q=" + params.q)
		})
		// resolves the promises
		const resolved = await Promise.all(promises)
		const all = {}
		// add the results to our state
		resolved.forEach((r, i) => {
                    let page = []
                    const model = SEARCH_PATHS[i]
                    if (model === "restaurants") {  
                        page = page.concat(r.data.restaurants)
                    } else if (model === "events") {
                        page = page.concat(r.data.events)
                    } else if (model === "entertainment") {
                        page = page.concat(r.data.entertainment)
                    }
                    const count = page.length
		    // limit to SEARCH_LIMIT
		    all[model] = page.slice(0, SEARCH_LIMIT)
		    if (count > SEARCH_LIMIT) {
			// add an eor object that will map to a link for more results
			all[model].push({
			    eor: true,
			    model: SEARCH_PATHS[i],
			    amount: count - SEARCH_LIMIT,
			})
		    }
		})
		setResults(all)
		setLoaded(true)
	    } catch (err) {
		console.error(err)
	    }
	}

	fetchData()
    }, [params, history])

    return (
      <section className={styles.content}>
	<div className={styles.contentHeader}>
          <Title level={1}>
            {params.q ? `Results for "${params.q}"` : "Search"}
          </Title>
	  <Paragraph>
            Search for a restaurant, an event, or entertainment here.
	  </Paragraph>
          <GeneralSearchBar
            onChange={handleSearchChange}
            onSearch={handleSearch}
            value={tempSearch}
            />
	</div>
	{loaded ? (
	<Fragment>
	  <div className={styles.searchResults}>
	    <Title level={3}>Restaurants</Title>
	    <div className={styles.searchItemsRestaurants}>
	      {results.restaurants.map((r) => {
	      if (r.eor === true)
	      return (
	      <RestaurantCard.MoreResults
		{...r}
		query={params.q}
		/>
	      )
	      return (
	      <RestaurantCard
		key={r.restaurant_id}
		{...r}
		searchQuery={params.q}
		/>
	      )
	      })}
	    </div>
	    {results.restaurants.length === 0 ? (
	    <Paragraph>No results found.</Paragraph>
	    ) : null}
	  </div>
	  <div className={styles.searchResults}>
	    <Title level={3}>Events</Title>
	    <div className={styles.searchItemsEvents}>
	      {results.events.map((e) => {
	      if (e.eor === true)
	      return (
		      <EventCard.MoreResults {...e}
		  query={params.q}
		/>
	      )
	      return (
	      <EventCard
                key={e.event_id}
                {...e}
                searchQuery={params.q}
		/>
	      )
	      })}
	    </div>
	    {results.events.length === 0 ? (
	    <Paragraph>No results found.</Paragraph>
	    ) : null}
	  </div>
	  <div className={styles.searchResults}>
	    <Title level={3}>Entertainment</Title>
	    <div className={styles.searchItemsEntertainment}>
	      {results.entertainment.map((e) => {
	      if (e.eor === true)
	      return (
	      <MoreResultsCard
                key={e.entertainment_id}
		{...e}
		searchQuery={params.q}
		/>
	      )
	      return (
	      <EntertainmentCard
                key={e.entertainment_id}
                {...e}
                searchQuery={params.q}
		/>
	      )
	      })}
	    </div>
	    {results.entertainment.length === 0 ? (
	    <Paragraph>No results found.</Paragraph>
	    ) : null}
	  </div>
	</Fragment>
	) : (
        <div>Loading results...</div>
	)}
      </section>
    )
}

export default Search;
