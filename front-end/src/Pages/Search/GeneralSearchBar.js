import React from "react"
import { Input } from "antd"
import styles from "./Search.module.css"
import "antd/dist/antd.css";

// Credit to Jefferson Ye's Texas Votes group

const { Search } = Input

// This component is used on both the home and search pages.
export default function GeneralSearchBar({
    value,
    onSearch,
    onChange,
}) {
    const onSearchChange = (e) => {
	onChange(e.target.value)
	}

	let className = styles.searchContainer
	let props = {
		size: "large",
		enterButton: "Search",
		placeholder: "Enter sitewide search here.",
        onChange: onSearchChange,
		onSearch: onSearch,
		value: value,
	}

    return (
        <div className={className}>
            <Search {...props} />
        </div>
    )

}

export function SearchBar({
    value,
    onSearch,
    onChange,
    placeholder,
}) {
    const onSearchChange = (e) => {
	onChange(e.target.value)
    };
    let className = styles.searchContainer;
    let props = {
	size: "large",
	enterButton: "Search",
	placeholder: placeholder,
        onChange: onSearchChange,
	onSearch: onSearch,
	value: value,
    };   
    return (
        <div className={className}>
          <Search {...props} />
        </div>
    );
}
