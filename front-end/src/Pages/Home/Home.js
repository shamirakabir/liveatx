import React, { useState } from 'react';
import { Card, CardActions, CardContent, CardMedia, Button, Typography, Grid, Box, CardActionArea } from '@material-ui/core';
import { Autocomplete, Stack, TextField } from '@mui/material'
import axios from 'axios';
import Hanks from "./Hanks.jpg";
import Kayak from "./kayaking.jpg";
import ACL from "./acl.jpg";
import GeneralSearchBar from "../Search/GeneralSearchBar";
import { useHistory } from 'react-router-dom';
import "./home.css"


function Home() {
    const history = useHistory()
    const [searchVal, setSearchVal] = useState("")

    return (
        <div>
            <div className="homeImageStyle">
                <h2 className="titleText">
                    Find your way around Austin!
                </h2>
                <div className="searchBarContainer">
                    <GeneralSearchBar
                        onSearch={(value) =>
                            history.push(`/search?q=${value}`)
                        }
                        onChange={(text) => setSearchVal(text)}
                        value={searchVal}
                    />
                </div>
            </div>
            <div className="container">
                <div className="row">
                <Grid
                    container
                    spacing={2}
                    justifyContent="center"
                    alignItems="center"
                    direction="column"
                    rowSpacing={5}
                    p={10}
                    style={{ minHeight: '100%', maxWidth: '100%'}}
                >
                    <Grid item xs={12}>
                        <Box sx={{flexGrow: 1}}>
                                <Grid container rowSpacing={10} alignItems="center" justifyContent="center">
                                    <Grid item xs={12}>
                                        <Grid container rowSpacing={10} columnSpacing={10} alignItems="center" justifyContent="center">
                                            <Box sx={{my: 1}}>
                                                <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                                    <CardActionArea href='/restaurant'>
                                                        <CardMedia
                                                        component="img"
                                                        height="200"
                                                        image={Hanks}
                                                        alt="Hank's"
                                                        />
                                                        <CardContent>
                                                            <Typography variant="h5" component="div">
                                                                Restaurants
                                                            </Typography>
                                                            <Typography color="text.secondary">
                                                                Check Out Austin's Restaurants!
                                                            </Typography>
                                                        </CardContent>
                                                    </CardActionArea>
                                                </Card>
                                            </Box>
                                            <Box sx={{my: 1}}>
                                                <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                                    <CardActionArea href='/event'>
                                                        <CardMedia
                                                            component="img"
                                                            height="200"
                                                            image={ACL}
                                                            alt="ACL"
                                                        />
                                                        <CardContent>
                                                            <Typography variant="h5" component="div">
                                                                Events
                                                            </Typography>
                                                            <Typography color="text.secondary">
                                                                Check Out Austin's Current Events!
                                                            </Typography>
                                                        </CardContent>
                                                    </CardActionArea>
                                                </Card>
                                            </Box>
                                            <Box sx={{my: 1}}>
                                                <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                                    <CardActionArea href='/entertainment'>
                                                        <CardMedia
                                                            component="img"
                                                            height="200"
                                                            image={Kayak}
                                                            alt="Kayak"
                                                        />
                                                        <CardContent>
                                                            <Typography variant="h5" component="div">
                                                                Entertainment
                                                            </Typography>
                                                            <Typography color="text.secondary">
                                                                Check Out Austin's Entertainment!
                                                            </Typography>
                                                        </CardContent>
                                                    </CardActionArea>
                                                </Card>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Box>
                        </Grid>
                    </Grid> 
                </div>
            </div>
        </div>
    );
}

export default Home 