import { getAPI } from '../../Library/Query.jsx';
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Pie, 
    PieChart,
    LineChart, 
    Line
} from "recharts";
// import BubbleChart from "@weknow/react-bubble-chart-d3"
import React, { useEffect, useState } from 'react';
// import { Typography } from '@material-ui/core';
// import { Typography } from "antd"
import { Spin } from 'antd';

const api_url = process.env.REACT_APP_NOT_SECRECT_CODE;

const CuisineChart = () => {
    const [data, setData] = useState([]);
    const [load, setLoad] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
	    setLoad(true);
            await getAPI(api_url+'/restaurants').then(response => {
                setData(response.data.restaurants)
                setLoad(false)
            }).catch((error) => {
                setData([])
                setLoad(false)
            });
        }
        fetchData();
    }, []);
    
    function getValues() {
        var values = [];
        var dict = {};

        for (var i = 0; i < data.length; i++) {
            var cuisine = data[i]["category"];
            if (cuisine in dict) {
                dict[cuisine]++;
            }
            else {
                dict[cuisine] = 1;
            }
        }

        for (var method in dict) {
            var randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
            values.push({"name": method, "value": dict[method], "fill": randomColor})
        }

        return values;
    }


    if (load) {
        return <Spin />
    }
    
    return (
        <center>
        <div style={{ margin: 'auto', marginTop: '5vh'}} >
            
            <h3 className="text-center"> Number of Restaurants for Each Cuisine </h3>
            <p className="text-center"> Hover to view values </p>
            <PieChart 
                width={1000} 
                height={800} 
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <Pie style={{paddingLeft:'50px'}}
                    dataKey="value"
                    isAnimationActive={true}
                    data={getValues()}
                    cx={485}
                    cy={350}
                    outerRadius={300}
                    fill="#fff"
                    label
                />
                <Tooltip />
            </PieChart>
        </div>
        </center>

    );  

}

const EntertainmentChart = () => {
    const [data, setData] = useState([]);
    const [load, setLoad] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            await getAPI(api_url+'/entertainment').then(response => {
                setData(response.data.entertainment)
                setLoad(false)
            }).catch((error) => {
                setData([])
                setLoad(false)
            });
        }
        fetchData();
    }, []);

    function getValues() {
        var dict = {
            "$" : 0,
            "$$" : 0,
            "$$$" : 0,
            "$$$$": 0
        };
        
        for (var i = 0; i < data.length; i++) {
            var val = data[i]["price"];
            dict[val]++;
        }

        var values = [
            {
                name: "$",
                value: dict["$"],
            },
            {
                name: "$$",
                value: dict["$$"],
            },
            {
                name: "$$$",
                value: dict["$$$"],
            },
            {
                name: "$$$$",
                value: dict["$$$$"],
            }
        ];

        return values;
    }

    if (load) {
        return <Spin />
    }

    return (
        <div style={{margin: 'auto', marginTop: '5vh'}} >
            <h3 className="text-center"> Number of Entertainment by Rating </h3>
            <p className="text-center"> Hover to view exact values</p>
            <p className="text-center"> (x - Rating, y - Count)</p>
            <BarChart
                width={1000}
                height={500}
                data={getValues()}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}
                barSize={20}>
                <XAxis dataKey="name" padding={{left: 100, right: 10 }}/>
                <YAxis type="number" dataKey="value"/>
                <Tooltip/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Bar dataKey="value" fill="#bf5700" background={{ fill: "#eee" }}/>
            </BarChart>
        </div>
    );

}

const EventChart = () => {
    const [data, setData] = useState([]);
    const [load, setLoad] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
	    setLoad(true);
            await getAPI(api_url+'/events').then(response => {
                setData(response.data.events)
                setLoad(false)
            }).catch((error) => {
                setData([])
                setLoad(false)
            });
        }
        fetchData();
    }, []);
    
    function getValues() {
        var values = [];
        var dict_highest = {}
        var dict_average = {};

        for (var i = 0; i < data.length; i++) {
            var venue = data[i]["venue"];
            var hi_price = data[i]["highest_ticket_price"];
            var avg_price = data[i]["average_ticket_price"];

            if (venue in dict_highest) {
                var curr = dict_highest[venue];
                dict_highest[venue] = Math.max(curr, hi_price);
                curr = dict_average[venue]
                dict_average[venue] = Math.max(curr, avg_price);

            }
            else {
                dict_highest[venue] = hi_price;
                dict_average[venue] = avg_price; 

            }
        }

        for(var venue in dict_highest){
            var temp = {"high": dict_highest[venue], "avg": dict_average[venue], "venue": venue};
            values.push(temp);
        }

        return values;
    }

    if (load) {
        return <Spin />
    }

    return (
        <div style={{margin: 'auto', marginTop: '5vh'}} >
            <h3 className="text-center"> Highest Price Ticket vs Average Price Ticket for each Venue </h3>
            <p className="text-center"> Hover to view exact values</p>

            <LineChart
            width={1300}
            height={600}
            data={getValues()}
            margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
            }}
            >
				<CartesianGrid />
				<XAxis dataKey="venue" name="Venue Name" tick={false} label={{value: 'Venues'}} />
				<YAxis />
				<Tooltip cursor={{ strokeDasharray: '3 3' }}  />
				<Line type="monotone" dataKey="high" stroke="#8884d8" activeDot={{ r: 8 }} />
                <Line type="monotone" dataKey="avg" stroke="#82ca9d" />
			</LineChart>
        </div>
    );


}

export { CuisineChart, EntertainmentChart, EventChart }
