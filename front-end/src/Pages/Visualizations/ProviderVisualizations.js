import React from "react";
import { Typography } from "antd";
import { StateChart, IndustryChart, CitiesChart } from "./CollegiateConnectionVisualizations.js"

const { Title } = Typography

const Visualizations = () => {
	return (
		<center>
			<main>
				<Title level={1}>
					Provider's Visualizations
				</Title>
				<StateChart />
				<IndustryChart />
				<CitiesChart />
			</main>
		</center>
	)
}

export default Visualizations
