import React from "react";
import { Typography } from "antd";
import { CuisineChart, EntertainmentChart, EventChart } from "./LiveATXVisualizations.js"

const { Title } = Typography

const Visualizations = () => {
	return (
		<center>
			<main>
				<Title level={1}>
					Our Visualizations
				</Title>
				<CuisineChart />
				<EntertainmentChart />
				<EventChart />
			</main>
		</center>
	)
}

export default Visualizations

