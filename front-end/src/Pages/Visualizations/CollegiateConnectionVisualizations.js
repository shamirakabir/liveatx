import { getAPI } from '../../Library/Query.jsx';
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Pie, 
    PieChart,
    ScatterChart,
    Scatter,
    ZAxis
} from "recharts";
// import BubbleChart from "@weknow/react-bubble-chart-d3"
import React, { useEffect, useState } from 'react';
// import { Typography } from '@material-ui/core';
// import { Typography } from "antd"
import { Spin } from 'antd';

const api_url = 'https://api.collegiateconnection.me'

const StateChart = () => {
    const [data, setData] = useState([]);
    const [load, setLoad] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
	    setLoad(true);
            await getAPI(api_url+'/colleges?per_page=5000').then(response => {
                setData(response.data.colleges)
                setLoad(false)
            }).catch((error) => {
                setData([])
                setLoad(false)
            });
        }
        fetchData();
    }, []);
    
    function getValues() {
        var values = [];
        var dict = {};

        for (var i = 0; i < data.length; i++) {
            var state = data[i]["state_name"];
            if (state in dict) {
                dict[state]++;
            }
            else {
                dict[state] = 1;
            }
        }

        for (var method in dict) {
            var randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
            values.push({"name": method, "value": dict[method], "fill": randomColor})
        }
        
        return values;
    }


    if (load) {
        return <Spin />
    }
    
    return (
        <center>
            <div style={{margin: 'auto', marginTop: '5vh'}} >
                <h3 className="text-center"> Number of Colleges per State </h3>
                <p className="text-center"> Hover to view exact values</p>
                <BarChart
                    width={800}
                    height={600}
                    data={getValues()}
                    layout="horizontal"
                    margin={{ bottom: 5 }}
                    >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis
                    type="category"
                    dataKey="name"
                    tick={false}
                    label={{
                        value: 'States'
                    }}
                    />
                    <YAxis
                    type="number"
                    label={{
                        value: "Number of Colleges",
                        angle: -90,
                        position: "insideLeft"
                    }}
                    />
                    <Tooltip />
                    <Bar dataKey="value" fill="#1E88E5" />
                </BarChart>

            </div>
        </center>
    );

}

const IndustryChart = () => {
    const [data, setData] = useState([]);
    const [load, setLoad] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
	    setLoad(true);
            await getAPI(api_url+'/companies?per_page=5000').then(response => {
                setData(response.data.companies)
                setLoad(false)
            }).catch((error) => {
                setData([])
                setLoad(false)
            });
        }
        fetchData();
    }, []);
    
    function getValues() {
        var values = [];
        var dict = {};

        for (var i = 0; i < data.length; i++) {
            var industry = data[i]["industry"];
            if (industry in dict) {
                dict[industry]++;
            }
            else {
                dict[industry] = 1;
            }
        }

        for (var industry in dict) {
            var randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
            values.push({"name": industry, "value": dict[industry], "fill": randomColor})
        }

        return values;
    }


    if (load) {
        return <Spin />
    }
    
    return (
        <center>
        <div style={{ margin: 'auto', marginTop: '5vh'}} >
            
            <h3 className="text-center"> Number of Companies for Each Industry </h3>
            <p className="text-center"> Hover to view values </p>
            <PieChart 
                width={1000} 
                height={800} 
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <Pie style={{paddingLeft:'50px'}}
                    dataKey="value"
                    isAnimationActive={true}
                    data={getValues()}
                    cx={485}
                    cy={350}
                    outerRadius={300}
                    fill="#fff"
                    label
                />
                <Tooltip />
            </PieChart>
        </div>
        </center>

    );  

}

const CitiesChart = () =>{
    const [data, setData] = useState([]);
    const [load, setLoad] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
	    setLoad(true);
            await getAPI(api_url+'/cities?per_page=5000').then(response => {
                setData(response.data.cities)
                setLoad(false)
            }).catch((error) => {
                setData([])
                setLoad(false)
            });
        }
        fetchData();
    }, []);
    
    function getValues() {
        var values = [];

        for (var i = 0; i < data.length; i++) {
            var city = data[i]["name"];
            var housing = data[i]["housing"];
            var col = data[i]["cost_of_living"];
            
            values.push({"col": col, "housing": housing, "city": city})

        }

        return values;
    }

    if (load) {
        return <Spin />
    }

    return (
        <div style={{margin: 'auto', marginTop: '5vh'}} >
            <h3 className="text-center"> Housing vs Cost of Living</h3>
            <p className="text-center"> Hover to view exact values</p>

            <ScatterChart
            width={800}
            height={800}
            margin={{
                top: 20,
                right: 20,
                bottom: 20,
                left: 20,
            }}
            >
            <CartesianGrid />
            <XAxis type="number" dataKey="col" name="Cost of Living" />
            <YAxis type="number" dataKey="housing" name="Housing" />
            <ZAxis dataKey="city" name="City" />
            <Tooltip cursor={{ strokeDasharray: '3 3' }} />
            <Scatter data={getValues()} fill="#8884d8" />
            </ScatterChart>
        </div>
    );
}

export { StateChart, IndustryChart, CitiesChart }