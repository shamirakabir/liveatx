/* external */
import React, { useState, useEffect }	from 'react';
import {
    Link,
    useHistory,
}					from 'react-router-dom';
import {
    useQueryParams,
    StringParam,
    withDefault,
}					from 'use-query-params';
import {
    Button,
    Card,
    Col,
    Input,
    Row,
    Space,
    Spin,
    Table,
    Typography,
}					from 'antd';

/* components */
import EntertainmentCard		from '../Components/EntertainmentCard.jsx';
import EventCard			from '../Components/EventCard.jsx';
import Highlight			from '../Components/Highlight.jsx';
import MapComponent			from '../Components/MapComponent.jsx';
import Placeholder			from '../Components/Placeholder.jsx';
import Rating				from '../Components/Rating.jsx';
import RestaurantCard			from '../Components/RestaurantCard.jsx';
import {
    restaurantTableColumns,
    eventTableColumns,
    entertainmentTableColumns,
}					from '../Components/Columns.jsx';

/* other */
import { query }			from '../Library/Query.jsx';
import styles				from '../Styles/Search.module.css';

import 'antd/dist/antd.css';
require('datejs');

const { Title, Paragraph }	= Typography;
const { Meta }			= Card;

const SEARCH_PATHS		= ['restaurants', 'events', 'entertainment'];
const SEARCH_LIMIT		= 10;

const Search = () => {
    const history		= useHistory();
    const [loading, setLoading] = useState(true);
    const [search, setSearch]	= useState(undefined);    
    const [error, setError] = useState(null);
    
    const [restaurantDisplayType	, setRestaurantDisplayType]	= useState('grid');
    const [eventDisplayType		, setEventDisplayType]		= useState('grid');
    const [entertainmentDisplayType	, setEntertainmentDisplayType]	= useState('grid');
    
    const [params, setParams]	= useQueryParams({
        q: withDefault(StringParam, ''),
    });

    const [total, setTotal]	= useState(0);
    const [results, setResults] = useState({
        restaurants: [],
        events: [],
        entertainment: [],
        restaurantsTotal: 0,
        eventsTotal: 0,
        entertainmentTotal: 0,
    });    

    const handleSearchChange = obj => {
        setSearch(obj.target.value);
    };

    const handleSearch = text => {
        setParams(params => ({...params, q: text}));
        history.push(`/search?q=${text}`);
    };
    
    const Hi = (props) => (
        <Highlight
          words={[params.q]}
          text={props.text} />
    );

    useEffect(() => {
        const fetch = async () => {
            setError(null);
            setLoading(true);
            Promise.all(SEARCH_PATHS.map(model => query(model, params))).then(data => {
                var tmp = {};
                var sum = 0;
                data.forEach((r, i) => {
                    let page = [];
                    const model = SEARCH_PATHS[i];
                    switch (model) {
                    case 'restaurants':
                        page = page.concat(r.data.restaurants);
                        break;
                    case 'events':
                        page = page.concat(r.data.events);
                        break;
                    case 'entertainment':
                        page = page.concat(r.data.entertainment);
                        break;
                    default:
                        console.err('unknown model type');
                    }
                    const count = page.length;
                    tmp[model] = page.slice(0, SEARCH_LIMIT);
                    tmp[`${model}Total`] = count;
                    sum += count;
                });
                setResults(tmp);
                setTotal(sum);
                setLoading(false);
            }).catch(error => {
                console.error(error);
                setLoading(false);
                setError(error);
            });
        };
        fetch();
    }, [params, history]);

    const RestaurantGrid = () => {
        if (results.restaurants.length === 0)
            return <Typography>No results found.</Typography>;
        return (
            <React.Fragment>
              <div className={styles.grid}>
                {
                    results.restaurants.map(result => (
                        <RestaurantCard
                          query={params.q}
                          {...result}
                        />))
                }
                {
                    (results.restaurantsTotal > SEARCH_LIMIT) ? (
                        <RestaurantCard.MoreResults
                          query={params.q}
                          amount={results.restaurantsTotal - SEARCH_LIMIT}
                        />
                    ) : null
                }
              </div>
            </React.Fragment>
        );
    };

    const RestaurantTable = () => {
        if (results.restaurants.length === 0)
            return <Typography>No results found.</Typography>;
        return (
            <Space className={styles.table} direction='vertical'>
              <Table
                dataSource={results.restaurants}
                columns={restaurantTableColumns(params.q)}
                pagination={false}
              />
              {
                  (results.restaurantsTotal > SEARCH_LIMIT) ? (
                      <Link
                        className={styles.center}
                        to={`/restaurant/search/?q=${params.q}`}
                        target='_blank'
                      >
                        <Typography>
                          View {results.restaurantsTotal - SEARCH_LIMIT} more restaurants
                        </Typography>
                      </Link>
                  ) : null
              }
            </Space>
        );
    };
    
    const RestaurantMap = () => {
        var center = { lat: 0, lng: 0, };
        if (results.restaurants.length === 0)
            return <Typography>No results found.</Typography>;
        const markerData = results.restaurants.map(result => {
            const {
                restaurant_id,
                name,
                image_url,
                category,
                address,
                rating,
                review_count,
                is_open,
            } = result;
            center.lat += result.latitude;
            center.lng += result.longitude;
            return {
                name: name,
                title: name,
                position: { lat: result.latitude, lng: result.longitude, },
                info: (
                    <Space>
                      <img
                        className={styles.infoWindowCover}
                        src={image_url}
                        alt={name}
                      />
                      <Card className={styles.infoWindowCard}>
                        <Meta
                          title={
                              <Hi text={name} />
                          }
                          description={
                              <Space
                                direction='vertical'
                                size={0}
                                style={{width: '100%'}}
                              >
                                <Hi text={category} />
                                <Hi text={address} />
                                <Rating rating={parseFloat(rating)} widgetDimensions='15px'/>
                                <div>from {review_count} reviews</div>
                                { is_open ? (
                                    <Hi text='Open Now' />
                                ) : (
                                    <Hi text='Closed'/>
                                ) }
                                <Button
                                  type='text'
                                  href={`/restaurants/${restaurant_id}`}
                                  style={{color: 'inherit', width: '100%'}}
                                >
                                  See more
                                </Button>
                              </Space>
                          }
                        />
                      </Card>
                    </Space>
                ),
            };
        });
        center.lat /= results.restaurants.length;
        center.lng /= results.restaurants.length;
        return(
            <div className={styles.center}>
              <div
                style={{
                    position: 'relative',
                    height: '50vh',
                    width: '100%',
                }}>
                <MapComponent
                  position={center}
                  zoom={12}
                  markers={markerData}
                />
              </div>
            </div>
        );
    };

    const EventGrid = () => {
        if (results.events.length === 0)
            return <Typography>No results found.</Typography>;
        return (
            <React.Fragment>
              <div className={styles.grid}>
                {
                    results.events.map(result => (
                        <EventCard
                          query={params.q}
                          {...result}
                        />))
                }
                {
                    (results.eventsTotal > SEARCH_LIMIT) ? (
                        <EventCard.MoreResults
                          query={params.q}
                          amount={results.eventsTotal - SEARCH_LIMIT}
                        />
                    ) : null
                }
              </div>
            </React.Fragment>
        );
    };    

    const EventTable = () => {
        if (results.events.length === 0)
            return <Typography>No results found.</Typography>;
        return (
            <Space className={styles.table} direction='vertical'>
              <Table
                dataSource={results.events}
                columns={eventTableColumns(params.q)}
                pagination={false}
              />
              {
                  (results.eventsTotal > SEARCH_LIMIT) ? (
                      <Link
                        className={styles.center}
                        to={`/events/search/?q=${params.q}`}
                        target='_blank'
                      >
                        <Typography>
                          View {results.eventsTotal - SEARCH_LIMIT} more events
                        </Typography>
                      </Link>
                  ) : null
              }
            </Space>
        );
    };
    
    const EventMap = () => (
        <Placeholder />
    );

    const EntertainmentGrid = () => {
        if (results.entertainment.length === 0)
            return <Typography>No results found.</Typography>;
        return (
            <React.Fragment>
              <div className={styles.grid}>
                {
                    results.entertainment.map(result => (
                        <EntertainmentCard
                          query={params.q}
                          {...result}
                        />))
                }
                {
                    (results.entertainmentTotal > SEARCH_LIMIT) ? (
                        <EntertainmentCard.MoreResults
                          query={params.q}
                          amount={results.entertainmentTotal - SEARCH_LIMIT}
                        />
                    ) : null
                }
              </div>
            </React.Fragment>
        );
    };    

    const EntertainmentTable = () => {
        if (results.entertainment.length === 0)
            return <Typography>No results found.</Typography>;
        return (
            <Space className={styles.table} direction='vertical'>
              <Table
                dataSource={results.entertainment}
                columns={entertainmentTableColumns(params.q)}
                pagination={false}
              />
              {
                  (results.entertainmentTotal > SEARCH_LIMIT) ? (
                      <Link
                        className={styles.center}
                        to={`/entertainment/search/?q=${params.q}`}
                        target='_blank'
                      >
                        <Typography>
                          View {results.entertainmentTotal - SEARCH_LIMIT} more entertainment results
                        </Typography>
                      </Link>
                  ) : null
              }
            </Space>
        );
    };
    
    const EntertainmentMap = () => {
        var center = { lat: 0, lng: 0, };
        if (results.entertainment.length === 0) return <Typography>No results found.</Typography>;
        const markerData = results.entertainment.map(result => {
            const {
                entertainment_id,
                name,
                image_url,
                category,
                address,
                rating,
                review_count,
                is_open,
            } = result;
            center.lat += result.latitude;
            center.lng += result.longitude;
            return {
                name: name,
                title: name,
                position: { lat: result.latitude, lng: result.longitude, },
                info: (
                    <Space>
                      <img
                        className={styles.infoWindowCover}
                        src={image_url}
                        alt={name}
                      />
                      <Card className={styles.infoWindowCard}>
                        <Meta
                          title={
                              <Hi text={name} />
                          }
                          description={
                              <Space
                                direction='vertical'
                                size={0}
                                style={{width: '100%'}}
                              >
                                <Hi text={category} />
                                <Hi text={address} />
                                <Rating rating={parseFloat(rating)} widgetDimensions='15px'/>
                                <div>from {review_count} reviews</div>
                                { is_open ? (
                                    <Hi text='Open Now' />
                                ) : (
                                    <Hi text='Closed'/>
                                ) }
                                <Button
                                  type='text'
                                  href={`/entertainment/${entertainment_id}`}
                                  style={{color: 'inherit', width: '100%'}}
                                >
                                  See more
                                </Button>
                              </Space>
                          }
                        />
                      </Card>
                    </Space>
                ),
            };
        });
        center.lat /= results.entertainment.length;
        center.lng /= results.entertainment.length;
        return(
            <div className={styles.center}>
              <div
                style={{
                    position: 'relative',
                    height: '50vh',
                    width: '100%',
                }}>
                <MapComponent
                  position={center}
                  zoom={12}
                  markers={markerData}
                />
              </div>
            </div>
        );
    };

    if (error)
        return <Placeholder.Error err={error} />;
    
    return (
        <React.Fragment>
          <section className={styles.content}>
            <div className={styles.contentHeader}>
              <Title level={1}>
                { params.q ? (
                    total + ' results for "' + params.q + '"'
                ) : (
                    'Search'
                ) }
              </Title>
              <Paragraph>
                Search restaurants, events, and entertainment.
              </Paragraph>
              <div className='searchBar'>
                <Input.Search
                  size='large'
                  placeholder='Search everything'
                  enterButton='Search'
                  onChange={handleSearchChange}
                  onSearch={handleSearch}
                  value={search}
                  loading={loading}
                />
              </div>
            </div>
            { params.q ? (
                loading ? (
                    <div className={styles.center}>
                      <Spin size='large' />
                    </div>
                ) : (
                    <React.Fragment>
                      <div className={styles.results}>
                        <Row justify='end'>
                          <Col flex={1}>
                            <Title level={3}>
                              Restaurants
                            </Title>
                          </Col>
                          <Col flex='none'>
                            <Space>
                              <div>Display as </div>
                              <Button
                                onClick={event => setRestaurantDisplayType('grid')}
                                disabled={restaurantDisplayType === 'grid'}
                              >
                                Grid
                              </Button>
                              <Button
                                onClick={event => setRestaurantDisplayType('table')}
                                disabled={restaurantDisplayType === 'table'}
                              >
                                Table
                              </Button>
                              <Button
                                onClick={event => setRestaurantDisplayType('map')}
                                disabled={restaurantDisplayType === 'map'}
                              >
                                Map
                              </Button>
                            </Space>
                          </Col>
                        </Row>
                        { (restaurantDisplayType === 'grid')  ? <RestaurantGrid />  : null }
                        { (restaurantDisplayType === 'table') ? <RestaurantTable /> : null }
                        { (restaurantDisplayType === 'map')   ? <RestaurantMap />   : null }
                      </div>
                      <div className={styles.results}>
                        <Row justify='end'>
                          <Col flex={1}>
                            <Title level={3}>
                              Events
                            </Title>
                          </Col>
                          <Col flex='none'>
                            <Space>
                              <div>Display as </div>
                              <Button
                                onClick={event => setEventDisplayType('grid')}
                                disabled={eventDisplayType === 'grid'}
                              >
                                Grid
                              </Button>
                              <Button
                                onClick={event => setEventDisplayType('table')}
                                disabled={eventDisplayType === 'table'}
                              >
                                Table
                              </Button>
                              <Button
                                onClick={event => setEventDisplayType('map')}
                                disabled={eventDisplayType === 'map'}
                              >
                                Map
                              </Button>
                            </Space>
                          </Col>
                        </Row>
                        { (eventDisplayType === 'grid')  ? <EventGrid />  : null }
                        { (eventDisplayType === 'table') ? <EventTable /> : null }
                        { (eventDisplayType === 'map')   ? <EventMap />   : null }
                      </div>
                      <div className={styles.results}>
                        <Row justify='end'>
                          <Col flex={1}>
                            <Title level={3}>
                              Entertainment
                            </Title>
                          </Col>
                          <Col flex='none'>
                            <Space>
                              <div>Display as </div>
                              <Button
                                onClick={event => setEntertainmentDisplayType('grid')}
                                disabled={entertainmentDisplayType === 'grid'}
                              >
                                Grid
                              </Button>
                              <Button
                                onClick={event => setEntertainmentDisplayType('table')}
                                disabled={entertainmentDisplayType === 'table'}
                              >
                                Table
                              </Button>
                              <Button
                                onClick={event => setEntertainmentDisplayType('map')}
                                disabled={entertainmentDisplayType === 'map'}
                              >
                                Map
                              </Button>
                            </Space>
                          </Col>
                        </Row>
                        { (entertainmentDisplayType === 'grid')  ? <EntertainmentGrid />  : null }
                        { (entertainmentDisplayType === 'table') ? <EntertainmentTable /> : null }
                        { (entertainmentDisplayType === 'map')   ? <EntertainmentMap />   : null }
                      </div>
                    </React.Fragment>
                )
            ) : null }
          </section>
        </React.Fragment>
    );
};

export default Search;
