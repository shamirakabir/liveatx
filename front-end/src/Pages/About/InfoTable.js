import React, {Component} from 'react';
import { Paper } from '@material-ui/core';
import { Spin } from 'antd';
const COMMIT_URL = "https://gitlab.com/api/v4/projects/29899890/repository/contributors";
const ISSUE_URL = "https://gitlab.com/api/v4/projects/29899890/issues?assignee_username=";
const UNITTEST_URL = "https://gitlab.com/api/v4/projects/24679574/pipelines?username=";

const MEMBER_ID = ["ArcherH", "shamirakabir", "robynfajardo", "phloofy", "st0rmin"];


class Table extends Component {
    constructor(prop) {
        super(prop);
        this.state = {
            commits: null,
            issues: null,
            unitTest: null
        };
    }

    async componentDidMount() {
        let commits = await getAllCommits();
        let issues = await getAllIssues();
        let unitTest = await getAllUnitTest();
        this.setState({
            commits: commits,
            issues: issues,
            unitTest: unitTest
        });
    }

    getCommitsByName(name) {
        let commit = this.state.commits.get(name);
        if (commit !== undefined) {
            return commit;
        }
        return 0;
    }

    getIssuesByName(name) {
        let numIssues = this.state.issues.get(name);
        if (numIssues !== undefined) {
            return numIssues;
        }
        return 0;
    }

    getUnitTestByName(name) {
        let numTest = this.state.unitTest.get(name);
        if (numTest !== undefined) {
            return numTest;
        }
        return 0;
    }

    getAllfromMap(map) {
        var total = 0;
        map.forEach((value, key) => {
            total += value;
        });
        return total;
    }

    render() {
        if (this.state.data === null
            || this.state.issues === null) {
                return (
                    <Spin size='large'/>
                )
            }
        return (
            <Paper>
                <table cellPadding="10px">
                    <thead>
                        <tr>   
                            <th>Name</th>
                            <th>Commits</th>
                            <th>Issues</th>
                            <th>Unit Tests</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                        <tr>
                            <td>Archer Hasnbany</td>
                            <td>{this.getCommitsByName("Archer Hasbany")}</td>
                            <td>{this.getIssuesByName(MEMBER_ID[0])}</td>
                            <td>9</td>
                        </tr>
                        <tr>
                            <td>Shamira Kabir</td>
                            <td>{this.getCommitsByName("Shamira Kabir")}</td>
                            <td>{this.getIssuesByName(MEMBER_ID[1])}</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>Robyn Fajardo</td>
                            <td>{this.getCommitsByName("Robyn Fajardo")+this.getCommitsByName("Robyn F Fajardo")}</td>
                            <td>{this.getIssuesByName(MEMBER_ID[2])}</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>Ethan Tan</td>
                            <td>{this.getCommitsByName("Ethan Tan")}</td>
                            <td>{this.getIssuesByName(MEMBER_ID[3])}</td>
                            <td>24</td>
                        </tr>
                        <tr>
                            <td>Thomas Norman</td>
                            <td>{this.getCommitsByName("st0rmin")}</td>
                            <td>{this.getIssuesByName(MEMBER_ID[4])}</td>
                            <td>9</td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td>{this.getAllfromMap(this.state.commits)}</td>
                            <td>{this.getAllfromMap(this.state.issues)}</td>
                            <td>42</td>
                        </tr>
                    </tbody>
                </table>
            </Paper>
        )
    }
}

async function getAllUnitTest() {
    var group = new Map();
    for (var index in MEMBER_ID) {
        let id = MEMBER_ID[index];
        let response = await fetch(UNITTEST_URL + id);
        let json = await response.json();
        group.set(id, json.length);
    }
    return group;
}

async function getAllIssues() {
    var group = new Map();
    for (var index in MEMBER_ID) {
        let id = MEMBER_ID[index];
        let response = await fetch(ISSUE_URL+id);
        let json = await response.json();
        group.set(id, json.length);
    }
    return group;
}

async function getAllCommits() {
    var group = new Map();
    let response = await fetch(COMMIT_URL);
    var commits = await response.json();
    commits.forEach((element) => {
        const {name, email, commits} = element
        let numCommits = group.get(name)
        if (numCommits !== undefined) {
            group.set(name, numCommits + commits);
        } else {
            group.set(name, commits);
        }
    });
    return group;
}

export default Table