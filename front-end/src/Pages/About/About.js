import {React, Component} from 'react';
import { Card, CardContent, CardMedia, Typography, Grid, Box, CardActionArea} from '@material-ui/core';
import Table from "./InfoTable.js";
import "./about.css";
import Robyn from "./profile_pics/robyn.jpg";
import Shamira from "./profile_pics/shamira.jpg";
import Thomas from "./profile_pics/thomas.jpg";
import Ethan from "./profile_pics/ethan.jpg";
import Archer from "./profile_pics/archer.jpg";
import SeakGeek from "./api_pics/seakgeek.jpg";
import Yelp from "./api_pics/yelp.jpg";
import Docker from "./api_pics/docker.png";
import MUI from "./api_pics/mui.jpg";
import r from "./api_pics/react.png";
import Google from "./api_pics/Google-Maps-logo.png";
import Postman from "./api_pics/postman.png";
import Python from "./api_pics/python.png";
import GitLab from "./api_pics/gitlab.png";
import antd from "./api_pics/antd.jpg";
import Postgres from "./api_pics/postgres.png";
import AWS from "./api_pics/aws.png";
import NameCheap from "./api_pics/namecheap.png";
import Selenium from "./api_pics/selenium.png";
import Jest from "./api_pics/jest.png";
import Flask from "./api_pics/flask.png";
import Black from "./api_pics/black.png";
import SQLAlchemy from "./api_pics/sqlalchemy.jpg";
import Marshmallow from "./api_pics/marshmallow.png";
import Discord from "./api_pics/discord.png";

function About() {
    return (
        <div>
            <Grid
                container
                spacing={2}
                justifyContent="center"
                alignItems="center"
                direction="column"
                rowSpacing={5}
                p={10}
                style={{ minHeight: '100%', maxWidth: '100%'}}
                >
                <Grid item xs={12}>
                    <Typography variant="h2" component="div">About</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">What is LiveATX?</Typography>
                </Grid>
                <Grid item xs={12}>
                    <p>
                        Our website is meant for people in Austin to find fun and exciting things to do in the city, whether that be restaurants, entertainment, or special events! 
                        A major feature of the website is that you can filter your event/restaurant by proximity and means of transportation, so no matter what access to transportation you have, you can find things to do!
                        Anyone who is in Austin can use this website to find things to do whether you have access to a bus, car, or even just walking.
                    </p>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">Interesting Finds</Typography>
                </Grid>
                <Grid item xs={12}>
                    <p>While creating this website we found some interesting things about things to do around Austin, some of which include:</p>
                    <ul justify="center">
                        <li>Many events are concerts which are held at most of the same venues</li>
                        <li>Many cheaper restaurants appear near UT Campus.</li>
                        <li>Because of Austin's geography, the entertainment around Austin varies wildly.</li>
                    </ul>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h2" component="div">Meet the Team</Typography>
                </Grid>
                <Grid item xs={12} >
                    <Box sx={{flexGrow: 1}}>
                        <Grid container rowSpacing={10} alignItems="center" justifyContent="center">
                            <Grid item xs={12}>
                                <Grid container rowSpacing={10} columnSpacing={10} alignItems="center" justifyContent="center">
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "22rem" }}>
                                            <CardMedia
                                                component="img"
                                                height="200"
                                                crop= 'scale'
                                                image={Robyn}
                                                alt="Robyn"
                                            />
                                            <CardContent>
                                                <center>
                                                    <Typography variant="h5" component="div">
                                                        Robyn Fajardo
                                                    </Typography>
                                                    <Typography>
                                                        Computer Science and Asian Studies
                                                    </Typography>
                                                    <Typography>
                                                        Junior
                                                    </Typography>
                                                    <Typography>
                                                        Front-end
                                                    </Typography>
                                                </center>
                                            </CardContent>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "22rem" }}>
                                            <CardMedia
                                                component="img"
                                                height="200"
                                                crop= 'scale'
                                                image={Archer}
                                                alt="Archer"
                                            />
                                            <CardContent>
                                                <center>
                                                    <Typography variant="h5" component="div">
                                                        Archer Hasbany
                                                    </Typography>
                                                    <Typography>
                                                        Computer Science
                                                    </Typography>
                                                    <Typography>
                                                        Senior
                                                    </Typography>
                                                    <Typography>
                                                        Back-end
                                                    </Typography>
                                                </center>
                                            </CardContent>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "22rem" }}>
                                            <CardMedia
                                                component="img"
                                                height="200"
                                                crop= 'scale'
                                                image={Shamira}
                                                alt="Shamira"
                                            />
                                            <CardContent>
                                                <center>
                                                    <Typography variant="h5" component="div">
                                                        Shamira Kabir
                                                    </Typography>
                                                    <Typography>
                                                        Computer Science
                                                    </Typography>
                                                    <Typography>
                                                        Senior
                                                    </Typography>
                                                    <Typography>
                                                        Front-end
                                                    </Typography>
                                                </center>
                                            </CardContent>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "22rem" }}>
                                            <CardMedia
                                                component="img"
                                                height="200"
                                                crop= 'scale'
                                                image={Ethan}
                                                alt="Ethan"
                                            />
                                            <CardContent>
                                                <center>
                                                    <Typography variant="h5" component="div">
                                                        Ethan Tan
                                                    </Typography>
                                                    <Typography>
                                                        Computer Science
                                                    </Typography>
                                                    <Typography>
                                                        Junior
                                                    </Typography>
                                                    <Typography>
                                                        Front-end
                                                    </Typography>
                                                </center>
                                            </CardContent>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "22rem" }}>
                                            <CardMedia
                                                component="img"
                                                height="200"
                                                crop= 'scale'
                                                image={Thomas}
                                                alt="Thomas"
                                            />
                                            <CardContent>
                                                <center>
                                                    <Typography variant="h5" component="div">
                                                        Thomas Norman
                                                    </Typography>
                                                    <Typography>
                                                        Computer Science
                                                    </Typography>
                                                    <Typography>
                                                        Junior
                                                    </Typography>
                                                    <Typography>
                                                        Back-end
                                                    </Typography>
                                                </center>
                                            </CardContent>
                                        </Card>
                                    </Box>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">GitLab Statistics</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Box sx={{my: 2}}>
                        <Table />
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">Tools Used</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Box sx={{flexGrow: 1}}>
                        <Grid container rowSpacing={10} alignItems="center" justifyContent="center">
                            <Grid item xs={12}>
                                <Grid container rowSpacing={10} columnSpacing={10} alignItems="center" justifyContent="center">
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://reactjs.org/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={r}
                                                    flex="1" 
                                                    alt="React"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            React
                                                        </Typography>
                                                        <Typography>
                                                            Front-End Web Development
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://www.docker.com/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Docker}
                                                    flex="1" 
                                                    alt="Docker"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Docker
                                                        </Typography>
                                                        <Typography>
                                                            Containerization
                                                        </Typography>
                                                </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://mui.com/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={MUI}
                                                    flex="1" 
                                                    alt="MUI"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Material UI
                                                        </Typography>
                                                        <Typography>
                                                            Front-End Styling
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://ant.design/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={antd}
                                                    flex="1" 
                                                    alt="Ant Design"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Ant Design
                                                        </Typography>
                                                        <Typography>
                                                            Front-End Styling
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>  
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://www.python.org/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Python}
                                                    flex="1" 
                                                    alt="Python"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Python
                                                        </Typography>
                                                        <Typography>
                                                            Back-End Data Scraping
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://postman.com'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Postman}
                                                    flex="1" 
                                                    alt="Postman"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Postman
                                                        </Typography>
                                                        <Typography>
                                                            API Design
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://flask.palletsprojects.com/en/2.0.x/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Flask}
                                                    flex="1" 
                                                    alt="Flask"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Flask
                                                        </Typography>
                                                        <Typography>
                                                            Framework for API Development
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://flask.palletsprojects.com/en/2.0.x/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={SQLAlchemy}
                                                    flex="1" 
                                                    alt="SQLAlchemy"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            SQLAlchemy
                                                        </Typography>
                                                        <Typography>
                                                            SQL Toolkit and Object-Relational Mapper
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://marshmallow.readthedocs.io/en/stable/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Marshmallow}
                                                    flex="1" 
                                                    alt="Marshmallow"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Marshmallow
                                                        </Typography>
                                                        <Typography>
                                                            Complex Datatype Conversion
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://black.readthedocs.io/en/stable/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Black}
                                                    flex="1" 
                                                    alt="Black"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Black
                                                        </Typography>
                                                        <Typography>
                                                            Python Code Formatter
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://www.postgresql.org/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Postgres}
                                                    flex="1" 
                                                    alt="PostgreSQL"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            PostgreSQL
                                                        </Typography>
                                                        <Typography>
                                                            Database Management
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://aws.amazon.com/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={AWS}
                                                    flex="1" 
                                                    alt="AWS"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            AWS
                                                        </Typography>
                                                        <Typography>
                                                            Hosting of website/back-end deployment
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://www.selenium.dev/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Selenium}
                                                    flex="1" 
                                                    alt="Selenium"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Selenium
                                                        </Typography>
                                                        <Typography>
                                                            GUI Testing
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://jestjs.io/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Jest}
                                                    flex="1" 
                                                    alt="Jest"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Jest
                                                        </Typography>
                                                        <Typography>
                                                            JavaScript Testing
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://discord.com/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Discord}
                                                    flex="1" 
                                                    alt="Discord"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Discord
                                                        </Typography>
                                                        <Typography>
                                                            Communication
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "19rem" }}>
                                            <CardActionArea href='https://www.namecheap.com/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={NameCheap}
                                                    flex="1" 
                                                    alt="NameCheap"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            NameCheap
                                                        </Typography>
                                                        <Typography>
                                                            Domain Name Registrar
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">APIs Used</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Box sx={{flexGrow: 1}}>
                        <Grid container rowSpacing={10} alignItems="center" justifyContent="center">
                            <Grid item xs={12}>
                                <Grid container rowSpacing={10} columnSpacing={10} alignItems="center" justifyContent="center">
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "21rem"}}>
                                            <CardActionArea href='https://platform.seatgeek.com/'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={SeakGeek}
                                                    alt="SeakGeek"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            SeatGeek
                                                        </Typography>
                                                        <Typography>
                                                            Retrieve Events around Austin
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "21rem" }}>
                                            <CardActionArea href='https://www.yelp.com/fusion'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Yelp}
                                                    flex="1" 
                                                    alt="Yelp"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Yelp
                                                        </Typography>
                                                        <Typography>
                                                            Retrieve Restaurants around Austin
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "21rem" }}>
                                            <CardActionArea href='https://developers.google.com/maps/documentation/javascript/overview'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Google}
                                                    flex="1" 
                                                    alt="Google Maps API"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Javascript Google Maps
                                                        </Typography>
                                                        <Typography>
                                                            Retrieve Locations and Map of Businesses and Events
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "21rem" }}>
                                            <CardActionArea href='https://developers.google.com/maps/documentation/geocoding/overview'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Google}
                                                    flex="1" 
                                                    alt="Google Maps API"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            Geocoding Google Maps
                                                        </Typography>
                                                        <Typography>
                                                            Retrieve Geocoding of Businesses and Events
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">Our Application</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Box sx={{flexGrow: 1}}>
                        <Grid container rowSpacing={10} alignItems="center" justifyContent="center">
                            <Grid item xs={12}>
                                <Grid container rowSpacing={10} columnSpacing={10} alignItems="center" justifyContent="center">
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "16rem"}}>
                                            <CardActionArea href='https://gitlab.com/shamirakabir/liveatx'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={GitLab}
                                                    alt="GitLab"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            GitLab Repository
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                    <Box sx={{my: 1}}>
                                        <Card style={{ width: "18rem", marginLeft: "20px", float: "left", height: "16rem"}}>
                                            <CardActionArea href='https://documenter.getpostman.com/view/17689474/UUy37QtV'>
                                                <CardMedia
                                                    component="img"
                                                    height="200"
                                                    crop= 'scale'
                                                    image={Postman}
                                                    alt="Postman API"
                                                />
                                                <CardContent>
                                                    <center>
                                                        <Typography variant="h5" component="div">
                                                            API Documentation
                                                        </Typography>
                                                    </center>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>
            </Grid>
        </div>
    )
}


export default About; 