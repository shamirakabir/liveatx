import React			from 'react';
import { configure, shallow }	from 'enzyme';
import Adapter			from 'enzyme-adapter-react-16';
import axios			from 'axios';
import { useQueryParams }	from 'use-query-params';

import Home			from '../Pages/Home/Home.js';
import About			from '../Pages/About/About.js';
import Table			from '../Pages/About/InfoTable.js';

import RestaurantCard		from '../Components/RestaurantCard.jsx';
import EntertainmentCard	from '../Components/EntertainmentCard.jsx';
import EventCard		from '../Components/EventCard.jsx';
import Highlight		from '../Components/Highlight.jsx';
import Placeholder		from '../Components/Placeholder.jsx';
import Navbar			from '../Components/Navbar/Navbar.js';

import Entertainment		from '../Pages/Entertainment.jsx';
import Event			from '../Pages/Event.jsx';
import Restaurant		from '../Pages/Restaurant.jsx';

configure({ adapter: new Adapter() });

describe('Render Pages', () => {

    test('Render Home', () => {
	const homeTest = shallow(<Home />);
	expect(homeTest).toMatchSnapshot();
    });

    test('Render About', () => {
	const aboutTest = shallow(<About />);
	expect(aboutTest).toMatchSnapshot();
    });
})

describe('Render Components', () => {


    test('Render Navbar', () => {
	const navbarTest = shallow(<Navbar />);
	expect(navbarTest).toMatchSnapshot();
    });

    test('Render InfoTable', () => {
	const infotableTest = shallow(<Table />);
	expect(infotableTest).toMatchSnapshot();
    });

    test('Render RestaurantCard', () => {
        const data = require('./restaurantById.json');
        const restaurantCardTest = shallow(<RestaurantCard {...data} />);
        expect(restaurantCardTest).toMatchSnapshot();
    });

    test('Render EventCard', () => {
        const data = require('./eventByID.json');
        const eventCardTest = shallow(<EventCard {...data} />);
        expect(eventCardTest).toMatchSnapshot();
    });

    test('Render EntertainmentCard', () => {
        const data = require('./entertainmentByID.json');
        const entertainmentCardTest = shallow(<EntertainmentCard {...data} />);
        expect(entertainmentCardTest).toMatchSnapshot();
    });

    test('Render Highlight', () => {
        const highlightTest = shallow(
            <Highlight
              words={['a', 'b']}
              text={'the quick brown fox jumps over the lazy dog'}
            />
        );
        expect(highlightTest).toMatchSnapshot();
    });

    test('Render Placeholder', () => {
        const placeholderTest = shallow(<Placeholder />);
        expect(placeholderTest).toMatchSnapshot();
    });

    test('Render Error', () => {
        const errorTest = shallow(<Error err={{message: 'test error'}} />);
        expect(errorTest).toMatchSnapshot();
    });

});

jest.mock('axios');
jest.mock('use-query-params');

describe('Render Models', () => {

    test('Render Restaurants', () => {
	const restaurantData = require('./restaurantData.json')	;
	axios.get.mockImplementationOnce(() => Promise.resolve(restaurantData));
	useQueryParams.mockImplementation((params) => [{}, () => {}]);
	const restaurantTest = shallow(<Restaurant />);
	expect(restaurantTest).toMatchSnapshot();	

    });
    
    test('Render Events', () => {
	const eventData = require('./eventData.json');
	axios.get.mockImplementationOnce(() => Promise.resolve(eventData));
	useQueryParams.mockImplementation((params) => [{}, () => {}]);
	const eventTest = shallow(<Event />);
	expect(eventTest).toMatchSnapshot();	
    });

    test('Render Entertainment', () => {
	const entertainmentData = require('./entertainmentData.json');
	axios.get.mockImplementationOnce(() => Promise.resolve(entertainmentData));
	useQueryParams.mockImplementation((params) => [{}, () => {}]);
	const entertainmentTest = shallow(<Entertainment />);
	expect(entertainmentTest).toMatchSnapshot();
    });
});
