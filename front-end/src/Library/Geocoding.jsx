import axios from 'axios';

import { getAPI } from '../Pages/library/Data.js';

const key = 'AIzaSyBvA8E2EteyZApEyKJ_dp8p0EVcsCLyJAU';
const url = 'https://maps.googleapis.com/maps/api/geocode/json';

export const fetchLatLng = async address => {
    const request = url + '?address=' + address + '&key=' + key;
    console.log(request);
    const result = await getAPI(request);
    console.log(result);
    return result.data.results[0].geometry.location;
};

