import axios from 'axios';
import cache from 'lru-cache';
import { stringify } from 'query-string';

const data_cache = new cache();
const api_url = process.env.REACT_APP_NOT_SECRECT_CODE;
// const api_url = 'http://localhost/api';

export const getAPI = async url => {
    if (!data_cache.has(url)) {
        const data = await axios.get(url);
        data_cache.set(url, data);
        console.log(`[cache] ${data}`);
        return data;
    } else {
        return data_cache.get(url);
    }
};

export const getUrl = (model, params) => {
    console.log(params);
    const tmp = stringify(params);
    const url = `${api_url}/${model}?${tmp}`;
    return url;
}

export const query = (model, params) => getAPI(getUrl(model, params));
