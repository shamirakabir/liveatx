import React						from 'react';
import { MuiThemeProvider, Typography }			from '@material-ui/core';
import { BrowserRouter as Router, Switch, Route }	from 'react-router-dom';
import Home						from './Pages/Home/Home.js';
import About						from './Pages/About/About.js';
import Entertainment					from './Pages/Entertainment.jsx';
import EntertainmentInstance				from './Pages/EntertainmentInstance.jsx';
import EntertainmentSearch				from './Pages/Entertainment/EntertainmentSearch.js';
import Restaurant					from './Pages/Restaurant.jsx';
import RestaurantInstance				from './Pages/RestaurantInstance.jsx';
import Event						from './Pages/Event.jsx';
import EventSearch					from './Pages/Event/EventSearch.js';
import EventInstance					from './Pages/EventInstance.jsx';
import Navbar						from './Components/Navbar/Navbar.js';
import Search						from './Pages/Search.jsx';
import Visualizations					from './Pages/Visualizations/Visualizations.js';
import ProvidersVisualizations					from './Pages/Visualizations/ProviderVisualizations.js';
import { QueryParamProvider }				from 'use-query-params';
import './App.css';

function App() {
  return (
    <Router>
      <QueryParamProvider ReactRouterRoute={Route}>
	<div>
          <Navbar/>
          {/* A <Switch> looks through its children <Route>s and renders the first one that matches the current URL. */}
          <Switch>
	    <Route exact path="/about">
	      <About />
	    </Route>
            <Route path="/restaurants/:id">
              <RestaurantInstance />
            </Route>
            <Route exact path="/restaurant">
              <Restaurant />
            </Route>
            <Route path="/events/:id">
              <EventInstance />
            </Route>
            <Route exact path="/event">
              <Event />
            </Route>
            <Route path="/entertainment/search">
              <EntertainmentSearch />
            </Route>
            <Route path="/entertainment/:id">
              <EntertainmentInstance />
            </Route> 
            <Route exact path="/entertainment">
              <Entertainment />
            </Route>
            <Route exact path="/search">
              <Search />
            </Route>
            <Route exact path="/visualizations">
              <Visualizations />
            </Route>
            <Route exact path="/providers-visualizations">
              <ProvidersVisualizations />
            </Route>
            <Route exact path="/">
              <Home />
            </Route>
          </Switch>
	</div>
      </QueryParamProvider>
    </Router>
  );
}

export default App;
