import React		from 'react';
import Highlight	from './Highlight.jsx';
import Rating		from './Rating.jsx';

import { Link }		from 'react-router-dom';

export const restaurantTableColumns = query => [
    {
	title: 'Name',
	dataIndex: 'name',
	key: 'name',
	render: (text, record, index) =>
	    <Link to={`/restaurants/${record.restaurant_id}`} target='_blank'>
	      <Highlight words={[query]} text={text} />
	    </Link>
    },
    {
	title: 'Category',
	dataIndex: 'category',
	key: 'category',
	render: text => <Highlight words={[query]} text={text} />
    },
    {
	title: 'Rating',
	dataIndex: 'rating',
	key: 'rating',
	render: text => <Rating rating={parseFloat(text)} />
    },
    {
	title: 'Review Count',
	dataIndex: 'review_count',
	key: 'review_count',
    },
    {
	dataIndex: 'is_open',
	key: 'is_open',
	render: open => (
	    open ? (
		<Highlight words={[query]} text='Open Now'/>
	    ) : (
		<Highlight words={[query]} text='Closed'/>
	    )
	)
    },
];

export const eventTableColumns = query => [
    {
	title: 'Name',
	dataIndex: 'name',
	key: 'name',
	render: (text, record, index) =>
	    <Link to={`/events/${record.event_id}`} target='_blank'>
	      <Highlight words={[query]} text={text} />
	    </Link>
    },
    {
	dataIndex: 'performers',
	key: 'performers',
	render: data => <Highlight words={[query]} text={data.join(', ')} />
    },
    {
	title: 'Address',
	dataIndex: ['address', 'zipcode'],
	key: 'address',
	render: (text, record, index) =>
	    <Highlight words={[query]} text={`${record.address}, ${record.zipcode}`} />
    },
    {
	title: 'Date',
	dataIndex: 'datetime_utc',
	key: 'datetime_utc',
	render: date => <Highlight words={[query]}
				   text={(new Date(date)).toString('MMMM d yyyy hh:mm tt')} />
    },
    {
	title: 'Average ticket price',
	dataIndex: 'average_ticket_price',
	key: 'average_ticket_price',
	render: text => `$${text}`
    },
];

export const entertainmentTableColumns = (query) => [
    {
	title: 'Name',
	dataIndex: 'name',
	key: 'name',
	render: (text, record, index) =>
	    <Link to={`/entertainment/${record.entertainment_id}`} target='_blank'>
	      <Highlight words={[query]} text={text} />
	    </Link>
    },
    {
	title: 'Category',
	dataIndex: 'category',
	key: 'category',
	render: text => <Highlight words={[query]} text={text} />
    },
    {
	title: 'Rating',
	dataIndex: 'rating',
	key: 'rating',
	render: text => <Rating rating={parseFloat(text)} />
    },
    {
	title: 'Review Count',
	dataIndex: 'review_count',
	key: 'review_count',
    },
    {
	dataIndex: 'is_open',
	key: 'is_open',
	render: text => (
	    text ? (
		<Highlight words={[query]} text='Open Now'/>
	    ) : (
		<Highlight words={[query]} text='Closed'/>
	    )
	)
    },
];
