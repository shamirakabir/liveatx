import React	from 'react';
import Ratings	from 'react-ratings-declarative';

const Rating = (props) => (
    <Ratings
      rating={props.rating}
      widgetDimensions={props.widgetDimensions || '20px'}
      widgetSpacing='0px'
    >
      <Ratings.Widget />
      <Ratings.Widget />
      <Ratings.Widget />
      <Ratings.Widget />
      <Ratings.Widget />
    </Ratings>
);

export default Rating;
