import React		from 'react';
import { Card, Space }	from 'antd';
import { Link }		from 'react-router-dom';
import Highlight	from './Highlight.jsx';
import styles		from '../Styles/Search.module.css';
import 'antd/dist/antd.css';

const { Meta } = Card;
require('datejs');

const EventCard = props => {
    const {
        name,
        event_id,
        datetime_utc,
        images,
        performers,
        venue,
        average_ticket_price,
        address,
        zipcode,
        query,
    } = props;

    const Hi = props => (
        <Highlight
          words={[query]}
          text={props.text} />
    );

    const date = new Date(datetime_utc);

    return (
        <Link to={`/events/id=${event_id}`}>
          <Card
            className={styles.card}
            hoverable
            cover={
                <img className={styles.cover}
                     src={images[0]}
                     alt={name}
                />
            }
          >
            <Meta
              title={
                  <Hi text={name} />
              }
              description={
                  <Space
                    direction='vertical'
                    size={0}
                  >
                    <Hi text={performers.join(', ')} />
                    <Hi text={venue} />
                    <Hi text={address + ', ' + zipcode} />
                    <Hi text={date.toString('MMMM d yyyy hh:mm tt')} />
                    <div>Average ticket price: ${average_ticket_price}</div>
                  </Space>
              }
            />
          </Card>
        </Link>
    );
};

const MoreResults = ({amount, query}) => (
    <Link to={`event?q=${query}`}>
      <Card className={styles.card}>
        <Meta
          title={'View ' + amount + ' more events'}
        />
      </Card>
    </Link>
);

EventCard.MoreResults = MoreResults;

export default EventCard;
