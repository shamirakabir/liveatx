import React		from 'react';
import { Button, Card, Space }	from 'antd';
import { Link }		from 'react-router-dom';
import Highlight	from './Highlight.jsx';
import Rating		from './Rating.jsx';
import styles		from '../Styles/Search.module.css';
import 'antd/dist/antd.css';

const { Meta } = Card;

const EntertainmentCard = props => {
    const {
        address,
        category,
        entertainment_id,
        image_url,
        is_open,
        latitude,
        longitude,
        name,
        rating,
        review_count,
	query,
    } = props;

    const Hi = (props) => (
        <Highlight
          words={[query]}
          text={props.text} />
    );

    return (
        <Link to={`/entertainment/${entertainment_id}`}>
          <Card
            className={styles.card}
            hoverable
            cover={
                <img className={styles.cover}
                     src={image_url}
                     alt={name}
                />
            }
          >
            <Meta
              title={
                  <Hi text={name} />
              }
              description={
                  <Space
                    direction='vertical'
                    size={0}
                  >
                    <Hi text={category} />
                    <Hi text={address} />
                    <Rating rating={parseFloat(rating)}/>
                    <div>from {review_count} reviews</div>
                    { is_open ? (
                        <Hi text='Open Now' />
                    ) : (
                        <Hi text='Closed'/>
                    ) }
                  </Space>
              }
            />
          </Card>
        </Link>
    );    
};

const Mini = props => {
    const {
        entertainment_id,
        name,
        image_url,
        category,
        address,
        rating,
        review_count,
        is_open,
        query,
    } = props;
    
    const Hi = (props) => (
        <Highlight
          words={[query]}
          text={props.text} />
    );
        
    return (
        <Space>
          <img
            className={styles.infoWindowCover}
            src={image_url}
            alt={name}
          />
          <Card className={styles.infoWindowCard}>
            <Meta
              title={
                  <Hi text={name} />
              }
              description={
                  <Space
                    direction='vertical'
                    size={0}
                    style={{width: '100%'}}
                  >
                    <Hi text={category} />
                    <Hi text={address} />
                    <Rating rating={parseFloat(rating)} widgetDimensions='15px'/>
                    <div>from {review_count} reviews</div>
                    { is_open ? (
                        <Hi text='Open Now' />
                    ) : (
                        <Hi text='Closed'/>
                    ) }
                    <Button
                      type='text'
                      href={`/entertainment/${entertainment_id}`}
                      style={{color: 'inherit', width: '100%'}}
                    >
                      See more
                    </Button>
                  </Space>
              }
            />
          </Card>
        </Space>
    );
};

const MoreResults = ({amount, query}) => (
    <Link to={`/entertainment/search?=${query}`}>
      <Card className={styles.card}>
        <Meta
          title={`View ${amount} more entertainment results`}
        />
      </Card>
    </Link>
);

EntertainmentCard.MoreResults = MoreResults;
EntertainmentCard.Mini = Mini;

export default EntertainmentCard;
