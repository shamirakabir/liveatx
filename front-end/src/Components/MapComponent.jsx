import React, { useEffect, useState }			from 'react';
import { InfoWindow, Map, Marker, GoogleApiWrapper }	from 'google-maps-react';

/**
 * props:
 *   zoom:     initial zoom
 *   position: initial center
 *   markers:  array of properties describing child markers
 */
const MapComponent = props => {
    const [activeId, setActiveId] = useState(null);
    const handleClick = (props, marker, event) => {
        setActiveId(null);
    };

    var id = 0;    
    return (
        <Map
          google={props.google}
          zoom={props.zoom || 16}
          initialCenter={props.position}
          onClick={handleClick}
          styles={[{
              featureType: 'poi',
              stylers: [{ visibility: 'off' }],              
          }]}
        >
          {
              props.markers.map(opts => {
                  const myId = id;
                  id += 1;
                  return (
                      <InfoMarker
                        markerId={myId}
                        activeId={activeId}
                        setActiveId={setActiveId}
                        {...opts}
                      />
                  );
              })
          }
        </Map>
    );
};

/**
 * props:
 *   map, google, mapCenter: inherit from parent map
 *   markerId: id of marker in parent
 *   activeId: id of active marker in parent
 *   setActiveId: callback to set active marker id
 *   hideInfo: hide the info marker if active
 *   info:  content of info window
 *   other Marker/InfoWindow props
 */
const InfoMarker = props => {
    const { markerId, activeId, setActiveId } = props;
    const [showInfo, setShowInfo] = useState(false);
    const [active, setActive] = useState(null);
    const handleClick = (props, marker, event) => {
        setShowInfo(true);
        setActiveId(markerId);
    };

    const handleClose = (props, window, event) => {
        setShowInfo(false);
        setActiveId(null);
    };
    useEffect(() => {
        if (activeId !== markerId) {
            setShowInfo(false);
            setActive(null);
        }
    }, [props]);
    
    return (
        <React.Fragment>
          <Marker
            onClick={handleClick}
            {...props}
          />
          {
              props.noWindow ? null : (
                  <InfoWindow
                    marker={active}
                    visible={showInfo}
                    onClose={handleClose}
                    {...props}
                  >
                    <div>{props.info}</div>
                  </InfoWindow>
              )
          }
        </React.Fragment>
    );
};

MapComponent.InfoMarker = InfoMarker;

export default GoogleApiWrapper({
    apiKey: 'AIzaSyBvA8E2EteyZApEyKJ_dp8p0EVcsCLyJAU',
})(MapComponent);
    
