import React, { useState } from 'react';
import {
  List,
  ListItem,
  ListItemIcon,
  IconButton,
  ListItemText,
  makeStyles,
  Drawer,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';

const DrawerComponent = () => {
  const useStyles = makeStyles(theme => ({
    drawerContainer: {},
    iconButtonContainer: {
      marginLeft: 'auto',
      color: 'white',
    },

    menuIconToggle: {
      fontSize: '3rem',
    },

    listItem: {
        color: '#FFAD7B',
    }
  }));

  const [openDrawer, setOpenDrawer] = useState(false);

  // Inspired by https://www.youtube.com/watch?v=6JSkAfNvY4M
  const classes = useStyles();
  return (
    <>
      <Drawer
        anchor='right'
        classes={{ paper: classes.drawerContainer }}
        onClose={() => setOpenDrawer(false)}
        open={openDrawer}
        onOpen={() => setOpenDrawer(true)}>
        <List>
          <ListItem divider button component={Link} to='/' onClick={() => setOpenDrawer(false)}>
            <ListItemIcon >
              <ListItemText className={classes.listItem} >Home</ListItemText>
            </ListItemIcon>
          </ListItem>

          <ListItem divider button component={Link} to='/about' onClick={() => setOpenDrawer(false)}>
            <ListItemIcon>
              <ListItemText className={classes.listItem} >About</ListItemText>
            </ListItemIcon>
          </ListItem>

          <ListItem divider button component={Link} to='/restaurant' onClick={() => setOpenDrawer(false)}>
            <ListItemIcon>
              <ListItemText className={classes.listItem} >Restaurants</ListItemText>
            </ListItemIcon>
          </ListItem>

          <ListItem divider button component={Link} to='/event' onClick={() => setOpenDrawer(false)}>
            <ListItemIcon>
              <ListItemText className={classes.listItem} >Events</ListItemText>
            </ListItemIcon>
          </ListItem>

          <ListItem divider button component={Link} to='/entertainment' onClick={() => setOpenDrawer(false)}>
            <ListItemIcon>
              <ListItemText className={classes.listItem} >Entertainment</ListItemText>
            </ListItemIcon>
          </ListItem>

          <ListItem divider button component={Link} to='/visualizations' onClick={() => setOpenDrawer(false)}>
            <ListItemIcon>
              <ListItemText className={classes.listItem} >Visualizations</ListItemText>
            </ListItemIcon>
          </ListItem>

          <ListItem divider button component={Link} to='/providers-visualizations' onClick={() => setOpenDrawer(false)}>
            <ListItemIcon>
              <ListItemText className={classes.listItem} >Provider's Visualizations</ListItemText>
            </ListItemIcon>
          </ListItem>

          <ListItem divider button component={Link} to='/search' onClick={() => setOpenDrawer(false)}>
            <ListItemIcon>
              <ListItemText className={classes.listItem} >Search</ListItemText>
            </ListItemIcon>
          </ListItem>
        </List>
      </Drawer>
      {/* Since this is inside our toolbar we can push it to the end of the toolbar */}
      <IconButton
        className={classes.iconButtonContainer}
        onClick={() => setOpenDrawer(!openDrawer)}
        disableRipple>
        <MenuIcon className={classes.menuIconToggle} />
      </IconButton>
    </>
  );
};

export default DrawerComponent;