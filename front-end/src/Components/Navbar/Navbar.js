import React from 'react';
import { AppBar, Box, Button, Toolbar, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import {BrowserRouter as Link} from 'react-router-dom';
import DrawerComponent from './DrawerComponent.js';
import logo from "./logo.png";

const Navbar = () => {
  const theme = useTheme();
  const isMatch = useMediaQuery(theme.breakpoints.down('md'));
  
  return (
    <>
    <Box style={{ flexGrow: 1 }}>
      <AppBar position="static" style={{ background: '#FFAD7B' }}>
        <Toolbar>
          {/* <Typography variant="h6" component="div" style={{ flexGrow: 0.05 }}>
            LiveATX
          </Typography> */}
          <img src={logo} alt="logo" />
          {isMatch ? ( <> <DrawerComponent /> </>) : (
            <>
              <Button color="inherit" href="/">Home</Button>
              <Button color="inherit" href="/about">About</Button>
              <Button color="inherit" href="/restaurant">Restaurants</Button>
              <Button color="inherit" href="/event">Events</Button>
              <Button color="inherit" href="/entertainment">Entertainment</Button>
              <Button color="inherit" href="/visualizations">Visualizations</Button>
              <Button color="inherit" href="/providers-visualizations">Provider's Visualizations</Button>
              <Button color="inherit" href="/search">Search</Button>
            </>
          )}
        </Toolbar>
      </AppBar>
    </Box>
    </>
  );
};

export default Navbar;
