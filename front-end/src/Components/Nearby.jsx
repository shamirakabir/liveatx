import React, { useEffect, useState } from 'react';
import { Button } from '@material-ui/core';
import Placeholder from './Placeholder.jsx';
import { getAPI } from '../Library/Query.jsx';

const api_url = process.env.REACT_APP_NOT_SECRECT_CODE;

const Restaurants = ({distance}) => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetch = async () => {
            setLoading(true);
            getAPI(`${api_url}/restaurants/distance=${distance}`)
                .then(response => setData(response.data.slice(0, 5)))
                .catch(setError)
                .finally(() => setLoading(false));
        };
        fetch();
    }, []);    
    
    if (error) return <Placeholder.Error err={error} />;
    if (loading) return <Placeholder.Loading />;
    if (data.length === 0)
        return <p>No restaurants found.</p>;
    return (
        <React.Fragment>
          {
              data.map(x => (
                  <li>
                    <Button href={`/restaurants/${x.restaurant_id}`}>
                      {x.name}
                    </Button>
                  </li>
              ))
          }
        </React.Fragment>
    );
};

const Events = ({zipcode}) => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetch = async () => {
            setLoading(true);
            getAPI(`${api_url}/events/zipcode=${zipcode}`)
                .then(response => setData(response.data.slice(0, 5)))
                .catch(setError)
                .finally(() => setLoading(false));
        };
        fetch();
    }, []);    
    
    if (error) return <Placeholder.Error err={error} />;
    if (loading) return <Placeholder.Loading />;
    if (data.length === 0)
        return <p>No events found.</p>;
    return (
        <React.Fragment>
          {
              data.map(x => (
                  <li>
                    <Button href={`/events/${x.event_id}`}>
                      {x.name}
                    </Button>
                  </li>
              ))
          }
        </React.Fragment>
    );
};

const Entertainment = ({distance}) => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetch = async () => {
            setLoading(true);
            getAPI(`${api_url}/entertainment/distance=${distance}`)
                .then(response => setData(response.data.slice(0, 5)))
                .catch(setError)
                .finally(() => setLoading(false));
        };
        fetch();
    }, []);    
    
    if (error) return <Placeholder.Error err={error} />;
    if (loading) return <Placeholder.Loading />;
    if (data.length === 0)
        return <p>No entertainment found.</p>;
    return (
        <React.Fragment>
          {
              data.map(x => (
                  <li>
                    <Button href={`/entertainment/${x.entertainment_id}`}>
                      {x.name}
                    </Button>
                  </li>
              ))
          }
        </React.Fragment>
    );
};

const Nearby = {};
Nearby.Restaurants	= Restaurants;
Nearby.Events		= Events;
Nearby.Entertainment	= Entertainment;

export default Nearby;
