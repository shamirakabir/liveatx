import React		from 'react';
import { Space, Spin }	from 'antd';
import doesntworklmao	from './Media/doesntworklmao.gif';
import FRICK		from './Media/FRICK.png';

import 'antd/dist/antd.css';

const Placeholder = () => (
    <div
      style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}
    >
      <Space direction='vertical'>
	<div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}
	>
	  <img src={doesntworklmao} />
	</div>
	<h3
	  style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}
	>
	  doesn't work lmao
	</h3>
      </Space>
    </div>
);

const Error = ({err}) => (
    <div
      style={{display: 'flex', alignItems: 'center', justifyContent: 'center', height: '80vh'}}
    >
      <Space direction='vertical'>
	<div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}
	>
	  <img src={FRICK} />
	</div>
	<h3
	  style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}
	>
	  {`error :(`}
	</h3>
	<h3
	  style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}
	>
	  {err.message}
	</h3>
      </Space>
    </div>
);

const Loading = () => (
    <div
      style={{
	  display: 'flex',
	  alignItems: 'center',
	  justifyContent: 'center',
	  height: '100%',
	  width: '100%',
      }}
    >
      <Spin size='large'/>
    </div>
);

Placeholder.Error = Error;
Placeholder.Loading = Loading;

export default Placeholder;
