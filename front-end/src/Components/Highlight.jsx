import React, { Fragment }	from 'react';
import Highlighter		from 'react-highlight-words';

import styles			from '../Styles/Search.module.css';

const Highlight = props => (    
    <Highlighter
      highlightClassName={styles.highlight}
      searchWords={props.words}
      textToHighlight={props.text}
      {...props} />
);

export default Highlight;
