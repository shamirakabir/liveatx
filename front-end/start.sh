#! /usr/bin/bash

docker build . -f Dockerfile.dev -t liveatx-frontend-dev && \
    docker run -it --rm -p 3000:3000 \
	   --volume ${PWD}:/app \
	   --workdir /app \
	   liveatx-frontend-dev $@
